import pkg_resources

import cciseastate.altimeter


# Quality levels
QUALITY_GOOD = 3
QUALITY_ACCEPTABLE = 2
QUALITY_BAD = 1
QUALITY_UNDEFINED = 0


_processors = {
        entry_point.name: entry_point
        for entry_point
        in pkg_resources.iter_entry_points('cciseastate.missions')
}


def l2p_processor_v2(mission):
    return _processors[mission.upper()]

