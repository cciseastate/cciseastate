"""
Base processor for the conversion of a GDR altimeter product to CCI format and
content.

:copyright: Copyright 2020 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
from abc import abstractmethod
import os
import logging
from collections import OrderedDict
import uuid
import datetime
from pkg_resources import resource_filename
from pathlib import Path

import numpy
import xarray as xr
from netCDF4 import getlibversion

from cerbere.dataset.field import Field
from cerbere.feature.trajectory import Trajectory
from cerform.geodesic.distance import get_distance
from ceremd import denoise


VERSION = "2.0.6"

# Quality levels
QUALITY_GOOD = 3
QUALITY_ACCEPTABLE = 2
QUALITY_BAD = 1
QUALITY_UNDEFINED = 0


# variables in L2P
VARIABLES = OrderedDict([
    ('sigma0', 'Ku band backscatter coefficient'),
    ('sigma0_adjusted', 'Ku band adjusted backscatter coefficient'),
    ('sigma0_rms', 'RMS of the deviation from the median Ku band sigma0 (from '
                   'high rate measurements)'),
    ('sigma0_num_valid',
     'number of high rate valid points used to compute Ku band backscatter '
     'coefficient'),
#    ('mss', 'mean square slope from Ku band backscatter'),
    ('swh', 'Ku band significant wave height'),
    ('swh_adjusted', 'Ku band adjusted significant wave height'),
    ('swh_quality', 'quality of Ku band significant wave height measurement'),
    ('swh_uncertainty', 'best estimate of significant wave height standard error'),
    ('swh_rms', 'RMS of the deviation from the median Ku band significant wave '
                'height (from high rate measurements)'),
    ('swh_num_valid', 'number of high rate valid points used to compute Ku '
                      'band significant wave height'),
    ('swh_rejection_flags', 'consolidated instrument and ice flags'),
#    ('off_nadir_angle_wf', 'square of the off nadir angle computed from waveforms'),
    ('range', 'Ku band range'),
    ('range_rms', 'RMS of the Ku band range'),
    ('wind_speed_alt', 'wind speed from Ku band altimeter, as in GDR'),
    # ('wind_speed_alt_adjusted_abdalla_2012',
    #  'adjusted wind speed from Abdalla 2012'),
    # ('wind_speed_alt_adjusted_gourrion_2002',
    #  'adjusted wind speed from Gourrion 2002'),
    # ('wind_speed_rad', 'wind speed from radiometer, as in GDR'),
    # ('wind_speed_alt_quality', 'quality of Ku band altimeter wind speed'),
    # ('wind_speed_rad_quality', 'quality of radiometer wind speed'),
    ('total_column_liquid_water_content_rad', 'radiometer liquid water content'),
    ('sea_ice_fraction', 'sea ice fraction')
])

# variables to be copied from GDR to L2P
SOURCE_FIELDS = ['sigma0', 'sigma0_rms', 'sigma0_num_valid',
                 'swh', 'swh_rms', 'swh_num_valid', 'off_nadir_angle_wf',
                 'range', 'range_rms', 'wind_speed_alt', 'wind_speed_rad',
                 'total_column_liquid_water_content_rad']

AUTHORITY = 'CF 1.7'
STANDARDNAME = {
    'mss': 'sea_surface_wave_mean_square_slope',
    'swh': 'sea_surface_wave_significant_height',
    'swh_adjusted': 'sea_surface_wave_significant_height',
    'swh_filtered': 'sea_surface_wave_significant_height',
    'sigma0': 'surface_backwards_scattering_coefficient_of_radar_wave',
    'sigma0_adjusted': 'surface_backwards_scattering_coefficient_of_radar_wave',
    'wind_speed_alt': 'wind_speed',
    'wind_speed_alt_adjusted_abdalla_2012': 'wind_speed',
    'wind_speed_alt_adjusted_gourrion_2002': 'wind_speed',
    'wind_speed_rad': 'wind_speed',
    'total_column_liquid_water_content_rad': 'atmosphere_cloud_liquid_water_content',
    'sea_ice_fraction': 'sea_ice_fraction'
}

# fields related to the selected radar band (Ku or Ka) measurements
BAND = ['swh', 'swh_adjusted', 'swh_rms', 'swh_quality',
        'swh_num_valid', 'swh_rejection_flags',
        'sigma0', 'sigma0_adjusted', 'sigma0_rms', 'sigma0_num_valid',
        'mss', 'wind_speed_alt', 'wind_speed_alt_quality',
        'wind_speed_alt_adjusted_abdalla_2012',
        'wind_speed_alt_adjusted_gourrion_2002'
        ]

CONTENTTYPE = {
    'swh': 'physicalMeasurement',
    'swh_adjusted': 'physicalMeasurement',
    'swh_quality': 'qualityInformation',
    'swh_uncertainty': 'qualityInformation',
    'swh_rms': 'auxiliaryInformation',
    'swh_num_valid': 'auxiliaryInformation',
    'swh_rejection_flags': 'qualityInformation',
    'sigma0': 'physicalMeasurement',
    'sigma0_adjusted': 'physicalMeasurement',
    'sigma0_rms': 'auxiliaryInformation',
    'sigma0_num_valid': 'auxiliaryInformation',
    'mss': 'physicalMeasurement',
    'off_nadir_angle_wf': 'auxiliaryInformation',
    'range': 'auxiliaryInformation',
    'range_rms': 'auxiliaryInformation',
    'wind_speed_alt': 'physicalMeasurement',
    'wind_speed_alt_adjusted_abdalla_2012': 'physicalMeasurement',
    'wind_speed_alt_adjusted_gourrion_2002': 'physicalMeasurement',
    'wind_speed_rad': 'physicalMeasurement',
    'wind_speed_alt_quality': 'qualityInformation',
    'wind_speed_rad_quality': 'qualityInformation',
    'total_column_liquid_water_content_rad': 'physicalMeasurement',
    'sea_ice_fraction': 'auxiliaryInformation',
}

FLAG_VALUES = {
    'swh_quality': [0, 1, 2, 3]
}
FLAG_MASKS = {
    'swh_rejection_flags': [1, 2, 4, 8, 16, 32, 64, 128]
}

NO_MISSING_VALUE = [
    'swh_quality',
    'swh_rejection_flags',
    'swh_num_valid',
    'sigma0_num_valid',
    'wind_speed_alt_quality',
]

FLAG_MEANINGS = {
    'swh_quality': ' '.join(['undefined', 'bad', 'acceptable', 'good']),
    'swh_rejection_flags': ' '.join([
        'not_water',
        'sea_ice',
        'swh_validity',
        'sigma0_validity',
        'waveform_validity',
        'ssh_validity',
        'swh_rms_outlier',
        'swh_outlier',
    ])
}

ANCILLARY = {
    'swh': 'swh_quality swh_rejection_flags',
    'swh_adjusted': 'swh_quality swh_rejection_flags',
    'sigma0': 'sigma0_quality',
    'sigma0_adjusted': 'sigma0_quality',
    'wind_speed_alt': 'wind_speed_alt_quality',
    'wind_speed_rad': 'wind_speed_rad_quality',
    'wind_speed_alt_adjusted_abdalla_2012': 'wind_speed_alt_quality',
    'wind_speed_alt_adjusted_gourrion_2002': 'wind_speed_alt_quality',
}

COMMENT = {
    'swh': 'All instrumental corrections included. As available from retracker and unedited.',
    'swh_adjusted': 'All instrumental corrections included. Adjusted and unedited',
    'swh_uncertainty': 'Standard error calculated from buoy colocations',
    'sigma0': 'All instrumental corrections included. As available from retracker and unedited..',
    'sigma0_adjusted': 'All instrumental corrections included. Adjusted and unedited.',
}

UNITS = {
    'sigma0': 'dB',
    'sigma0_adjusted': 'dB',
    'sigma0_rms': 'dB',
    'sigma0_num_valid': '1',
    'mss': 'm2',
    'swh': 'm',
    'swh_adjusted': 'm',
    'swh_filtered': 'm',
    'swh_uncertainty': 'm',
    'swh_rms': 'm',
    'swh_num_valid': '1',
    'off_nadir_angle_wf': 'degree2',
    'wind_speed_alt': 'm s-1',
    'wind_speed_alt_adjusted_abdalla_2012': 'm s-1',
    'wind_speed_alt_adjusted_gourrion_2002': 'm s-1',
    'wind_speed_rad': 'm s-1',
    'total_column_liquid_water_content_rad': 'kg m-2',
    'sea_ice_fraction': '1',
}

DTYPE = {
    'sigma0': numpy.float64,
    'sigma0_adjusted': numpy.float64,
    'sigma0_rms': numpy.float64,
    'sigma0_num_valid': numpy.uint8,
    'mss': numpy.float64,
    'swh': numpy.float64,
    'swh_adjusted': numpy.float64,
    'swh_filtered': numpy.float64,
    'swh_quality': numpy.uint8,
    'swh_uncertainty': numpy.float64,
    'swh_rms': numpy.float64,
    'swh_num_valid': numpy.uint8,
    'swh_rejection_flags': numpy.uint16,
    'off_nadir_angle_wf': numpy.float64,
    'range': numpy.float64,
    'range_rms': numpy.float64,
    'wind_speed_alt': numpy.float64,
    'wind_speed_alt_adjusted_abdalla_2012': numpy.float64,
    'wind_speed_alt_adjusted_gourrion_2002': numpy.float64,
    'wind_speed_rad': numpy.float64,
    'wind_speed_alt_quality': numpy.uint8,
    'wind_speed_rad_quality': numpy.uint8,
    'total_column_liquid_water_content_rad': numpy.float64,
    'sea_ice_fraction': numpy.float64,
}

PRECISION = {
    'sigma0': 3,
    'sigma0_adjusted': 3,
    'sigma0_rms': 3,
    'sigma0_num_valid': 0,
    'mss': 3,
    'swh': 3,
    'swh_adjusted': 3,
    'swh_filtered': 3,
    'swh_quality': 0,
    'swh_uncertainty': 3,
    'swh_rms': 3,
    'swh_num_valid': 0,
    'range': 3,
    'range_rms': 3,
    'swh_rejection_flags': 0,
    'off_nadir_angle_wf': 3,
    'wind_speed_alt': 3,
    'wind_speed_alt_adjusted_abdalla_2012': 3,
    'wind_speed_alt_adjusted_gourrion_2002': 3,
    'wind_speed_rad': 3,
    'wind_speed_alt_quality': 0,
    'wind_speed_rad_quality': 0,
    'total_column_liquid_water_content_rad': 3,
    'sea_ice_fraction': 2
}

RELATED_ATTR = {
    'BAND': 'band',
    'COMMENT': 'comment',
    'PRECISION': 'least_significant_digit',
    'ANCILLARY': 'ancillary_variables',
    'FLAG_VALUES': 'flag_values',
    'FLAG_MEANINGS': 'flag_meanings',
    'FLAG_MASKS': 'flag_masks',
    'CONTENTTYPE': 'coverage_content_type'
}

VARTTRIBUTES = OrderedDict([
    ('COMMENT', COMMENT),
    ('PRECISION', PRECISION),
    ('ANCILLARY', ANCILLARY),
    ('FLAG_VALUES', FLAG_VALUES),
    ('FLAG_MEANINGS', FLAG_MEANINGS),
    ('FLAG_MASKS', FLAG_MASKS),
    ('CONTENTTYPE', CONTENTTYPE)
])

GLOBAL_ATTRIBUTES = {
    'Conventions': 'CF-1.7, ACDD-1.3, ISO 8601',
    'title': 'ESA CCI Sea State L2P derived from %s GDR',
    'id': 'ESACCI-SEASTATE-L2P-SWH-%s',
    'institution': "Institut Francais de Recherche pour l'Exploitation de la mer / CERSAT, European Space Agency",
    'institution_abbreviation': 'Ifremer/Cersat, ESA',
    'source': 'CCI Sea State %s GDR to L2P Processor',
    'history': '%s - Creation',
    'references': 'CCI Sea State Product Specification Document (PSD), v1.1',
    'product_version': '1.0',
    'summary': 'This dataset contains along-track significant wave height measurements from %s altimeter, cross-calibrated with other altimetry missions and reference in situ measurements.',
    'keywords': ['Oceans > Ocean Waves > Significant Wave Height',
                 'Oceans > Ocean Waves > Sea State'
                 ],
    'keywords_vocabulary': 'NASA Global Change Master Directory (GCMD) Science Keywords',
    'naming_authority': 'fr.ifremer.cersat',
    'cdm_data_type': 'trajectory',
    'featureType': 'trajectory',
    'comment': 'These data were produced at ESACCI as part of the ESA SST CCI project.',
    'creator_name': 'Cersat',
    'creator_url': 'http://cersat.ifremer.fr',
    'creator_email': 'cersat@ifremer.fr',
    'creator_institution': 'Ifremer / Cersat',
    'project': 'Climate Change Initiative - European Space Agency',
    'geospatial_lat_min': -80.,
    'geospatial_lat_max': 80.,
    'geospatial_lat_units': 'degree_north',
    'geospatial_lon_min': -180.,
    'geospatial_lon_max': 180.,
    'geospatial_lon_units': 'degree_east',
    'standard_name_vocabulary': 'NetCDF Climate and Forecast (CF) Metadata Convention version 1.7',
    'license': 'ESA CCI Data Policy: free and open access',
    'platform': '',
    'platform_type': 'low earth orbit satellite',
    'platform_vocabulary': 'CCI',
    'instrument': '',
    'instrument_type': 'altimeter',
    'instrument_vocabulary': 'CCI',
    'spatial_resolution': '',
    'cycle_number': '',
    'relative_pass_number': '',
    'equator_crossing_time': '',
    'equator_crossing_longitude': '',
    'netcdf_version_id': getlibversion(),
    'acknowledgement': 'Please acknowledge the use of these data with the following statement: these data were obtained from the ESA CCI Sea State project',
    'format_version': 'Data Standards v2.1',
    #'processing_software': '%s GDR to L2P Processor %s',
    'processing_level': 'L2P',
    'track_id': '',
    'publisher_name': 'Cersat',
    'publisher_url': 'cersat.ifremer.fr',
    'publisher_email': 'cersat@ifremer.fr',
    'publisher_institution': 'Ifremer / Cersat',
    'scientific_support_contact': 'Guillaume.Dodet@ifremer.fr',
    'technical_support_contact': 'cersat@ifremer.fr',
    'key_variables': 'swh_adjusted_denoised'
}


# size in km of the sliding window SWH outlier filter
WINDOW_IN_KM = 100000.

# required minimum of valid measurements in above window
MIN_NB_OF_VALID_POINTS = 4


class L2P(object):

    def __init__(self, configuration):
        self.satellite = self.satellite()
        self.band = self.band()
        self.instrument = self.instrument()
        self.configuration = configuration

    def init_l2p(self, time, lat, lon):
        obj = Trajectory(xr.Dataset(
            coords={
                'lat': xr.DataArray(data=lat, dims=['time']),
                'lon': xr.DataArray(data=lon, dims=['time']),
                'time': xr.DataArray(data=time, dims=['time'])}))

        for v, longname in self.l2p_vars().items():
            # field attributes
            stdname = None
            if v in STANDARDNAME:
                stdname = STANDARDNAME[v]
            units = None
            if v in UNITS:
                units = UNITS[v]
            attrs = OrderedDict([])
            for _, attlist in VARTTRIBUTES.items():
                if v in attlist:
                    attrs[RELATED_ATTR[_]] = attlist[v]
            if v in BAND:
                attrs['band'] = self.band
            attrs['coordinates'] = 'time lon lat'

            no_missing_value = False
            if v in NO_MISSING_VALUE:
                no_missing_value = True

            # field
            field = Field(
                name=v,
                description=longname,
                standard_name=(stdname, AUTHORITY),
                dims=OrderedDict([('time', len(time))]),
                dtype=DTYPE[v],
                units=units,
                attrs=attrs,
                data=None,
                no_missing_value=no_missing_value
            )
            obj.add_field(field)

        # customize rejection flag if needed
        obj.get_field('swh_rejection_flags').attrs['flag_meanings'] = \
            self.swh_rejection_flags()

        # global attributes
        obj.attrs.update(GLOBAL_ATTRIBUTES)
        creation_time = datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S')
        obj.attrs['date_created'] = creation_time
        obj.attrs['date_modified'] = creation_time
        obj.attrs['instrument'] = self.instrument
        obj.attrs['platform'] = self.satellite
        obj.attrs['instrument'] = self.instrument
        obj.attrs['track_id'] = str(uuid.uuid4())
        obj.attrs['band'] = self.band
        obj.attrs['source'] = (
            obj.attrs['source'] % self.satellite
        )
        obj.attrs['source_version'] = VERSION
#         obj.metadata['processing_software'] = (
#             obj.metadata['processing_software'] % (self.satellite, VERSION)
#             )
        obj.attrs['history'] = (
            [obj.attrs['history'] % datetime.datetime.now()]
        )
        obj.attrs['summary'] = (
            obj.attrs['summary'] % self.satellite
        )
        obj.attrs['id'] = (
            obj.attrs['id'] % self.satellite
        )
        obj.attrs['title'] = (
            obj.attrs['title'] % self.satellite
        )
        obj.attrs['spatial_resolution'] = self.resolution()

        self.l2p = obj

    def gdr_track_sea_ice_concentration(self, retracked_fname, aux_path):
        return self.gdr_track_aux(retracked_fname, aux_path, 'seaice')

    def gdr_track_dist2coast(self, retracked_fname, aux_path):
        return self.gdr_track_aux(retracked_fname, aux_path, 'dist2coast')

    def gdr_track_landmask(self, retracked_fname, aux_path):
        return self.gdr_track_aux(retracked_fname, aux_path, 'landmask')

    def gdr_track_bathymetry(self, retracked_fname, aux_path):
        return self.gdr_track_aux(retracked_fname, aux_path, 'bathy')

    def gdr_track_era5(self, retracked_fname, aux_path):
        return self.gdr_track_aux(retracked_fname, aux_path, 'era5')

    def gdr_track_era5_swh(self, retracked_fname, aux_path):
        return self.gdr_track_aux(retracked_fname, aux_path, 'era5_swh')

    def gdr_track_sealevel(self, fname, aux_path):
        return self.gdr_track_aux(fname, aux_path, 'sealevel')

    def gdr_track_denoising(self, fname, aux_path):
        return self.gdr_track_aux(fname, aux_path, 'emd')

    @classmethod
    def swh_rejection_flags(cls):
        return FLAG_MEANINGS['swh_rejection_flags']

    @classmethod
    def l2p_vars(cls):
        return VARIABLES

    @classmethod
    def retracked_1hz_feature(cls, fname):
        """return input GDR feature as a cerbere trajectory"""
        raise NotImplementedError

    @abstractmethod
    def satellite(self):
        raise NotImplementedError

    @abstractmethod
    def instrument(self):
        raise NotImplementedError

    @abstractmethod
    def band(self):
        raise NotImplementedError

    @abstractmethod
    def resolution(self):
        raise NotImplementedError

    @abstractmethod
    def cycle(self, track):
        raise NotImplementedError

    @abstractmethod
    def relative_pass(self, track):
        raise NotImplementedError

    @abstractmethod
    def equator_crossing_time(self, track):
        raise NotImplementedError

    @abstractmethod
    def equator_crossing_longitude(self, track):
        raise NotImplementedError

    @classmethod
    def get_cci_name(cls, satellite, start):
        """
        return CCI a compliant file name

        example: ESACCI-SEASTATE-L2P-SWH-JASON2-20170130T145103-fv01.nc
        """
        return 'ESACCI-SEASTATE-L2P-SWH-{}-{}-fv01.nc'.format(
            satellite.replace(' ', '_'),
            start.strftime('%Y%m%dT%H%M%S')
        )

    @abstractmethod
    def get_native_name(self, fieldname):
        """
        Return the GDR equivalent field name
        """
        raise NotImplementedError

    @abstractmethod
    def set_swh_quality(self, track):
        """
        set the quality information on SWH
        """
        raise NotImplementedError

    @abstractmethod
    def set_wind_speed_alt_quality(self, track):
        """
        set the quality information on altimeter wind speed
        """
        raise NotImplementedError

    @abstractmethod
    def set_wind_speed_rad_quality(self, track):
        """
        set the quality information on altimeter wind speed
        """
        raise NotImplementedError

    def copy_source_values(self, intrack):
        """
        Copy the source values from GDR to L2P corresponding fields.
        """
        for field in SOURCE_FIELDS:
            nname = self.get_native_name(field)
            if nname is not None:
                self.l2p.get_field(field).set_values(
                    intrack.get_field(nname).get_values()
                )
            else:
                logging.warning("Could not fill in %s. Not existing." % field)

    def set_mask_bit(self, flagfield, indices, meanings):
        """
        Update a flag mask by setting a list of bits.

        The bits are given by their meaning (not their order).
        """
        values = flagfield.get_values()

        allmeanings = flagfield.attrs['flag_meanings']
        if isinstance(allmeanings, str):
            allmeanings = [_ for _ in allmeanings.split(' ') if _ != '']

        # transform mask attributes to list if it is not the case
#         allmasks = flagfield.attributes['flag_masks']
#         if isinstance(allmasks, str):
#             allmasks = [_ for _ in allmasks.split(' ') if _ != '']

        criteria = meanings
        if isinstance(meanings, str):
            criteria = [meanings]

        # calculate sum (and) of all mask bits requested to be set
        setdata = values[indices]
        for criterium in criteria:
            try:
                bit = allmeanings.index(criterium)
            except:
                raise ValueError("Unknown flag meaning : {}".format(criterium))
            setdata = setdata | (1 << bit)

        # update field
        values[indices] = setdata

    @abstractmethod
    def adjusted_swh(self, track):
        """compute calibrated SWH"""
        raise NotImplementedError

    def _get_rms_table_name(self):
        raise NotImplementedError

    def _coeff_std(self):
        raise NotImplementedError

    def _get_midrange_rms_poly(self):
        raise NotImplementedError

    def test_sea_ice(self, track):
        # set sea ice concentration
        aux_path = self.configuration['aux_path']
        self.l2p.set_values(
            'sea_ice_fraction',
            self.gdr_track_sea_ice_concentration(
                    track.url, aux_path).get_values('sea_ice_fraction'))

        seaice = self.l2p.get_values('sea_ice_fraction')
        shw_quality = self.l2p.get_values('swh_quality')

        # set low ice fraction as suspect only
        low_conc = ((shw_quality == QUALITY_GOOD) &
                    (seaice > 0.) & (seaice <= 0.1))
        shw_quality[low_conc] = QUALITY_ACCEPTABLE

        # set bad if ice contaminated
        high_conc = ((shw_quality > QUALITY_UNDEFINED) & (seaice > 0.1))
        shw_quality[high_conc] = QUALITY_BAD

        self.set_mask_bit(self.l2p.get_field('swh_rejection_flags'),
                          seaice > 0,
                          'sea_ice')

    @abstractmethod
    def read_rms_threshold_table(self, rms_table_name):
        """read RMS threshold table"""
        raise NotImplementedError

    @abstractmethod
    def test_swh_rms_threshold(self, track, **kwargs):
        raise NotImplementedError

    def filter_outliers(
            self,
            track,
            window_in_km=WINDOW_IN_KM,
            min_valid_count=MIN_NB_OF_VALID_POINTS,
            coeff_std=3.9
    ):
        """
        Filter extreme points.
        """
        lat = track.get_lat()
        lon = track.get_lon()
        swh_ku = self.l2p.get_values('swh')
        quality = self.l2p.get_values('swh_quality')
        rejection = self.l2p.get_field('swh_rejection_flags')

        # rejection bit arrays
        t_outliers_pts = numpy.zeros((len(lat),), dtype=bool)
        t_5m_pts = numpy.zeros((len(lat),), dtype=bool)

        # calculate distance between consecutive measurements
        distfunc = numpy.vectorize(get_distance)
        inter_dists = distfunc(
            lat,
            lon,
            numpy.roll(lat, -1),
            numpy.roll(lon, -1)
        ).filled(0.)
        #print inter_dists

        # calculate neighbour windows
        windows = numpy.ma.zeros((len(lat), 2), dtype=numpy.int32)
        good = numpy.where(quality == QUALITY_GOOD)[0]
        for ind in good:

            # extends the window until end-to-end distance is more than the
            # window size (stop before it happens to keep within size)
            iprev = ind
            if iprev == 0:
                iprev = None
            inext = ind
            if inext == len(inter_dists) - 1:
                inext = None
            distsum = 0.
            windowsize = 0.
            window = [ind, ind]
            while ((windowsize < window_in_km)
                    & (distsum < 2 * window_in_km)
                    & ~(iprev is None and inext is None)):

                if iprev is not None:
                    newsum = distsum + inter_dists[iprev - 1]
                    if newsum < 2 * window_in_km:
                        distsum = newsum
                        iprev -= 1
                        if quality[iprev] == QUALITY_GOOD:
                            windowsize = inter_dists[iprev:window[1] - 1].sum()
                            if windowsize < window_in_km:
                                window[0] = iprev

                        if (iprev <= 0) | (windowsize >= window_in_km):
                            iprev = None
                    else:
                        iprev = None

                if inext is not None:
                    newsum = distsum + inter_dists[inext + 1]
                    if newsum < 2 * window_in_km:
                        distsum = newsum
                        inext += 1
                        if quality[inext] == QUALITY_GOOD:
                            windowsize = inter_dists[window[0]:inext - 1].sum()
                            if windowsize < window_in_km:
                                window[1] = inext

                        if (inext == len(lat) - 1) | (windowsize >= window_in_km):
                            inext = None
                    else:
                        inext = None

            windows[ind, 0] = window[0]
            windows[ind, 1] = window[1] + 1

        # loop 3 times max
        for _ in range(0, 3):
            good = numpy.where(quality == QUALITY_GOOD)[0]

            for ind in good:

                wstart = windows[ind, 0]
                wend = windows[ind, 1]

                # selection of measurements
                sswh = swh_ku[wstart:wend][quality[wstart:wend]
                                           == QUALITY_GOOD]

                # required minimum of valid measurements
                if sswh.count() <= min_valid_count:
                    quality[ind] = QUALITY_BAD
                    t_outliers_pts[ind] = True
                    continue

                # remove the N points deviating the most from the mean
                N = 2
                sswh = sswh[numpy.ma.fabs(sswh - sswh.mean()).argsort()][:-N]

                # current point statistics
                # - mean
                # - standard deviation
                cmean = sswh.mean()
                cstd = sswh.std()

                # apply filters
                if (numpy.ma.abs(swh_ku[ind] - cmean) > (coeff_std * cstd)):
                    quality[ind] = QUALITY_BAD
                    t_outliers_pts[ind] = True
                elif (numpy.ma.fabs(swh_ku[ind] - cmean) > 5):
                    quality[ind] = QUALITY_BAD
                    t_5m_pts[ind] = True

        self.set_mask_bit(rejection,
                          t_outliers_pts | t_5m_pts,
                          'swh_outlier')

    @abstractmethod
    def adjust_sigma0(self, track):
        raise NotImplementedError

    def set_adjusted_sigma0(self, track):
        asig0 = self.l2p.get_field('sigma0_adjusted')
        asig0.set_values(self.adjust_sigma0(track))
        adj_formula, adj_ref = self.sigma0_adjustment_attributes()
        asig0.attrs['adjustment_formula'] = adj_formula
        asig0.attrs['adjustment_reference'] = adj_ref

    def set_adjusted_swh(self):
        raise NotImplementedError

    def swh_uncertainty(self):
        raise NotImplementedError

    def set_swh_uncertainty(self):
        unc = self.l2p.get_field('swh_uncertainty')
        unc.set_values(self.swh_uncertainty())
        formula, ref = self.swh_uncertainty_attributes()
        unc.attrs['formula'] = formula
        unc.attrs['reference'] = ref

    def swh_uncertainty_attributes(cls):
        raise NotImplementedError

    def sigma0_adjustment_attributes(self):
        raise NotImplementedError

    def correct_sigma0_for_wspd(self, track):
        raise NotImplementedError

    def wind_speed_abdalla(self):
        """
        Compute adjusted wind speed from sigma0, Abdalla 2012

        https://www.tandfonline.com/doi/pdf/10.1080/01490419.2012.718676?needAccess=true
        """
        sigma0 = self.correct_sigma0_for_wspd()
        if sigma0 is None:
            logging.warning("Could not calculate Abdalla wind speed")
            return

        alpha = 46.5
        beta = 3.6
        gamma = 1690
        delta = 0.5
        sigb = 10.917

        # first guess
        um = numpy.ma.masked_all(len(sigma0))
        ind = sigma0 <= sigb
        um[ind] = alpha - beta * sigma0[ind]
        um[~ind] = gamma * numpy.ma.exp(-delta * sigma0[~ind])

        # u10
        u10 = (
            um + 1.4
            * numpy.ma.power(um, 0.096)
            * numpy.ma.exp(-0.32 * numpy.ma.power(um, 1.096))
        )
        field = self.l2p.get_field('wind_speed_alt_adjusted_abdalla_2012')
        field.set_values(u10)

    def wind_speed_gourrion(self, high_winds='young1993'):
        """
        Applies Gourrion et al. wind speed calculation based on sigma0
        and Hs
        """
        # calibrate swh and sigma0 to topex reference
        swh, sigma0 = self.topex_calibrated_sigma0()

        ao = -0.34336
        bo = 0.06909
        aHs = 0.08725
        bHs = 0.06374
        aU = 0.10000
        bU = 0.02844

        Wx = numpy.array([
            [-33.95062, -11.03394],
            [-3.93428, -0.05834]
        ])
        Wy = numpy.array([0.54012, 10.40481])
        Bx = numpy.array([[18.06378, -0.37228], ] * len(swh))
        By = numpy.array([-2.28387])

        P = numpy.array([
            ao + bo * sigma0,
            aHs + bHs * swh
        ])

        X = 1. / (1 + numpy.exp(-(Wx.dot(P) + Bx.T)))
        Y = 1. / (1 + numpy.exp(-(Wy.dot(X).T + By.T)))

        u10 = (Y - aU) / bU
        field = self.l2p.get_field('wind_speed_alt_adjusted_gourrion_2002')
        field.set_values(u10)
        return
    
        # correction for high wind speed
        if high_winds == 'young1993':
            # (Young, 1993)
            u10[u10 > 20.] = -6.4 * sigma0[u10 > 20.] + 72.

        elif high_winds == 'quilfen2011':
            # (Quilfen, 2011)
            # apply for NCRS < 10.7896
            j2toenv = (-2.7668 + 0.2024)
            sigma0 = self.l2p.get_values('sigma0_adjusted')

            lowsig = (sigma0 < (10.7896 + j2toenv))
            u10[lowsig] = 96.98 - 7.32 * (sigma0[lowsig] - j2toenv)

        else:
            raise ValueError("Unknown parameterization for high winds")

        field = self.l2p.get_field('wind_speed_alt_adjusted_gourrion_2002')
        field.set_values(u10)

    def topex_calibrated_sigma0(self):
        """for the period 1996-1997 (NSCAT colocation time frame.

        This is precalibration required to apply Gourrion wind speed calculation.
        Uses Queffeulou corrections.
        """
        # relevant Topex cycle for NSCAT era
        scycle = 146
        ecycle = 176

        # SWH intercalibration to Topex
        swh = self.l2p.get_values('swh_adjusted')

        # calibration offset = poly3(98) - poly3(cycle) for 98 <= cycle <= 235
        # we calculate the mean correction over NSCAT era
        poly = numpy.poly1d(
            [6.9624e-8, -7.7894e-6, -6.0426e-4, 0.0864]
        )
        dtopex = - 0.0766 + \
            (poly(98) - poly(numpy.arange(scycle, ecycle + 1, 1))).mean()
        swh2topex = (swh - dtopex) / 1.0539
        
        # sigma0 intercalibration to topex
        sigma0 = self.l2p.get_values('sigma0_adjusted')

        # for cycle 133 and greater, from Hayne and Hancock, July 1999.
        # + Queffeulou correction to Envisat (-0.4739 dB)

        # read threshold table
        topex_sigma_cor = resource_filename(
            'cciseastate',
            'resources/topex_ku_sigma0_cal.txt'
        )
        with open(topex_sigma_cor) as cal_file:
            caltable = numpy.ma.fix_invalid(
                map(float, cal_file.read().split())
            )
        caltable = numpy.reshape(caltable, (len(caltable)/2, 2))

        calibration = caltable[
            numpy.where((caltable[:, 0] >= float(scycle))
                        & (caltable[:, 0] <= float(ecycle)))
        ].mean() - 0.4739
        sigma02topex = sigma0 - calibration

        return swh2topex, sigma02topex

    def merge_auxiliaries(self, track):
        """add ancillary fields"""
        # bathymetry
        try:
            dst = self.gdr_track_bathymetry(
                track.url, self.configuration['aux_path'])
            self.l2p.add_field(dst.get_field('bathymetry').clone())
        except IOError:
            raise IOError("bathymetry file could not be found.")

        # distance to coast
        try:
            dst = self.gdr_track_dist2coast(
                track.url, self.configuration['aux_path'])
            self.l2p.add_field(dst.get_field('distance_to_coast').clone())

        except IOError:
            raise IOError("dist2coast file could not be found.")

        # sea level
        try:
            dst = self.gdr_track_sealevel(
                track.url, self.configuration['aux_path'])
            for name in ['sla_unfiltered', 'mdt']:
                self.l2p.add_field(dst.get_field(name).clone())
        except IOError:
            logging.error("sea level file could not be found.")

        # era5
        try:
            dst = self.gdr_track_era5(
                track.url, self.configuration['aux_path'])
            for name in ['wind_speed_model_u', 'wind_speed_model_v',
                         'surface_air_pressure', 'surface_air_temperature']:
                self.l2p.add_field(dst.get_field(name).clone())
                self.l2p.get_field(name).precision = 3
        except IOError:
            raise IOError("ERA5 file could not be found.")

        # era5 SWH
        try:
            dst = self.gdr_track_era5_swh(
                track.url, self.configuration['aux_path'])
            for name in ['swh_model',
                         'mean_wave_period_model',
                         'peak_wave_period_model',
                         'mean_wave_direction_model',
                         'primary_swell_direction_model',
                         'primary_swell_swh_model']:
                self.l2p.add_field(dst.get_field(name).clone(),
                                   force_index=True)
                self.l2p.get_field(name).precision = 3
        except IOError:
            raise IOError("ERA5 SWH file could not be found.")

    def _denoise(self, swh):
        return denoise(
            self.l2p,
            fieldname='swh_adjusted',
            values=swh,
            check_times=True,
            time_threshold=2,
            N=3,
            T_mult=.75)

    def denoise(self, track, save=False):
        swh = self.l2p.get_values('swh_adjusted')
        swh_quality = self.l2p.get_values('swh_quality')

        # estimate segments on quality
        swh = numpy.ma.masked_where(swh_quality != 3, swh)

        # calculate denoised swh
        dswh = self._denoise(swh)

        # save if required
        if save:
            aux_fname = (
                self.configuration['aux_path']
                / Path(self.satellite.lower())
                / Path(track.parts[-3])
                / Path(track.parts[-2])
                / track.name.replace('.nc', '_{}.nc'.format('emd'))
            )
            if aux_fname.exists():
                aux_fname.unlink()
            dswh.save(aux_fname)

        for name in [
                'swh_adjusted_denoised',
                'swh_adjusted_denoised_uncertainty',
                'swh_adjusted_noise']:
            field = Field(
                name=name.replace('_adjusted', ''),
                dims=dswh.get_field(name).dims,
                description=dswh.get_field(name).description,
                units=dswh.get_field(name).units,
                data=dswh.get_values(name).astype(numpy.float32),
                attrs=dswh.get_field(name).attrs,
                precision=3)
            field.attrs['comment'] = (
                "EMD denoising by Quilfen et al. applied to swh_adjusted variable"
            )
            if name != 'swh_adjusted_denoised':
                field.attrs.pop('standard_name')
            self.l2p.add_field(field)

    def process(self, fname, outrootdir):

        intrack = self.retracked_1hz_feature(fname)

        if intrack.get_lat().count() == 0:
            logging.warning(
                "The file was not processed because it has no valid data."
            )
            exit(0)

        # create output feature
        self.init_l2p(
            intrack.get_times(),
            intrack.get_lat(),
            intrack.get_lon()
        )

        # update dynamic attributes
        self.l2p.attrs['cycle_number'] = self.cycle(intrack)
        self.l2p.attrs['relative_pass_number'] = self.relative_pass(intrack)
        eqtime = self.equator_crossing_time(intrack)
        if eqtime is not None:
            self.l2p.attrs['equator_crossing_time'] = eqtime
        eqlon = self.l2p.attrs['equator_crossing_longitude']
        if eqlon is not None:
            self.l2p.attrs['equator_crossing_longitude'] = eqlon

        # copy source fields
        self.copy_source_values(intrack)

        # add quality information
        self.set_swh_quality(intrack)

        # set ice flag
        self.test_sea_ice(intrack)

        # test SWH RMS
        self.test_swh_rms_threshold(intrack)

        # filter outliers
        self.filter_outliers(intrack, coeff_std=self._coeff_std())

        # adjust SWH
        self.set_adjusted_swh()

        # uncertainty
        self.set_swh_uncertainty()

        # denoising
        self.denoise(intrack)

        # adjust sigma0
        self.set_adjusted_sigma0(intrack)

        # wind speed quality
        self.set_wind_speed_alt_quality(intrack)
        self.set_wind_speed_rad_quality(intrack)

        # re-calculate corrected wind speed
        self.wind_speed_gourrion(high_winds='quilfen2011')
        self.wind_speed_abdalla()

        # merge with auxiliary fields
        self.merge_auxiliaries(intrack)

        # global attributes
        self.l2p.attrs['input'] = (
                "GDR product: %s" % os.path.basename(fname)
        )
        self.l2p.attrs['processing_software'] = self.processor()

        # float clipping
        #         for f in self.l2p.get_fieldnames():
        #             if f in PRECISION:
        #                 field = self.l2p.get_field(f)
        #                 scale = 10**PRECISION[f]
        #                 field.set_values(
        #                     numpy.round(field.get_values() * scale) / scale
        #                     )

        # open output file
        outdir = os.path.join(
            outrootdir,
            intrack.time_coverage_start.strftime('%Y/%j')
        )
        if not os.path.exists(outdir):
            os.makedirs(outdir)

        outfname = os.path.join(
            outdir,
            self.get_cci_name(self.satellite, intrack.time_coverage_start)
        )
        logging.info('Output L2P file : %s' % outfname)
        if os.path.exists(outfname):
            os.remove(outfname)

        self.l2p.save(outfname, force_profile=True)
