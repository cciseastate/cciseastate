"""
Base processor for the L4 product generation


:copyright: Copyright 2019 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
from collections import OrderedDict
import copy
import datetime
from dateutil.relativedelta import relativedelta
import glob
import logging
import os
import uuid
import time

import cftime
import numpy
from scipy.stats import binned_statistic_2d
import shapely.geometry
import xarray

from netCDF4 import getlibversion

import cerbere.cfconvention
from cerbere.dataset.field import Field
from cerbere.feature.grid import CylindricalGrid
from cerbere.dataset.ncdataset import NCDataset


VERSION = "1.0"

# grid properties
BINS = [160, 360]
RANGES = [[-80, 80], [-180., 180.]]
RESOLUTION = 1.

# minimum number of measurements in a bin
MIN_SAMPLE = 4


GLOBAL_ATTRIBUTES = {
    'Conventions': 'CF-1.7, ACDD-1.3, ISO 8601',
    'title': 'ESA CCI Sea State L4',
    'id': 'ESACCI-SEASTATE-L4-SWH',
    'institution': "Institut Francais de Recherche pour l'Exploitation de la mer / CERSAT, European Space Agency",
    'institution_abbreviation': 'Ifremer/Cersat, ESA',
    'source': 'CCI Sea State L4 Processor',
    'source_version': VERSION,
    'history': '%s - Creation',
    'references': 'CCI Sea State Product Specification Document (PSD), v1.1',
    'product_version': '1.0',
    'summary': 'This dataset contains gridded statistics of significant wave height from multiple altimeters, denoised and cross-calibrated.',
    'keywords': ', '.join(['Oceans > Ocean Waves > Significant Wave Height',
                 'Oceans > Ocean Waves > Sea State'
                 ]),
    'keywords_vocabulary': 'NASA Global Change Master Directory (GCMD) Science Keywords',
    'naming_authority': 'fr.ifremer.cersat',
    'cdm_data_type': 'grid',
    'featureType': 'Grid',
    'comment': 'These data were produced at ESACCI as part of the ESA SST CCI project.',
    'creator_name': 'Cersat',
    'creator_url': 'http://cersat.ifremer.fr',
    'creator_email': 'cersat@ifremer.fr',
    'creator_institution': 'Ifremer / Cersat',
    'project': 'Climate Change Initiative - European Space Agency',
    'geospatial_lat_min': -80.,
    'geospatial_lat_max': 80.,
    'geospatial_lat_units': 'degree_north',
    'geospatial_lon_min': -180.,
    'geospatial_lon_max': 180.,
    'geospatial_lon_units': 'degree_east',
    'time_coverage_resolution': '1M',
    'standard_name_vocabulary': 'NetCDF Climate and Forecast (CF) Metadata Convention version 1.7',
    'license': 'ESA CCI Data Policy: free and open access',
    'platform': '',
    'platform_type': 'low earth orbit satellite',
    'platform_vocabulary': 'CCI',
    'instrument': '',
    'instrument_type': 'altimeter',
    'instrument_vocabulary': 'CCI',
    'spatial_resolution': '1 degree',
    'netcdf_version_id': getlibversion(),
    'acknowledgement': 'Please acknowledge the use of these data with the following statement: these data were obtained from the ESA CCI Sea State project',
    'format_version': 'Data Standards v2.1',
    #'processing_software': '%s GDR to L2P Processor %s',
    'processing_level': 'L4',
    'track_id': '',
    'publisher_name': 'CERSAT',
    'publisher_url': 'cersat.ifremer.fr',
    'publisher_email': 'cersat@ifremer.fr',
    'publisher_institution': 'Ifremer / Cersat',
    'scientific_support_contact': 'Guillaume.Dodet@ifremer.fr',
    'technical_support_contact': 'cersat@ifremer.fr',
    'key_variables': 'swh_mean'
}

SWH_RANGES = [0.5, 1, 1.5, 2, 2.5, 3., 3.5, 4., 5., 6, 8, 10]

LONG_NAMES = {
    'swh_mean':'mean of median significant wave height values',
    'swh_rms':'rms of median significant wave height values',
    'swh_count':'number of median significant wave height values',
    'swh_sum':'total of median significant wave height values',
    'swh_squared_sum':'total of median significant wave height squared values',
    'swh_log_sum':'total of median significant wave height log values',
    'swh_log_squared_sum':'total of median significant wave height log squared values',
    'swh_count_greater_than_0.50':'number of median significant wave height values greater than 0.5m',
    'swh_count_greater_than_1.00':'number of median significant wave height values greater than 1.0m',
    'swh_count_greater_than_1.50':'number of median significant wave height values greater than 1.5m',
    'swh_count_greater_than_2.00':'number of median significant wave height values greater than 2.0m',
    'swh_count_greater_than_2.50':'number of median significant wave height values greater than 2.5m',
    'swh_count_greater_than_3.00':'number of median significant wave height values greater than 3.0m',
    'swh_count_greater_than_3.50':'number of median significant wave height values greater than 3.5m',
    'swh_count_greater_than_4.00':'number of median significant wave height values greater than 4.0m',
    'swh_count_greater_than_5.00':'number of median significant wave height values greater than 5.0m',
    'swh_count_greater_than_6.00':'number of median significant wave height values greater than 6.0m',
    'swh_count_greater_than_8.00':'number of median significant wave height values greater than 8.0m',
    'swh_count_greater_than_10.00':'number of median significant wave height values greater than 10.0m',
    'swh_max':'maximum median significant wave height value' 
    }

OPERATOR = {
    'swh_mean':'mean',
    'swh_rms':'root_mean_square',
    'swh_count':'count',
    'swh_sum':'sum',
    'swh_squared_sum':'sum_of_squares',
    'swh_log_sum':'sum_of_log',
    'swh_log_squared_sum':'sum_of_log_squares',
    'swh_count_greater_than_0.50':'count (interval: ',
    'swh_count_greater_than_1.00':'count',
    'swh_count_greater_than_1.50':'count',
    'swh_count_greater_than_2.00':'count',
    'swh_count_greater_than_2.50':'count',
    'swh_count_greater_than_3.00':'count',
    'swh_count_greater_than_3.50':'count',
    'swh_count_greater_than_4.00':'count',
    'swh_count_greater_than_5.00':'count',
    'swh_count_greater_than_6.00':'count',
    'swh_count_greater_than_8.00':'count',
    'swh_count_greater_than_10.00':'count',
    'swh_max':'maximum' 
    }

PLATFORM = {
    'ers-1': 'ERS-1',
    'ers-2': 'ERS-2',
    'topex': 'TOPEX',
    'envisat': 'Envisat',
    'jason-1': 'Jason-1',
    'jason-2': 'Jason-2',
    'jason-3': 'Jason-3',
    'gfo': 'GFO',
    'saral': 'Saral',
    'sentinel-3a': 'Sentinel-3A',
    'cryosat-2': 'CryoSat-2'
    }

INSTRUMENT = {
    'ERS-1': 'RA',
    'ERS-2': 'RA',
    'TOPEX': 'TOPEX',
    'Envisat': 'RA2',
    'Jason-1': 'Poseidon-2',
    'Jason-2': 'Poseidon-3',
    'Jason-3': 'Poseidon-3b',
    'GFO': 'GFO-RA',
    'Saral': 'AltiKa',
    'Sentinel-3A': 'SRAL',
    'CryoSat-2': 'SIRAL'
    }

BAND = {
    'ERS-1': 'Ku',
    'ERS-2': 'Ku',
    'TOPEX': 'Ku',
    'Envisat': 'Ku',
    'Jason-1': 'Ku',
    'Jason-2': 'Ku',
    'Jason-3': 'Ku',
    'GFO': 'Ku',
    'Saral': 'Ka',
    'Sentinel-3A': 'Ku',
    'CryoSat-2': 'Ku'
    }


def pass2grid(dataset):
    print(dataset.encoding['source'], dataset['time'].to_masked_array().min())
    return L4.passbin(dataset, bins=BINS, ranges=RANGES)


class L4(object):

    def __init__(self):
        pass

    @classmethod
    def passbin(cls,
            data,
            bins,
            ranges
            ):
        """Remap some fields from source onto target
        
        Args:

        """
        # preselect valid data
        swh = numpy.ma.fix_invalid(
            data['swh_denoised'].to_masked_array())
        ind = ~numpy.ma.getmaskarray(swh)
        
        lats = data['lat'].to_masked_array()[ind]
        lons = data['lon'].to_masked_array()[ind]
        swh = swh[ind]
 
        # bin the data and get the binning index
        stats = binned_statistic_2d(
                lats,
                lons,
                swh,
                statistic='median',
                bins=bins, 
                range=ranges,
                expand_binnumbers=False
                )
        # eliminate values where number of samples is lower than MIN_SAMPLE
        count = binned_statistic_2d(
                lats,
                lons,
                None,
                statistic='count',
                bins=bins, 
                range=ranges,
                expand_binnumbers=False
                ).statistic
        values = numpy.ma.masked_where(
            count < MIN_SAMPLE, stats.statistic, copy=False
            ) 
        
        data_as_xarr = xarray.Dataset(
            coords={'lat': stats.x_edge[:-1], 'lon': stats.y_edge[:-1]},
            data_vars={'swh': (('lat', 'lon'), values)}
            )

        data.close()

        return data_as_xarr

    
    @classmethod
    def bin(cls,
            data,
            operators,
            bins,
            ranges,
            min_nb_of_pixels_per_bin=0,
            ):
        """Remap some fields from source onto target
        
        Args:

        """
        # preselect valid data
        swh = numpy.ma.fix_invalid(
            data['swh_denoised'].to_masked_array())
        ind = ~numpy.ma.getmaskarray(swh)
        
        lats = data['lat'].to_masked_array()[ind]
        lons = data['lon'].to_masked_array()[ind]
        swh = swh[ind]
            
        # allocate data arrays for result grids
        data_grids = {}
        for op in operators:

            if op != 'count':
                data_grids[op] = numpy.ma.masked_all(
                    bins,
                    dtype=numpy.float64
                    )
            else:
                data_grids[op] = numpy.ma.zeros(bins, dtype=numpy.int32)   
        
        # bin the data and get the binning index
        logging.debug("start lat/lon binning")
        stats = binned_statistic_2d(
                lats,
                lons,
                swh,
                statistic='median',
                bins=bins, 
                range=ranges,
                expand_binnumbers=False
                )
        logging.debug("end lat/lon binning")
           
        # loop on each bin and fill in the output fields      
        bin_indices = numpy.unique(stats.binnumber)
        logging.debug("Number of bins : {}".format(len(bin_indices)))
        
        # indices in grid
        yy, xx = numpy.unravel_index(
            bin_indices, 
            (len(stats.x_edge) + 1, len(stats.y_edge) + 1)
            )
        yy -= 1
        xx -= 1

        for i in range(len(bin_indices)):

            selection = numpy.where(stats.binnumber == bin_indices[i])
            binx = xx[i]
            biny = yy[i]
            # ignore bins out of grid edges
            if binx < 0 or biny < 0 or binx >= bins[1] or biny >= bins[0]:
                print('Ignore ', i, selection, lats[selection], lons[selection])
                continue
            
            # get the number of valid pixels for the first field in the dict of
            # input fields
            count = data.count()

            # adjust minimum nb of pixels to latitude (for plate carree 
            # projection)
            min_sampling = max(
                1,
                numpy.cos(
                    lats[selection][0] * numpy.pi / 180.)
                * min_nb_of_pixels_per_bin)

            # assign values
            if count >= min_sampling:
                for op in operators:
                    if op == "count":
                        value = swh.count()
                    elif op == "mean":
                        value = swh.mean()
                    elif op == "std":
                        value = swh.std()
                    elif op == "median":
                        value = numpy.ma.median(swh)
                    elif op == "min":
                        value = swh.min()
                    elif op == "max":
                        value = swh.max()
                    elif op == "sum":
                        value = swh.sum()
                    elif op == "sum2":
                        value = numpy.ma.power(swh, 2).sum()
                    
                    opstr = "swh_%s" % op                                   
                    data_grids[opstr][biny, binx] = value
                    
                    if op == "count_":
                        for r, _ in enumerate(SWH_RANGES):
                            if r == len(SWH_RANGES) - 1:
                                value = (swh >= SWH_RANGES[-1]).sum()
                                opstr = "swh_count_greater_than_%.2f" % (
                                    SWH_RANGES[r])
                            else:
                                value = (
                                    (swh >= SWH_RANGES[r])
                                    & (swh < SWH_RANGES[r+1])
                                    ).sum()
                                opstr = "swh_count%.2f_to_%.2f" % (
                                    SWH_RANGES[r], SWH_RANGES[r+1])
                            data_grids[opstr][biny, binx] = value

        return data_grids

    def repeat_open(self, fname, counter):
        try:
            print(counter)
            return xarray.open_dataset(fname)
        except:
            time.sleep(5)
            print("Repeat open")
            return xarray.open_dataset(fname)

    def process(self, date, inrootdir, outrootdir, version):
        
        # loop on every pass file
        date_selection = date.strftime('*/%Y/*/*%Y%m??T*.nc')

        # select the pass files to open and bin them
        logging.debug("Opening files")
        stacked_data = xarray.concat(
            [pass2grid(self.repeat_open(_, counter))
             for counter, _ in enumerate(glob.glob(os.path.join(inrootdir,
                                                    date_selection)))],
            dim='time'
        )['swh'].to_masked_array()
        # stacked_data = xarray.open_mfdataset(
        #     os.path.join(inrootdir, date_selection),
        #     engine='netcdf4',
        #     #   combine='by_coords',
        #     #   concat_dim='time',
        #     combine='nested',
        #     concat_dim='time',
        #     preprocess=pass2grid,
        #     parallel=False
        # )['swh'].to_masked_array()
        
        # the result is a stack of binned pass files on which we can process
        # the statistics
        bindata = OrderedDict([])
        bindata['swh_mean'] = stacked_data.mean(axis=0)
        bindata['swh_rms'] = stacked_data.std(axis=0)
        bindata['swh_count'] = stacked_data.count(axis=0)
        bindata['swh_sum'] = stacked_data.sum(axis=0)
        bindata['swh_squared_sum'] = (stacked_data**2).sum(axis=0)
        bindata['swh_log_sum'] = numpy.ma.log(stacked_data).sum(axis=0)
        bindata['swh_log_squared_sum'] = (numpy.ma.log(stacked_data)**2).sum(axis=0)
        bindata['swh_max'] = stacked_data.max(axis=0)
        for i, _ in enumerate(SWH_RANGES):
            values = (stacked_data >= SWH_RANGES[i]).sum(axis=0)
            opstr = "swh_count_greater_than_%.2f" % (SWH_RANGES[i])
            bindata[opstr] = values

        # build cerbere object
        fields = OrderedDict([])
        dims = OrderedDict([('time', 1), ('lat', BINS[0]), ('lon', BINS[1])])

        crs = Field(
            numpy.ma.empty(shape=()),
            name='crs',
            dims=OrderedDict([]),
            dtype=numpy.int32,
            attrs={
                'grid_mapping_name': "latitude_longitude",
                'semi_major_axis': 6371000.0,
                'inverse_flattening': 0.
                })
        fields['crs'] = crs

        bndvalues = numpy.vstack(
            (numpy.arange(-80., 80., 1.),
            numpy.arange(-79., 81., 1.))
            ).T
        latbnds = Field(
            bndvalues,
            name='lat_bnds',
            dims=OrderedDict([('lat', 160), ('nv', 2)]),
            dtype=numpy.float64)
        fields['lat_bnds'] = latbnds

        bndvalues = numpy.vstack(
            (numpy.arange(-180., 180., 1.), numpy.arange(-179., 181., 1.))).T
        lonbnds = Field(
            bndvalues,
            name='lon_bnds',
            dims=OrderedDict([('lon', 360), ('nv', 2)]),
            dtype=numpy.float64)
        fields['lon_bnds'] = lonbnds
        
        start = datetime.datetime(date.year, date.month, 1, 0, 0, 0)
        end = start + relativedelta(months=1)
        bndvalues = numpy.expand_dims(
            numpy.array([numpy.datetime64(start), numpy.datetime64(end)]),
            axis=0)
        timebnds = Field(
            bndvalues,
            name='time_bnds',
            dims=OrderedDict([('time', 1), ('nv', 2)]),
            dtype=numpy.datetime64,
            #units=cerbere.cfconvention.DEFAULT_TIME_UNITS
        )
        fields['time_bnds'] = timebnds

        for fieldname in bindata:
            if 'squared' not in fieldname:
                units = 'm'
            else:
                units = 'm2'
            dtype = numpy.float64
            if 'count' in fieldname:
                dtype=numpy.int32
                units = '1'
            field = Field(
                numpy.expand_dims(bindata[fieldname], axis=0),
                name=fieldname,
                description=LONG_NAMES[fieldname],
                dims=dims,
                dtype=dtype,
                units=units,
                attrs={
                    'coverage_content_type': "physicalMeasurement",
                    'grid_mapping': "crs",
                    'cell_method': "lat: lon: median (interval: 0.1 degree_N interval: 0.1 degree_E comment: median per bin and satellite pass) time: %s over days" % OPERATOR[fieldname]
                    }
                )
            fields[fieldname] = field

        grid = CylindricalGrid(
            {'time': {'dims': ('time'), 'data': [date + relativedelta(days=1)]},
             'lat': {'dims': ('lat'), 'data': numpy.arange(-79.5, 80, 1.)},
             'lon': {'dims': ('lon',), 'data': numpy.arange(-179.5, 180, 1.)},
             **fields
             },
            attrs=GLOBAL_ATTRIBUTES
        )
        grid.bbox = shapely.geometry.box(-180., -80., 180., 80.)

        attrs = copy.copy(grid.get_coord('lat').attrs)
        attrs['bounds'] = 'lat_bnds'
        grid.get_coord('lat').attrs = attrs
        attrs = copy.copy(grid.get_coord('lon').attrs)
        attrs['bounds'] = 'lon_bnds'
        grid.get_coord('lon').attrs = attrs
        attrs = copy.copy(grid.get_coord('time').attrs)
        attrs['bounds'] = 'time_bnds'
        grid.get_coord('time').attrs = attrs

        # global attributes
        attrs = grid.attrs
        creation_time = datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S')
        attrs['date_created'] = creation_time
        attrs['date_modified'] = creation_time
        
        infiles = glob.glob(os.path.join(inrootdir, date_selection))
        platforms = [
            PLATFORM[platf]
            for platf in list(set([_.split('/')[-4] for _ in infiles]))
            ]
        attrs['platform'] = ', '.join(platforms)
        attrs['instrument'] = ', '.join([INSTRUMENT[_] for _ in platforms])
        attrs['track_id'] = str(uuid.uuid4())
        attrs['band'] = ', '.join([BAND[_] for _ in platforms])
        attrs['history'] = ', '.join(
            [attrs['history'] % datetime.datetime.now()]
        )
        attrs['time_coverage_start'] = start
        attrs['time_coverage_end'] = end
        attrs['uuid'] = attrs['track_id']

        attrs['product_version'] = version
        attrs['source_version'] = version
        
        # save file
        dirname = os.path.join(
            outrootdir,
            date.strftime('%Y')
            )
        if not os.path.exists(dirname):
            os.makedirs(dirname)
        fname = os.path.join(
            dirname,
            date.strftime('ESACCI-SEASTATE-L4-SWH-MULTI_1M-%Y%m-fv01.nc')
            )
        if os.path.exists(fname):
            logging.warning('deleting previous file {}'.format(fname))
            os.remove(fname)

        grid.save(fname)

