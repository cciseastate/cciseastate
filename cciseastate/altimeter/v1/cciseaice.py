"""
Sea ice concentration from CCI Sea Ice project
"""
import datetime
import logging
import os

import netCDF4 as netcdf
import numpy
import osr
import osgeo.gdal

# maximum time gap in days for sea ice concentration
MAX_TIME_GAP = 3


class CCISeaIce(object):
    """
    
    Args:
        rootpath (str): root path to CCI sea ice files
    """
    def __init__(self, rootpath, northpattern, southpattern):
        self.rootpath = rootpath
        self.northpattern = northpattern
        self.southpattern = southpattern
        
    
    def cci_gdal_raster(self, ds):
        """Read CCI data file with GDAL and returns a raster band with 
        concentration.
        """        
        # coordinate system
        srs = osr.SpatialReference()
        proj4str = str(
            ds.variables[ds.variables['ice_conc'].grid_mapping].proj4_string
            )
        srs.ImportFromProj4(proj4str)
        
        # read source data, as fraction instead of percentage
        data = ds.variables['ice_conc'][0, :, :]
        
        # extract GCPs
        lat0 = ds.variables["lat"][0, 0]
        lon0 = ds.variables["lon"][0, 0]
        lat1 = ds.variables["lat"][-1, -1]
        lon1 = ds.variables["lon"][-1, -1]
        lat2 = ds.variables["lat"][0, -1]
        lon2 = ds.variables["lon"][0, -1]
        lat3 = ds.variables["lat"][-1, 0]
        lon3 = ds.variables["lon"][-1, 0]
        maxLin,maxPix = ds.variables["lon"][:, :].shape
        
        srsLatLong = srs.CloneGeogCS()
        ct2=osr.CoordinateTransformation(srsLatLong, srs)
        x0,y0,z0 = ct2.TransformPoint(float(lon0),float(lat0))
        x1,y1,z1 = ct2.TransformPoint(float(lon1),float(lat1))
        x2,y2,z2 = ct2.TransformPoint(float(lon2),float(lat2))
        x3,y3,z3 = ct2.TransformPoint(float(lon3),float(lat3))
        gcp0 = osgeo.gdal.GCP( x0,y0,z0,0,0 )
        gcp1 = osgeo.gdal.GCP( x1,y1,z1,maxPix-1,maxLin-1 )
        gcp2 = osgeo.gdal.GCP( x2,y2,z2,maxPix-1,0 )
        gcp3 = osgeo.gdal.GCP( x3,y3,z3,0,maxLin-1 )
            
        # create raster in memory for source data from template
        driver = osgeo.gdal.GetDriverByName("Mem")
        outRaster = driver.Create("", maxPix, maxLin, 1, osgeo.gdal.GDT_Int16)
        outRaster.SetProjection( srs.ExportToWkt() )
        outRaster.SetGCPs( [gcp0,gcp1,gcp2,gcp3], srs.ExportToWkt() )
            
        # add concentration data band
        outband = outRaster.GetRasterBand(1)
        outband.WriteArray(data)
        
        return outRaster

    def make_regular_grid(self, raster, fillvalue):
        """Remap the sea ice concentration on a global regular grid""" 
        # Destination Grid
        srsgeo = osr.SpatialReference()
        srsgeo.ImportFromEPSG(4326)
        
        # remap in memory
        driver = osgeo.gdal.GetDriverByName("Mem")
        ods = driver.Create("", 3600, 1800, 1, osgeo.gdal.GDT_Int16)
        ods.SetGeoTransform([-179.95, 0.1, 0., -89.95, 0., 0.1])
        ods.SetProjection(srsgeo.ExportToWkt())

        osgeo.gdal.ReprojectImage(
            raster, ods,
            raster.GetProjection(), ods.GetProjection(),
            osgeo.gdal.GRA_NearestNeighbour
            )
        
        return numpy.ma.masked_values(
            ods.GetRasterBand(1).ReadAsArray(),
            fillvalue
            )
    
    def get_seaice_file(self, date, pattern):
        curdate = date
        while True:
            pfile = os.path.join(
                self.rootpath,
                curdate.strftime(pattern)
                )
            if os.path.exists(pfile):
                logging.info(
                    'Sea ice file: %s' % os.path.basename(pfile)
                    )
                return pfile
                        
            logging.warning(
                "Missing sea ice concentration file: %s" % pfile
                )
            curdate -= datetime.timedelta(days=1)
            if abs((curdate - date).days) > MAX_TIME_GAP:
                raise Exception(
                    "No ice concentration file could be found"
                )
    
    def set_concentration(self, feature):
        # get NH and SH files
        date = feature.get_start_time()

        nhfile = self.get_seaice_file(date, self.northpattern)
        shfile = self.get_seaice_file(date, self.southpattern)
        
        # read and remap       
        nhcf = netcdf.Dataset(nhfile)
        shcf = netcdf.Dataset(shfile)
        
        grid = self.make_regular_grid(
            self.cci_gdal_raster(nhcf),
            nhcf.variables['ice_conc']._FillValue
            )
        grid += self.make_regular_grid(
            self.cci_gdal_raster(shcf),
            nhcf.variables['ice_conc']._FillValue
            )
        grid = grid.filled(0)
                                      
        nhcf.close()
        shcf.close()

        # extract values over feature
        x = numpy.minimum(
            numpy.floor((feature.get_lon() + 180) / 0.1).astype(int),
            3599
            )
        y = numpy.minimum(
            numpy.floor((90. - feature.get_lat()) / 0.1).astype(int),
            1799
            )

        concentration = grid[::-1, :][y, x] / 100.

        field = feature.get_field('sea_ice_fraction')
        field.set_values(concentration)
        field.attributes['source'] = 'CCI Sea Ice'
        field.attributes['institution'] = 'ESA'
        field.attributes['source_files'] = [
            os.path.basename(nhfile),
            os.path.basename(shfile)
            ] 


