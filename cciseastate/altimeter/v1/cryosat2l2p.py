"""
Specific Cryosat-2 processing for the conversion of a GDR altimeter product to
CCI format and content.

Based on code from P. Queffeulou and C. Prevost

:copyright: Copyright 2019 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import os
import numpy
import datetime

from cerbere.feature.trajectory import Trajectory
from cerbere.dataset.ncdataset import NCDataset
from cciseastate.altimeter.v1.l2p import L2P
from cciseastate.altimeter.v1.l2p import QUALITY_GOOD, QUALITY_BAD, QUALITY_UNDEFINED



class CryoSat2L2P(L2P):
    
    def get_1hz_feature(self, fname):
        """return input GDR feature as a cerbere trajectory"""
        ncf = NCDataset(url=fname, mode='r')
        traj = Trajectory()
        traj.load(ncf)
        return traj

    def satellite(self):
        return 'CryoSat-2'

    def instrument(self):
        return 'SIRAL'

    def band(self):
        return 'Ku'

    def cycle(self, track):
        return track.get_metadata()['cycle_number']

    def relative_pass(self, track):
        return track.get_metadata()['pass_number']

    def equator_crossing_time(self, track):
        return track.get_metadata()['equator_time']

    def equator_crossing_longitude(self, track):
        lon = track.get_metadata()['equator_longitude']
        if lon > 180:
            lon -= 360.
        return lon

    def resolution(self):
        return "6 km"

    def _get_rms_table_name(self):
        return 'cryosat-2_swh_rms_threshold.txt'
    
    def _get_rms_table_def(self):
        return (-1., 16., 0.1)
    
    def _get_rms_classes(self):
        return (-1., 5., 8.)

    def _get_midrange_rms_poly(self):
        return numpy.poly1d([
            0.003712808782105,
            -0.022959435041166,
            0.155517950243010,
            0.484687460175769
            ])

    def _coeff_std(self):
        return 3.9

    def get_native_name(self, fieldname):
        """
        Return the GDR equivalent field name
        """
        translation = {
            'sigma0': 'sig0_ku', 
            'sigma0_rms': 'sig0_rms_ku',
            'swh': 'swh_ku',
            'swh_rms': 'swh_rms_ku',
            'sigma0_num_valid': 'range_numval_ku',
            'wind_speed_alt': 'wind_speed_alt',
            'range_rms': 'range_rms_ku',
            'range': 'range_ku',
            'off_nadir_angle_wf': 'off_nadir_angle2_pf'
        }
        if fieldname in translation:
            return translation[fieldname]

#     def customize_rejection_flag(self, rejfield):
#         rejfield.attributes['flag_meanings'] = ' '.join([
#             'altimeter_land_flag', 'sig0_ku_mle3_lower_than_5',
#             'sdmax_flag_0_5m', 'sdmax_flag_5_8m', 'sdmax_flag_over_8m',
#             'swh_outlier', 'swh_dev_from_mean_gt_5m', 'ssh_quality', 'swh_quality',
#             'sigma0_quality'
#             ])
#         print rejfield
#         rejfield.attributes['flag_masks'] += [64, 128, 256]

    def set_swh_quality(self, track):
        """
        set the quality information on SWH
        """       
        swh_ku = track.get_values('swh_ku')
        swh_rms_ku = track.get_values('swh_rms_ku')
        flags = track.get_field('flags')
        
        if 'hardware_bad' in flags.attributes['flag_meanings']:
            sar_mode = 'hardware_bad'
        else:
            sar_mode = 'sar_mode'
        
        # init quality fields             
        quality = self.l2p.get_values('swh_quality')
        quality[:] = QUALITY_GOOD
        
        rejection = self.l2p.get_field('swh_rejection_flags')
        rejection.get_values()[:] = 0
     
        # basic screening
        editing = (
            (swh_ku <= 0)
            | numpy.ma.getmaskarray(swh_rms_ku)
            | flags.bitmask_or('alt_land')
            | flags.bitmask_or('range_suspect')
            | flags.bitmask_or(sar_mode)
            | flags.bitmask_or('swh_suspect')
            | flags.bitmask_or('backscatter_suspect')
            ).filled(True)
        quality[editing] = QUALITY_BAD

        # undefined SWH values
        quality[
            (numpy.ma.getmaskarray(swh_ku) | numpy.ma.getmaskarray(swh_rms_ku))
            ] = QUALITY_UNDEFINED
               
        # set rejection flag
        self.set_mask_bit(
            rejection,
            flags.bitmask_or('alt_land'),
            'not_water'
            )
        self.set_mask_bit(
            rejection,
            flags.bitmask_or('range_suspect') | flags.bitmask_or(sar_mode),
            'ssh_validity'
            )        
        self.set_mask_bit(
            rejection,
            flags.bitmask_or('swh_suspect') | (swh_ku <= 0),
            'swh_validity'
            )
        self.set_mask_bit(
            rejection,
            flags.bitmask_or('backscatter_suspect'),
            'sigma0_validity'
            )

    def set_wind_speed_alt_quality(self, track):
        """
        set the quality information on altimeter wind speed
        """
        quality = self.l2p.get_values('wind_speed_alt_quality')
        #quality[:] = QUALITY_GOOD
        
        quality_hs = self.l2p.get_values('swh_quality')
        quality[:] = quality_hs[:]

    def set_wind_speed_rad_quality(self, track):
        """
        set the quality information on altimeter wind speed
        """
        pass

    def adjust_swh(self, track):
        """compute calibrated SWH"""
        adjusted_swh = numpy.ma.array(track.get_values('swh_ku'), copy=True)

        poly = numpy.poly1d([0.0124, 0.8858, 0.1446])
        ind = adjusted_swh < 7.67
        adjusted_swh[ind] = poly(adjusted_swh[ind])          

        return numpy.ma.masked_less(adjusted_swh, 0, copy=False)

    def swh_adjustment_attributes(self):
        formula = (
            "swh < 7.67: swh_adjusted = 0.1446 + 0.8858 swh + 0.0124 swh^2, "
            "swh >= 7.67: no adjustment applied."
            )
        reference = "CCI Sea State, G.Dodet, 2019"
        return formula, reference

    def swh_std_error(self):
        """compute the standard error of SWH
        
        1.96 x (STD_ERR_P1 x swh +STD_ERR_P0)
        """
        return 1.96 * (numpy.ma.maximum(1., self.l2p.get_values('swh_adjusted')) * 0.023 + 0.032)

    def adjust_sigma0(self, track):
        # no adjustment required
        return track.get_values('sig0_ku')

    def correct_sigma0_for_wspd(self):
        # no adjustment required
        return self.l2p.get_values('sigma0_adjusted')

    def sigma0_adjustment_attributes(self):
        formula = 'no correction required'
        reference = "Global altimeter SWH dataset, P.Queffeulou, 2017"
        return formula, reference

