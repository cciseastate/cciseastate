"""
Specific Envisat/RA2 processing for the conversion of a GDR altimeter product
to CCI format and content.

Based on code from P. Queffeulou

:copyright: Copyright 2019 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy

from cerbere.feature.trajectory import Trajectory

from cciseastate.altimeter.v1.l2p import L2P
from cciseastate.altimeter.v1.l2p import QUALITY_GOOD, QUALITY_BAD, QUALITY_UNDEFINED


class EnvisatL2P(L2P):

    def get_1hz_feature(self, fname):
        """return input GDR feature as a cerbere trajectory"""
        from cerbere.dataset.esara2file import ESARA2File
        ncf = ESARA2File(url=fname, mode='r')
        traj = Trajectory()
        traj.load(ncf)
        return traj

    def satellite(self):
        return 'Envisat'

    def instrument(self):
        return 'RA2'

    def band(self):
        return 'Ku'

    def cycle(self, track):
        return track.get_metadata()['cycle_number']

    def relative_pass(self, track):
        return track.get_metadata()['pass_number']

    def equator_crossing_time(self, track):
        return
        # return track.get_metadata()['equator_time']

    def equator_crossing_longitude(self, track):
        return
        # lon = track.get_metadata()['equator_longitude']
        # if lon > 180:
        #    lon -= 360.
        # return lon

    def resolution(self):
        return "7.4 km"

    def _get_rms_table_name(self):
        return 'envisat_swh_rms_threshold.txt'

    def _get_rms_table_def(self):
        return (0., 16., 0.1)

    def _get_rms_classes(self):
        return (0., 0., 100.)

    def _coeff_std(self):
        return 5.

    def _get_midrange_rms_poly(self):
        return numpy.poly1d([
            0.0384,
            -0.0500,
            0.8457
        ])

    def get_native_name(self, fieldname):
        """
        Return the GDR equivalent field name
        """
        translation = {
            'swh': 'ku_sig_wv_ht',
            'swh_rms': 'sd_18hz_ku_swh',
            'swh_num_valid': 'num_18hz_ku_ocean_swh',
            'sigma0': 'ku_ocean_bscat_coeff',
            'sigma0_rms': 'sd_18hrz_ku_ocean_bscat',
            'sigma0_num_valid': 'num_18hrz_ku_ocean_bscat',
            'wind_speed_alt': 'ra2_wind_sp',
            'off_nadir_angle_wf': 'off_nad_ang_wvform',
            'range_rms': 'sd_18hz_ku_ocean',
            'range': 'ku_band_ocean_range',
            'total_column_liquid_water_content_rad': 'mwr_liq_vapour_content',
        }
        if fieldname in translation:
            return translation[fieldname]

    def set_swh_quality(self, track):
        """
        set the quality information on SWH
        """
        swh_numval = track.get_values('num_18hz_ku_ocean_swh')
        off_nadir_angle_wf = track.get_values('off_nad_ang_wvform')
        swh = track.get_values('ku_sig_wv_ht')
        swh_rms = track.get_values('sd_18hz_ku_swh')
        sigma0 = track.get_values('ku_ocean_bscat_coeff')
        
        qual = track.get_values('quality_flag')
        mcd = track.get_values('meas_conf_data_flags')
        land = track.get_values('altim_landocean_flag')
        ku_ocean_retrk_qua_flags = track.get_values('ku_ocean_retrk_qua_flags')

        # init quality fields
        quality = self.l2p.get_values('swh_quality')
        quality[:] = QUALITY_GOOD
        
        rejection = self.l2p.get_field('swh_rejection_flags')
        rejection.get_values()[:] = 0
        
        # retracking Ku (OK=0)
        bit16 = (mcd & (1 << 16)) > 0
        # wave form samples fault (OK=0)
        bit06 = (mcd & (1 << 6)) > 0

        # basic screening
        # % nominal
        # k=find(quality_flag==0 & num_18hz_ku_ocean_swh >=19 & bit06==0
        # & bit16==0& altim_landocean_flag==0& ku_sig_wv_ht < 32767 
        # & ku_sig_wv_ht>0&sd_18hz_ku_swh>0& ku_ocean_retrk_qua_flags==0
        # & ku_ocean_bscat_coeff>0&ku_ocean_bscat_coeff<32767 
        # & abs(off_nad_ang_wvform) < 1000 & sdh<sdmax  & lat>=latmin 
        # & lat <= latmax & lon >= lonmin & lon <= lonmax);
        #
        # % moins drastique
        # k=find(quality_flag==0 & num_18hz_ku_ocean_swh >=19 & bit06==0
        # & bit16==0& altim_landocean_flag==0& ku_sig_wv_ht < 32767 
        # & ku_sig_wv_ht>0&sd_18hz_ku_swh>0& ku_ocean_retrk_qua_flags==0
        # & ku_ocean_bscat_coeff>0&ku_ocean_bscat_coeff<32767&sdh<sdmax  
        # & lat>=latmin & lat <= latmax & lon >= lonmin & lon <= lonmax);
        editing = (
            (swh.filled(0) <= 0)
            | (swh_rms.filled(0) <= 0)
            | (swh_numval <= 18)
            | (land != 0)
            | (ku_ocean_retrk_qua_flags != 0)
            | (sigma0.filled(0) <= 0)
            | (numpy.fabs(off_nadir_angle_wf) >= 0.1)
            | (qual != 0)
            | bit16
            | bit06

        ).filled(True)
        quality[editing] = QUALITY_BAD
        
        # undefined SWH values
        quality[
            (numpy.ma.getmaskarray(swh) | numpy.ma.getmaskarray(swh_rms))
            ] = QUALITY_UNDEFINED

        # set rejection flag
        self.set_mask_bit(rejection,
                          land != 0,
                          'not_water')
        self.set_mask_bit(rejection,
                          (swh.filled(0) <= 0),
                          'swh_validity')
        self.set_mask_bit(
            rejection,
            ((swh_numval <= 18)
             | (numpy.fabs(off_nadir_angle_wf) >= 0.1)
             | (qual != 0)
             | (ku_ocean_retrk_qua_flags != 0)
             | bit16
             | bit06),
            'waveform_validity'
            )
        self.set_mask_bit(rejection,
                          (sigma0.filled(0) <= 0),
                          'sigma0_validity')
        self.set_mask_bit(rejection,
                          swh_rms.filled(0) <= 0,
                          'swh_rms_outlier')


    def set_wind_speed_alt_quality(self, track):
        """
        set the quality information on altimeter wind speed
        """
        quality = self.l2p.get_values('wind_speed_alt_quality')
        #quality[:] = QUALITY_GOOD

        quality_hs = self.l2p.get_values('swh_quality')
        quality[:] = quality_hs[:]

    def set_wind_speed_rad_quality(self, track):
        """
        set the quality information on altimeter wind speed
        """
        pass

    def adjust_swh(self, track):
        """compute calibrated SWH

        for swh > 3.41 m:
            swh_cor = 1.0095 x swh + 0.0192

        for swh ≤ 3.41 m:
            swh_cor = 0.4358 + 0.5693 x swh + 0.1650 x swh2 - 0.0210 x swh3
        """
        values = numpy.ma.array(track.get_values('ku_sig_wv_ht'), copy=True)
        ind = (values > 3.41)
        values[ind] = values[ind] * 1.0095 + 0.0192
        values[~ind] = (
            0.4358
            + 0.5693 * values[~ind]
            + 0.1650 * values[~ind]**2
            - 0.0210 * values[~ind]**3
        )
        return values

    def swh_adjustment_attributes(self):
        formula = (
            'for swh > 3.41 m: swh_cor = 1.0095 x swh + 0.0192, '
            ' for swh ≤ 3.41 m: swh_cor = 0.4358 + 0.5693 x swh +'
            ' 0.1650 x swh2 - 0.0210 x swh3'
        )
        reference = "Global altimeter SWH dataset, P.Queffeulou, 2017"

        return formula, reference

    def swh_std_error(self):
        """compute the standard error of SWH
        
        1.96 x (STD_ERR_P1 x swh +STD_ERR_P0)
        """
        return 1.96 * (
            numpy.ma.maximum(1., self.l2p.get_values('swh_adjusted')) * 0.025
            + 0.050)

    def adjust_sigma0(self, track):
        return track.get_values('ku_ocean_bscat_coeff')

    def correct_sigma0_for_wspd(self):
        # Zieger et al., 2009
        return self.l2p.get_values('sigma0_adjusted') - 0.138

    def sigma0_adjustment_attributes(self):
        formula = 'no correction required'
        reference = "Global altimeter SWH dataset, P.Queffeulou, 2017"
        return formula, reference
