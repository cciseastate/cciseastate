"""
Specific ERS1 processing for the conversion of a OPR altimeter product to
CCI format and content.

Based on code from P. Queffeulou

:copyright: Copyright 2019 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import datetime
from pkg_resources import resource_filename

import numpy

from cerbere.datamodel.trajectory import Trajectory
from cerbere.mapper.ersoprfile import ERSOPRFile
from cciseastate.altimeter.v1.l2p import L2P
from cciseastate.altimeter.v1.l2p import QUALITY_GOOD, QUALITY_BAD, QUALITY_UNDEFINED



class ERS1OPRL2P(L2P):
    
    def get_1hz_feature(self, fname):
        """return input GDR feature as a cerbere trajectory"""
        ncf = ERSOPRFile(url=fname, mode='r')
        traj = Trajectory()
        traj.load(ncf)
        return traj

    def satellite(self):
        return 'ERS-1'

    def instrument(self):
        return 'RA'

    def band(self):
        return 'Ku'

    def cycle(self, track):
        return track.get_metadata()['cycle_number']

    def relative_pass(self, track):
        return track.get_metadata()['pass_number']

    def equator_crossing_time(self, track):
        return

    def equator_crossing_longitude(self, track):
        return

    def resolution(self):
        return "6.6km"

    def _get_rms_table_name(self):
        return
    
    def _get_rms_table_def(self):
        return
    
    def _get_rms_classes(self):
        return

    def _coeff_std(self):
        return 5.

    def _get_midrange_rms_poly(self):
        return

    def get_native_name(self, fieldname):
        """
        Return the GDR equivalent field name
        """
        translation = {
            'swh': 'swh',
            'swh_rms': 'swh_std',
            'swh_num_valid': 'nval',
            'sigma0': 'sig0',
            'sigma0_rms': 'sig0_std', 
            'wind_speed_alt': 'windsp_alt',
            'range_rms': 'range_std',
            'range': 'range',
            'off_nadir_angle_wf': 'off_nadir_wf',
            'total_column_liquid_water_content_rad': 'liquid_water_content_rad'
        }
        if fieldname in translation:
            return translation[fieldname]


    def set_swh_quality(self, track):
        """
        set the quality information on SWH
        """        
        swh_ku = track.get_values('swh')
        swh_rms_ku = track.get_values('swh_std')
        swh_numval_ku = track.get_values('nval')

        sigma0 = track.get_values('sig0')

        mcd = track.get_values('mcd').astype(numpy.int32)
        invalid = (mcd & (1 << 0)) > 0
        swh_qual = (mcd & (1 << 7)) > 0
        sig0_qual = (mcd & (1 << 8)) > 0

        # init quality fields             
        quality = self.l2p.get_values('swh_quality')
        quality[:] = QUALITY_GOOD
        
        rejection = self.l2p.get_field('swh_rejection_flags')
        rejection.get_values()[:] = 0

        # basic screening
        editing = (
            (swh_ku.filled(0) <= 0)
            | numpy.ma.getmaskarray(sigma0)
            | (swh_numval_ku <= 16)
            | invalid
            | swh_qual
            | sig0_qual
        ).filled(True)
        quality[editing] = QUALITY_BAD
        
        # undefined SWH values
        quality[
            (numpy.ma.getmaskarray(swh_ku) | numpy.ma.getmaskarray(swh_rms_ku))
            ] = QUALITY_UNDEFINED
        
        # set rejection flag
        self.set_mask_bit(rejection,
                          invalid,
                          'not_water')
        self.set_mask_bit(rejection,
                          (swh_ku.filled(0) <= 0) | swh_qual,
                          'swh_validity')
        self.set_mask_bit(rejection,
                          (swh_numval_ku <= 16),
                          'waveform_validity')
        self.set_mask_bit(rejection,
                          numpy.ma.getmaskarray(sigma0) | sig0_qual,
                          'sigma0_validity')
        self.set_mask_bit(rejection,
                          swh_rms_ku.filled(0) <= 0,
                          'swh_rms_outlier')


    def set_wind_speed_alt_quality(self, track):
        """
        set the quality information on altimeter wind speed
        """
        quality = self.l2p.get_values('wind_speed_alt_quality')
        quality_hs = self.l2p.get_values('swh_quality')
        quality[:] = quality_hs[:]
        
        stop = track.get_end_time()
        if stop <= datetime.datetime(1991, 8, 1, 2, 15, 18):
            quality[:] = QUALITY_BAD

    def set_wind_speed_rad_quality(self, track):
        """
        set the quality information on altimeter wind speed
        """
        pass

    def adjust_swh(self, track):
        """compute calibrated SWH.
        """
        swh_corr = track.get_values('swh') * 1.1259 + 0.1854
        return numpy.ma.masked_less(swh_corr, 0, copy=False)

    def swh_adjustment_attributes(self):
        formula = "swh_cor = swh *  1.1259 + 0.1854"
        reference = "Global altimeter SWH dataset, P.Queffeulou, 2017"
        return formula, reference

    def swh_std_error(self):
        """compute the standard error of SWH
        
        1.96 x (STD_ERR_P1 x swh +STD_ERR_P0)
        """
        return 1.96 * (
            numpy.ma.maximum(1., self.l2p.get_values('swh_adjusted')) * 0.007
            + 0.103)

    def adjust_sigma0(self, track):
        """
        """
        # Calibrated sigma0 Ku-band and corrected wind speed set to NaN before
        # 01-Aug-1991 02:15:18
        stop = track.get_end_time()
        if stop <= datetime.datetime(1991, 8, 1, 2, 15, 18):
            return self.l2p.get_values('sigma0_adjusted')
        
        return track.get_values('sig0') + 0.0976

    def correct_sigma0_for_wspd(self):
        return self.l2p.get_values('sigma0_adjusted') - 0.140

    def sigma0_adjustment_attributes(self):
        formula = "sigma0_adjusted = sigma0 +  0.0976, "\
                "data before 1-Aug-1991 02:15:18 are masked."
        reference = "Global altimeter SWH dataset, P.Queffeulou, 2017"
        return formula, reference

