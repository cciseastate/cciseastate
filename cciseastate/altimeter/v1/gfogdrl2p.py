"""
Specific GFO GDR processing for the conversion of a GFO altimeter product to
CCI format and content.

Based on code from P. Queffeulou

:copyright: Copyright 2019 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import datetime
from pkg_resources import resource_filename

import numpy

from cerbere.datamodel.trajectory import Trajectory
from cerbere.mapper.gfogdrfile import GFOGDRFile
from cciseastate.altimeter.v1.l2p import L2P
from cciseastate.altimeter.v1.l2p import QUALITY_GOOD, QUALITY_BAD, QUALITY_UNDEFINED



class GFOGDRL2P(L2P):
    
    def get_1hz_feature(self, fname):
        """return input GDR feature as a cerbere trajectory"""
        ncf = GFOGDRFile(url=fname, mode='r')
        traj = Trajectory()
        traj.load(ncf)
        return traj

    def satellite(self):
        return 'GFO'

    def instrument(self):
        return 'GFO-RA'

    def band(self):
        return 'Ku'

    def cycle(self, track):
        return track.get_metadata()['cycle_number']

    def relative_pass(self, track):
        return track.get_metadata()['pass_number']

    def equator_crossing_time(self, track):
        return

    def equator_crossing_longitude(self, track):
        return

    def resolution(self):
        return "6.6km"

    def _get_rms_table_name(self):
        return
    
    def _get_rms_table_def(self):
        return
    
    def _get_rms_classes(self):
        return

    def _coeff_std(self):
        return 5.

    def _get_midrange_rms_poly(self):
        return

    def get_native_name(self, fieldname):
        """
        Return the GDR equivalent field name
        """
        translation = {
            'swh': 'swh',
            'swh_rms': 'swh_std',
            'swh_num_valid': 'nvals_swh',
            'sigma0': 'sigma0',
            'wind_speed_alt': 'wind_speed',
            'range_rms': 'sshu_std',
            'range': 'sshu',
            'off_nadir_angle_wf': 'attitude_squared',
        }
        if fieldname in translation:
            return translation[fieldname]


    def set_swh_quality(self, track):
        """
        set the quality information on SWH
        """        
        swh_ku = track.get_values('swh')
        swh_rms_ku = track.get_values('swh_std')
        swh_numval_ku = track.get_values('nvals_swh')

        sigma0 = track.get_values('sigma0')

        surface_type = track.get_values('noaa_flags').astype(numpy.uint32)
        qual1 = track.get_values('quality_word_1').astype(numpy.uint32)
        qual2 = track.get_values('quality_word_2').astype(numpy.uint32)
       
        # bits 2, 3, 7, 10, 11 must be zero
        # record is zero filled
        bit2 = (qual1 & (1 << 2)) > 0
        # RA not in fine track
        bit3 = (qual1 & (1 << 3)) > 0
        # no smoothed VATT
        bit7 = (qual1 & (1 << 7)) > 0
        # SWH bounds error
        bit10 = (qual1 & (1 << 10)) > 0
        # land contamination
        bit11 = (qual2 & (1 << 11)) > 0

        # init quality fields             
        quality = self.l2p.get_values('swh_quality')
        quality[:] = QUALITY_GOOD
        
        rejection = self.l2p.get_field('swh_rejection_flags')
        rejection.get_values()[:] = 0

        # basic screening
        editing = (
            (surface_type == 3)
            | (bit2 | bit3 | bit7 | bit10 | bit11)
            | (swh_numval_ku != 10)
            | (swh_rms_ku / swh_ku >= 0.2)
            | numpy.ma.getmaskarray(sigma0)
            | (swh_ku.filled(0) <= 0)
            | (swh_ku.filled(0) > 30.)
        ).filled(True)
        quality[editing] = QUALITY_BAD
        
        # undefined SWH values
        quality[
            (numpy.ma.getmaskarray(swh_ku) | numpy.ma.getmaskarray(swh_rms_ku))
            ] = QUALITY_UNDEFINED
        
        # set rejection flag
        self.set_mask_bit(rejection,
                          (surface_type == 3) | bit11,
                          'not_water')
        self.set_mask_bit(rejection,
                          (swh_ku.filled(0) <= 0) | bit10 | (swh_ku.filled(0) > 30.),
                          'swh_validity')
        self.set_mask_bit(rejection,
                          (swh_numval_ku != 10) | bit2 | bit3 | bit7,
                          'waveform_validity')
        self.set_mask_bit(rejection,
                          numpy.ma.getmaskarray(sigma0),
                          'sigma0_validity')
        self.set_mask_bit(rejection,
                          swh_rms_ku.filled(0) <= 0 | (swh_rms_ku / swh_ku >= 0.2),
                          'swh_rms_outlier')


    def set_wind_speed_alt_quality(self, track):
        """
        set the quality information on altimeter wind speed
        """
        quality = self.l2p.get_values('wind_speed_alt_quality')
        #quality[:] = QUALITY_GOOD
        
        quality_hs = self.l2p.get_values('swh_quality')
        quality[:] = quality_hs[:]

    def set_wind_speed_rad_quality(self, track):
        """
        set the quality information on altimeter wind speed
        """
        pass

    def adjust_swh(self, track):
        """compute calibrated SWH.
        """
        swh_corr = track.get_values('swh') * 1.0625 + 0.0754
        return numpy.ma.masked_less(swh_corr, 0, copy=False)
    
    def swh_adjustment_attributes(self):
        formula = "swh_cor = swh * 1.0625 + 0.0754"
        reference = "Global altimeter SWH dataset, P.Queffeulou, 2017"
        return formula, reference

    def swh_std_error(self):
        """compute the standard error of SWH
        
        1.96 x (STD_ERR_P1 x swh +STD_ERR_P0)
        """
        return 1.96 * (
            numpy.ma.maximum(1., self.l2p.get_values('swh_adjusted')) * 0.021
            + 0.073)

    def adjust_sigma0(self, track):
        """
        """
        start = track.get_start_time()
        stop = track.get_end_time()
        
        # Do not use sigma0 GFO after 2 Aug 2006. 
        # Calibrated sigma0 Ku-band and corrected wind speed set
        # to NaN over this time period
        if start >= datetime.datetime(2006, 8, 2):
            return self.l2p.get_values('sigma0_adjusted')

        # correction wrt Envisat
        adjusted_sig0 = track.get_values('sigma0') - 0.4322

        # +0.32 dB from 11 January 2000 to 6 December 2000 (included)       
        if (start >= datetime.datetime(2000, 1, 11)
                and stop <= datetime.datetime(2000, 12, 6)):
            adjusted_sig0 += 0.32  

        return adjusted_sig0

    def correct_sigma0_for_wspd(self):
        return self.l2p.get_values('sigma0_adjusted') - 0.14

    def sigma0_adjustment_attributes(self):
        formula = "sigma0_adjusted = sigma0 - 0.4322, "\
                "added +0.32 between 11-Jan-2000 and 6-Dec-2000, "\
                "data are invalid beyond 2-Aug-2006"
        reference = "Global altimeter SWH dataset, P.Queffeulou, 2017"
        return formula, reference

