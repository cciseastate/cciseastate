"""
Specific Jason-1 processing for the conversion of a GDR altimeter product to
CCI format and content.

Based on code from P. Queffeulou

:copyright: Copyright 2019 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import numpy

import cerbere

from cciseastate.altimeter.v1.l2p import L2P
from cciseastate.altimeter.v1.l2p import QUALITY_GOOD, QUALITY_BAD, QUALITY_UNDEFINED


class Jason1L2P(L2P):
    
    def get_1hz_feature(self, fname):
        """return input GDR feature as a cerbere trajectory"""
        return cerbere.open_as_feature('Trajectory', fname, 'NCDataset')

    def satellite(self):
        return 'Jason-1'

    def instrument(self):
        return 'Poseidon-2'

    def band(self):
        return 'Ku'

    def cycle(self, track):
        return track.get_metadata()['cycle_number']

    def relative_pass(self, track):
        return track.get_metadata()['pass_number']

    def equator_crossing_time(self, track):
        return track.get_metadata()['equator_time']

    def equator_crossing_longitude(self, track):
        lon = track.get_metadata()['equator_longitude']
        if lon > 180:
            lon -= 360.
        return lon

    def resolution(self):
        return "5.8 km"

    def _get_rms_table_name(self):
        # for Version E
        return 'jason-1_swh_rms_threshold.txt'
    
    def _get_rms_table_def(self):
        # for Version E
        return (-1., 16., 0.1)
    
    def _get_rms_classes(self):
        # for Version E
        return (0., 4.5, 8.)

    def _coeff_std(self):
        return 5.

    def _get_midrange_rms_poly(self):
        # for Version E
        return numpy.poly1d([
            0.00388790,
            -0.07416582,
            0.54581272,
            -1.63633547,
            2.64616772
            ])

    def get_native_name(self, fieldname):
        """
        Return the GDR equivalent field name
        """
        translation = {
            'swh': 'swh_ku',
            'swh_rms': 'swh_rms_ku',
            'swh_num_valid': 'swh_numval_ku',
            'sigma0': 'sig0_ku', 
            'sigma0_rms': 'sig0_rms_ku',
            'sigma0_num_valid': 'sig0_numval_ku',
            'wind_speed_alt': 'wind_speed_alt',
            'range_rms': 'range_rms_ku',
            'range': 'range_ku',
            'off_nadir_angle_wf': 'off_nadir_angle_wf_ku',
            'wind_speed_rad': 'wind_speed_rad',
            'total_column_liquid_water_content_rad': 'rad_liquid_water',
        }
        if fieldname in translation:
            return translation[fieldname]

#     def customize_rejection_flag(self, rejfield):
#         rejfield.attributes['flag_meanings'] = ' '.join([
#             'altimeter_land_flag', 'sig0_ku_mle3_lower_than_5',
#             'sdmax_flag_0_5m', 'sdmax_flag_5_8m', 'sdmax_flag_over_8m',
#             'swh_outlier', 'swh_dev_from_mean_gt_5m', 'ssh_quality', 'swh_quality',
#             'sigma0_quality'
#             ])
#         print rejfield
#         rejfield.attributes['flag_masks'] += [64, 128, 256]

    def set_swh_quality(self, track):
        """
        set the quality information on SWH
        """       
        swh_ku = track.get_values('swh_ku')
        swh_rms_ku = track.get_values('swh_rms_ku')
        swh_numval_ku = track.get_values('swh_numval_ku')
        qual_alt_1hz_swh_ku = track.get_values('qual_alt_1hz_swh_ku')

        surface = track.get_values('surface_type')
        sigma0 = track.get_values('sig0_ku')

        # init quality fields             
        quality = self.l2p.get_values('swh_quality')
        quality[:] = QUALITY_GOOD
        
        rejection = self.l2p.get_field('swh_rejection_flags')
        rejection.get_values()[:] = 0

        # basic screening
        editing = (
            # ocean like echo
            (qual_alt_1hz_swh_ku != 0)
            # open oceans or semi-enclosed seas, enclosed seas or lakes
            | (surface >= 2)
            | (swh_ku.filled(0) <= 0)
            | (swh_rms_ku.filled(0) <= 0)
            | (swh_numval_ku < 19)
            | numpy.ma.getmaskarray(sigma0)
        ).filled(True)
        quality[editing] = QUALITY_BAD

        # undefined SWH values
        quality[
            (numpy.ma.getmaskarray(swh_ku) | numpy.ma.getmaskarray(swh_rms_ku))
            ] = QUALITY_UNDEFINED
        
        # set rejection flag
        self.set_mask_bit(rejection,
                          ((surface >= 2) | (qual_alt_1hz_swh_ku != 0)),
                          'not_water')
        self.set_mask_bit(rejection,
                          (swh_ku.filled(0) <= 0),
                          'swh_validity')
        self.set_mask_bit(rejection,
                          (swh_numval_ku < 19),
                          'waveform_validity')
        self.set_mask_bit(rejection,
                          numpy.ma.getmaskarray(sigma0),
                          'sigma0_validity')
        self.set_mask_bit(rejection,
                          swh_rms_ku.filled(0) <= 0,
                          'swh_rms_outlier')


    def set_wind_speed_alt_quality(self, track):
        """
        set the quality information on altimeter wind speed
        """
        quality = self.l2p.get_values('wind_speed_alt_quality')
        #quality[:] = QUALITY_GOOD
        
        quality_hs = self.l2p.get_values('swh_quality')
        quality[:] = quality_hs[:]

    def set_wind_speed_rad_quality(self, track):
        """
        set the quality information on altimeter wind speed
        """
        quality = self.l2p.get_values('wind_speed_rad_quality')
        quality[:] = QUALITY_GOOD
        
        ice = track.get_values('rad_sea_ice_flag')
        land = track.get_values('rad_surf_type')
        rain = track.get_values('rad_rain_flag')
        #rad_averaging_flag = track.get_values('rad_averaging_flag')
        #raddist = track.get_values('rad_distance_to_land')
        
        ind = (land != 0) | (ice == 1) | (rain == 1) 
        quality[ind] = QUALITY_BAD

    def adjust_swh(self, track):
        """compute calibrated SWH.
        
        correction for version jason-1 GDR E (G.Dodet)
        """
        # for version E
        swh_ku = track.get_values('swh_ku')     
        return numpy.ma.masked_less(swh_ku * 1.0125 + 0.0461, 0, copy=False)

    def swh_adjustment_attributes(self):
        # for version C
        formula = (
            "swh_adjusted = swh * 1.0125 + 0.0461,"
            )
        reference = "CCI Sea State, G.Dodet, 2019"
        return formula, reference

    def swh_std_error(self):
        """compute the standard error of SWH
        
        1.96 x (STD_ERR_P1 x swh +STD_ERR_P0)
        """
        return 1.96 * (
            numpy.ma.maximum(1., self.l2p.get_values('swh_adjusted')) * 0.028
            + 0.037)

    def adjust_sigma0(self, track):
        """
        bias vs Envisat version D
        
        for GDR version C.
        """
        # for version C
        return track.get_values('sig0_ku') - 2.8165

    def correct_sigma0_for_wspd(self):
        # for version C
        return self.l2p.get_values('sigma0_adjusted') - 0.3

    def sigma0_adjustment_attributes(self):
        # for version C
        formula = 'sigma0_adjusted = sigma0 - 2.8165 dB'
        reference = "Global altimeter SWH dataset (for GDR version C), P.Queffeulou, 2017"
        return formula, reference


