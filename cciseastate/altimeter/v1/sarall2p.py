"""
Specific Saral/AltiKa processing for the conversion of a GDR altimeter product
to CCI format and content.

Based on code from P. Queffeulou and C. Prevost

:copyright: Copyright 2019 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import numpy

from cerbere.feature.trajectory import Trajectory
from cerbere.dataset.ncdataset import NCDataset
from cciseastate.altimeter.v1.l2p import L2P
from cciseastate.altimeter.v1.l2p import QUALITY_GOOD, QUALITY_BAD, QUALITY_UNDEFINED



class SaralL2P(L2P):
    
    def get_1hz_feature(self, fname):
        """return input GDR feature as a cerbere trajectory"""
        ncf = NCFile(url=fname, mode='r')
        traj = Trajectory()
        traj.load(ncf)
        return traj

    def satellite(self):
        return 'Saral'

    def instrument(self):
        return 'AltiKa'

    def band(self):
        return 'Ka'

    def cycle(self, track):
        return track.get_metadata()['cycle_number']

    def relative_pass(self, track):
        return track.get_metadata()['pass_number']

    def equator_crossing_time(self, track):
        return track.get_metadata()['equator_time']

    def equator_crossing_longitude(self, track):
        lon = track.get_metadata()['equator_longitude']
        if lon > 180:
            lon -= 360.
        return lon

    def resolution(self):
        return "7 km"

    def _get_rms_table_name(self):
        return 'saral_swh_rms_threshold.txt'
    
    def _get_rms_table_def(self):
        return (0., 16., 0.1)
    
    def _get_rms_classes(self):
        return (0., 2.5, 12.)

    def _coeff_std(self):
        return 5.

    def _get_midrange_rms_poly(self):
        return numpy.poly1d([
            0.00122599,
            -0.01741631,
            0.17180005,
            0.17894723,
            ])

    def get_native_name(self, fieldname):
        """
        Return the GDR equivalent field name
        """
        translation = {
            'swh': 'swh',
            'swh_rms': 'swh_rms',
            'swh_num_valid': 'swh_numval',
            'sigma0': 'sig0', 
            'sigma0_rms': 'sig0_rms',
            'sigma0_num_valid': 'sig0_numval',
            'wind_speed_alt': 'wind_speed_alt',
            'off_nadir_angle_wf': 'off_nadir_angle_wf',
            'range_rms': 'range_rms',
            'range': 'range',            
            'total_column_liquid_water_content_rad': 'rad_liquid_water',
        }
        if fieldname in translation:
            return translation[fieldname]


    def set_swh_quality(self, track):
        """
        set the quality information on SWH
        """       
        #swh_numval = track.get_values('swh_numval')
        surface = track.get_values('surface_type')
        #off_nadir_angle_wf = track.get_values('off_nadir_angle_wf')
        ice = track.get_values('ice_flag')
     
        swh = track.get_values('swh')
        #swh_rms = track.get_values('swh_rms')
        sigma0 = track.get_values('sig0')

        # init quality fields             
        quality = self.l2p.get_values('swh_quality')
        quality[:] = QUALITY_GOOD
        
        rejection = self.l2p.get_field('swh_rejection_flags')
        rejection.get_values()[:] = 0

        # basic screening
        editing = (
            (surface >= 2)
            | (ice != 0)
            | numpy.ma.getmaskarray(sigma0)
            | (sigma0 < 5.)
        ).filled(True)
        quality[editing] = QUALITY_BAD

        # undefined SWH values
        quality[
            numpy.ma.getmaskarray(swh)
            ] = QUALITY_UNDEFINED

        # set rejection flag
        self.set_mask_bit(rejection,
                          (surface >= 2) | (ice != 0),
                          'not_water')
        self.set_mask_bit(
            rejection,
            (sigma0.filled(1) < 5) | numpy.ma.getmaskarray(sigma0),
            'sigma0_validity'
            )

    def set_wind_speed_alt_quality(self, track):
        """
        set the quality information on altimeter wind speed
        """
        quality = self.l2p.get_values('wind_speed_alt_quality')
        #quality[:] = QUALITY_GOOD
        
        quality_hs = self.l2p.get_values('swh_quality')
        quality[:] = quality_hs[:]

    def set_wind_speed_rad_quality(self, track):
        """
        set the quality information on altimeter wind speed
        """
        pass

    def wind_speed_gourrion(self, high_winds='young1993'):
        """
        Applies Gourrion et al. wind speed calculation based on sigma0
        and Hs
        """
        # does not apply to Ka band
        pass

    def wind_speed_abdalla(self):
        """
        Compute adjusted wind speed from sigma0, Abdalla 2012
        
        https://www.tandfonline.com/doi/pdf/10.1080/01490419.2012.718676?needAccess=true
        """
        pass

    def adjust_swh(self, track):
        """compute calibrated SWH"""
        return track.get_values('swh') * 0.9881 + 0.0555

    def swh_adjustment_attributes(self):
        formula = (
            "swh_adjusted = swh * 0.9881 +  0.0555"
            )
        reference = "CCI Sea State, G.Dodet, 2019"
        return formula, reference

    def swh_std_error(self):
        """compute the standard error of SWH
        
        1.96 x (STD_ERR_P1 x swh +STD_ERR_P0)
        """
        return 1.96 * (
            numpy.ma.maximum(1., self.l2p.get_values('swh_adjusted')) * 0.011
            + 0.075)

    def adjust_sigma0(self, track):
        # bias vs Envisat
        return track.get_values('sig0')

    def correct_sigma0_for_wspd(self):
        # no adjustment required
        return self.l2p.get_values('sigma0_adjusted')

    def sigma0_adjustment_attributes(self):
        formula = 'no correction required'
        reference = "Global altimeter SWH dataset, P.Queffeulou, 2017"
        return formula, reference

        
