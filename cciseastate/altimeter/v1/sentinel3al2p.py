"""
Specific Sentinel-3A processing for the conversion of a GDR altimeter product
to CCI format and content.

Based on code from P. Queffeulou and C. Prevost

:copyright: Copyright 2019 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import numpy

from cerbere.datamodel.trajectory import Trajectory
from cerbere.mapper.ncfile import NCFile
from cciseastate.altimeter.v1.l2p import L2P
from cciseastate.altimeter.v1.l2p import QUALITY_GOOD, QUALITY_BAD, QUALITY_UNDEFINED



class Sentinel3AL2P(L2P):
    
    def get_1hz_feature(self, fname):
        """return input GDR feature as a cerbere trajectory"""
        dim_matching = {'time': 'time_01'}
        coord_matching = {'time': 'time_01', 'lat': 'lat_01', 'lon': 'lon_01'}
        ncf = NCFile(
            url=fname,
            mode='r', 
            geodim_matching=dim_matching,
            geofield_matching=coord_matching
            )
        traj = Trajectory()
        traj.load(ncf)
        return traj

    def satellite(self):
        return 'Sentinel-3A'

    def instrument(self):
        return 'SRAL'

    def band(self):
        return 'Ku'

    def cycle(self, track):
        return track.get_metadata()['cycle_number']

    def relative_pass(self, track):
        return track.get_metadata()['pass_number']

    def equator_crossing_time(self, track):
        return track.get_metadata()['equator_time']

    def equator_crossing_longitude(self, track):
        lon = track.get_metadata()['equator_longitude']
        if lon > 180:
            lon -= 360.
        return lon

    def resolution(self):
        return "6.6 km"

    def _get_rms_table_name(self):
        return 'sentinel-3a_swh_rms_threshold.txt'
    
    def _get_rms_table_def(self):
        return (0., 16., 0.1)
    
    def _get_rms_classes(self):
        return (0., 5., 8.)

    def _coeff_std(self):
        return 5.

    def _get_midrange_rms_poly(self):
        return numpy.poly1d([2.06])

    def get_native_name(self, fieldname):
        """
        Return the GDR equivalent field name
        """
        translation = {
            'swh': 'swh_ocean_01_ku',
            'swh_rms': 'swh_ocean_rms_01_ku',
            'swh_num_valid': 'swh_ocean_numval_01_ku',
            'sigma0': 'sig0_ocean_01_ku', 
            'sigma0_rms': 'sig0_ocean_rms_01_ku',
            'sigma0_num_valid': 'sig0_ocean_numval_01_ku',
            'wind_speed_alt': 'wind_speed_alt_01_ku',    
            'range': 'range_ocean_01_ku',
            'range_rms': 'range_ocean_rms_01_ku',
            'off_nadir_angle_wf': 'corrected_off_nadir_angle_wf_ocean_01_ku',
            'total_column_liquid_water_content_rad': 'rad_liquid_water_01_ku',
        }
        if fieldname in translation:
            return translation[fieldname]

#     def customize_rejection_flag(self, rejfield):
#         rejfield.attributes['flag_meanings'] = ' '.join([
#             'altimeter_land_flag', 'sig0_ku_mle3_lower_than_5',
#             'sdmax_flag_0_5m', 'sdmax_flag_5_8m', 'sdmax_flag_over_8m',
#             'swh_outlier', 'swh_dev_from_mean_gt_5m', 'ssh_quality', 'swh_quality',
#             'sigma0_quality'
#             ])
#         print rejfield
#         rejfield.attributes['flag_masks'] += [64, 128, 256]

    def set_swh_quality(self, track):
        """
        set the quality information on SWH
        """
        surface = track.get_values('surf_type_01')
        swh_ku = track.get_values('swh_ocean_01_ku')
        swh_rms_ku = track.get_values('swh_ocean_rms_01_ku')
        swh_numval_ku = track.get_values('swh_ocean_numval_01_ku')
        
        # init quality fields             
        quality = self.l2p.get_values('swh_quality')
        quality[:] = QUALITY_GOOD
        
        rejection = self.l2p.get_field('swh_rejection_flags')
        rejection.get_values()[:] = 0

        # basic screening
        editing = (
            numpy.ma.getmaskarray(swh_rms_ku)
            | (surface >= 2)
            | (swh_rms_ku <= 0)
            | (swh_numval_ku <= 18)
        ).filled(True)
        quality[editing] = QUALITY_BAD

        # undefined SWH values
        quality[
            (numpy.ma.getmaskarray(swh_ku) | numpy.ma.getmaskarray(swh_rms_ku))
            ] = QUALITY_UNDEFINED


        # set rejection flag
        self.set_mask_bit(
            rejection,
            surface >= 2,
            'not_water'
            )
        self.set_mask_bit(
            rejection,
            numpy.ma.getmaskarray(swh_rms_ku) | (swh_rms_ku <= 0),
            'swh_rms_outlier'
            )
        self.set_mask_bit(
            rejection,
            (swh_numval_ku <= 18),
            'waveform_validity'
            )

    def set_wind_speed_alt_quality(self, track):
        """
        set the quality information on altimeter wind speed
        """
        quality = self.l2p.get_values('wind_speed_alt_quality')
        #quality[:] = QUALITY_GOOD
        
        quality_hs = self.l2p.get_values('swh_quality')
        quality[:] = quality_hs[:]
        
        # 28.5 seems to be a saturation value
        quality[track.get_values('wind_speed_alt_01_ku') >= 28.5] = QUALITY_BAD

    def set_wind_speed_rad_quality(self, track):
        """
        set the quality information on altimeter wind speed
        """
        quality = self.l2p.get_values('wind_speed_rad_quality')
        quality[:] = QUALITY_UNDEFINED

    def adjust_swh(self, track):
        """compute calibrated SWH"""
        adjusted_swh = numpy.ma.array(track.get_values('swh_ocean_01_ku'),
                                      copy=True)

        poly = numpy.poly1d([0.0117, 0.8611, 0.1646])
        ind = adjusted_swh < 10.51
        adjusted_swh[ind] = poly(adjusted_swh[ind])          

        return numpy.ma.masked_less(adjusted_swh, 0, copy=False)

    def swh_adjustment_attributes(self):
        formula = (
            "swh < 10.51: swh_adjusted = 0.1646 + 0.8611 swh + 0.0117 swh^2, "
            "swh >= 10.51: no adjustment applied."
            )
        reference = "CCI Sea State, G.Dodet, 2019"
        return formula, reference

    def swh_std_error(self):
        """compute the standard error of SWH
        
        1.96 x (STD_ERR_P1 x swh +STD_ERR_P0)
        """
        return 1.96 * (
            numpy.ma.maximum(1., self.l2p.get_values('swh_adjusted')) * 0.014
            + 0.053)

    def adjust_sigma0(self, track):
        # no correction
        return track.get_values('sig0_ocean_01_ku')

    def correct_sigma0(self, track):
        # no correction
        return track.get_values('sig0_ocean_01_ku')

    def correct_sigma0_for_wspd(self):
        # TBD
        return self.l2p.get_values('sigma0_adjusted')

    def sigma0_adjustment_attributes(self):
        formula = 'no correction yet'
        reference = ""
        return formula, reference

        
