"""
Specific Topex processing for the conversion of a GDR altimeter product to
CCI format and content.

Based on code from P. Queffeulou

:copyright: Copyright 2019 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import datetime
from pkg_resources import resource_filename

import numpy

from cerbere.datamodel.trajectory import Trajectory
from cerbere.mapper.topexgdrfile import TopexGDRFile
from cciseastate.altimeter.v1.l2p import L2P
from cciseastate.altimeter.v1.l2p import QUALITY_GOOD, QUALITY_BAD, QUALITY_UNDEFINED



class TopexL2P(L2P):
    
    def get_1hz_feature(self, fname):
        """return input GDR feature as a cerbere trajectory"""
        ncf = TopexGDRFile(url=fname, mode='r')
        traj = Trajectory()
        traj.load(ncf)
        return traj

    def satellite(self):
        return 'Topex-Poseidon'

    def instrument(self):
        return 'TOPEX'

    def band(self):
        return 'Ku'

    def cycle(self, track):
        return int(track.get_metadata()['cycle_number'])

    def relative_pass(self, track):
        return int(track.get_metadata()['pass_number'])

    def equator_crossing_time(self, track):
        return track.get_metadata()['equator_crossing_time']

    def equator_crossing_longitude(self, track):
        lon = track.get_metadata()['equator_crossing_longitude']
        if lon > 180:
            lon -= 360.
        return lon

    def resolution(self):
        return "5.8 km"

    def _get_rms_table_name(self):
        return
    
    def _get_rms_table_def(self):
        return
    
    def _get_rms_classes(self):
        return

    def _coeff_std(self):
        return 5.

    def _get_midrange_rms_poly(self):
        return

    def get_native_name(self, fieldname):
        """
        Return the GDR equivalent field name
        """
        translation = {
            'swh': 'swh',
            'swh_rms': 'swh_std',
            'swh_num_valid': 'swh_numval',
            'sigma0': 'sig0', 
            'wind_speed_alt': 'windsp_alt',
            'range_rms': 'range_std',
            'range': 'range',
            'off_nadir_angle_wf': 'off_nadir_wf'
        }
        if fieldname in translation:
            return translation[fieldname]

#     def customize_rejection_flag(self, rejfield):
#         rejfield.attributes['flag_meanings'] = ' '.join([
#             'altimeter_land_flag', 'sig0_ku_mle3_lower_than_5',
#             'sdmax_flag_0_5m', 'sdmax_flag_5_8m', 'sdmax_flag_over_8m',
#             'swh_outlier', 'swh_dev_from_mean_gt_5m', 'ssh_quality', 'swh_quality',
#             'sigma0_quality'
#             ])
#         print rejfield
#         rejfield.attributes['flag_masks'] += [64, 128, 256]

    def set_swh_quality(self, track):
        """
        set the quality information on SWH
        """        
        swh_ku = track.get_values('swh')
        swh_rms_ku = track.get_values('swh_std')
        swh_numval_ku = track.get_values('swh_numval')

        sigma0 = track.get_values('sig0')
        agc_numval = track.get_values('agc_numval')

        geo_bad1 = track.get_values('geo_bad1').astype(numpy.int8)
        land = (geo_bad1 & (1 << 1)) > 0
        ice = (geo_bad1 & (1 << 3)) > 0
        
        geo_bad2 = track.get_values('geo_bad2').astype(numpy.int8)
        rain = geo_bad2 & (1 << 0)

        # init quality fields             
        quality = self.l2p.get_values('swh_quality')
        quality[:] = QUALITY_GOOD
        
        rejection = self.l2p.get_field('swh_rejection_flags')
        rejection.get_values()[:] = 0

        # basic screening
        editing = (
            (swh_ku.filled(0) <= 0)
            | (swh_numval_ku != 8)
            | (agc_numval != 16)
            | ice
            | land
        ).filled(True)
        quality[editing] = QUALITY_BAD
        
        # undefined SWH values
        quality[
            (numpy.ma.getmaskarray(swh_ku) | numpy.ma.getmaskarray(swh_rms_ku))
            ] = QUALITY_UNDEFINED
        
        # set rejection flag
        self.set_mask_bit(rejection,
                          (ice | land),
                          'not_water')
        self.set_mask_bit(rejection,
                          (swh_ku.filled(0) <= 0),
                          'swh_validity')
        self.set_mask_bit(rejection,
                          (swh_numval_ku != 8),
                          'waveform_validity')
        self.set_mask_bit(rejection,
                          numpy.ma.getmaskarray(sigma0) | (agc_numval != 16),
                          'sigma0_validity')
        self.set_mask_bit(rejection,
                          swh_rms_ku.filled(0) <= 0,
                          'swh_rms_outlier')


    def set_wind_speed_alt_quality(self, track):
        """
        set the quality information on altimeter wind speed
        """
        quality = self.l2p.get_values('wind_speed_alt_quality')
        #quality[:] = QUALITY_GOOD
        
        quality_hs = self.l2p.get_values('swh_quality')
        quality[:] = quality_hs[:]

    def set_wind_speed_rad_quality(self, track):
        """
        set the quality information on altimeter wind speed
        """
        pass

    def adjust_swh(self, track):
        """compute calibrated SWH.
        """
        cycle = int(track.get_metadata()['cycle_number'])
        
        # for Side-A unit (up to cycle 235)
        if cycle <= 235:
            poly3 = numpy.poly1d([
                6.9624e-8,
                -7.7894e-6,
                -6.0426e-4,
                0.0864
                ])
            if cycle < 98:
                dh = 0.
            else:
                dh = poly3(98) - poly3(cycle)
            swh_corr = 1.0539 * track.get_values('swh') - 0.0766 + dh
        
        # for Side-B unit
        else:
            swh_corr = 1.0237 * track.get_values('swh') - 0.0476
                 
        return numpy.ma.masked_less(swh_corr, 0, copy=False)
    
    def __is_invalid_cycle(self):
        """
        Do not use the 10 first TOPEX cycles, date lower than or equal to
        31 Dec 1992.
        
        Do not use TOPEX miss-pointing cycles 433-437 included, i.e.  data
        between June 15, 2004 and August 2, 2004,  included. 
        
        Calibrated sigma0 Ku-band and corrected wind speed set to NaN over this
        time periods.
        """
        return (
            (self.l2p.get_end_time() <= datetime.datetime(1992, 12, 31)) |
            ((self.l2p.get_start_time() >= datetime.datetime(2004, 6, 12))
              & (self.l2p.get_end_time() <= datetime.datetime(2004, 8, 2)))
            ) 

    def swh_adjustment_attributes(self):
        if self.__is_invalid_cycle():
            formula = "All data set to invalid over this period"
        else:
            formula = (
                "Correction as a function of cycle number: for side-A (i.e. "
                "before cycle 236): Table 2-b for cycle numbers lower than 133,"
                " and Table 2-a for cycle 133 and greater, from  Hayne and "
                "Hancock, July 1999. For side-B(cycle 236 and greater): "
                "Table G-1 of Lockwood et al.,  July 2006.; \n"
                "An additional correction of - 0.4739 dB is applied for "
                "intercalibration with Envisat"
                )
        reference = "Global altimeter SWH dataset, P.Queffeulou, 2017"
        return formula, reference

    def swh_std_error(self):
        """compute the standard error of SWH
        
        1.96 x (STD_ERR_P1 x swh +STD_ERR_P0)
        """
        return 1.96 * (
            numpy.ma.maximum(1., self.l2p.get_values('swh_adjusted')) * 0.033
            + 0.021)

    def adjust_sigma0(self, track):
        """
        """
        if self.__is_invalid_cycle():
            # return masked values
            return self.l2p.get_values('sigma0_adjusted')
        
        # read calibration table
        CAL_SIG0_KU = resource_filename(
            'cciseastate',
            ('resources/%s' % 'topex_ku_sigma0_cal.txt')
        )

        with open(CAL_SIG0_KU) as cal_file:
            calsig0 = numpy.ma.fix_invalid(
                map(float, cal_file.read().split())
            )
        calsig0 = numpy.reshape(calsig0, (len(calsig0)/2, 2))
        
        # apply correction for current cycle
        cycle = track.get_metadata()['cycle_number']
        cind = numpy.where(
            calsig0[:, 0] == float(cycle)
            )[0]
        cal = calsig0[cind, 1]

        adjusted_sig0 = track.get_values('sig0') + cal

        return adjusted_sig0 - 0.4739

    def correct_sigma0_for_wspd(self):
        if self.__is_invalid_cycle():
            # return masked values
            return self.l2p.get_values('sigma0_adjusted')
        return self.l2p.get_values('sigma0_adjusted') - 0.15

    def sigma0_adjustment_attributes(self):
        if self.__is_invalid_cycle():
            formula = "All data set to invalid over this period"
        else:
            formula = 'sigma0_adjusted = sigma0 - 0.4739 dB'
        reference = "Global altimeter SWH dataset, P.Queffeulou, 2017"
        return formula, reference

    def topex_calibrated_sigma0(self):
        """for the period 1996-1997 (NSCAT colocation time frame.

        This is precalibration required to apply Gourrion wind speed calculation.
        Uses Queffeulou corrections.
        """
        swh = self.l2p.get_values('swh_adjusted')
        # remove adjustment to Envisat
        sigma0 = self.l2p.get_values('sigma0_adjusted') + 0.4739

        return swh, sigma0
