"""
Manages averaging of native frequency samples to 1 Hz measures
"""
import logging
from pathlib import Path

import numpy as np
from scipy.stats import binned_statistic

import cerbere

import cciseastate


# rejection flags for full freq measurements
HF_LAND = 1
HF_BAD_QUALITY = 2
HF_OUT_OF_RANGE = 4
HF_SIGMA0_OUT_OF_RANGE = 8

# rejection flags for 1 Hz SWH measurements
SWH_NUMVAL_1HZ = 1
SWH_VALIDITY_1HZ = 2

# rejection flags for 1 Hz sigma0 measurements
SIGMA0_NUMVAL_1HZ = 1
SIGMA0_VALIDITY_1HZ = 2

# minimum number of SWH values for 1 Hz averaging
MIN_SWH_VAL = 6

# minimum number of sigma0 values for 1 Hz averaging
MIN_SIGMA0_VAL = 6

# flag NetCDF attributes
FLAG_VALUES = {
    'quality': np.array([0, 1, 2, 3], dtype=np.int8)
}
FLAG_MASKS = {
    'swh_rejection': np.array([1, 2], dtype=np.int8),
    'sigma0_rejection': np.array([1, 2], dtype=np.int8)
}
FLAG_MEANINGS = {
    'quality': ' '.join(
        ['undefined', 'bad', 'acceptable', 'good']),
    'swh_rejection': ' '.join(
        ['nb_of_valid_swh_too_low', 'swh_validity']),
    'sigma0_rejection': ' '.join(
        ['nb_of_valid_sigma0_too_low', 'sigma0_validity'])
}


class Averaging(object):
    """Produces 1 Hz measurements from native frequency samples

    It uses information from the original GDR/SGDR files to guarantee the same
    samples are used in each 1 Hz cell.

    According to Passaro's pseudo-code:

    For every group of 20-Hz measurements:

    1st Outlier Check (Eliminate = put as NaN)
        - Eliminate all points with a bad quality Flag
        - Eliminate all points with unrealistic SWH (you will have to decide
          what is unrealistic...)
        - Eliminate all points with unrealistic sigma0 (anything bigger than
          30dB and lower than 7dB). you cannot do this on CS2, since sigma0
          there is not calibrated

    2nd Outlier Check: Use MAD-based outlier detection. you can use this python
     function, which I attach

    # MP DEFINITION OF FUNCTION TO ELIMINATE OUTLIERS USING THE MAXIMUM ABSOLUTE
     DEVIATION for a 3sigma criterion
    def MADoutlier(SSH_vector,SSH_measurement):
        MAD_SSH = 1.4286
            * (np.nanmedian(np.absolute(SSH_vector-np.nanmedian(SSH_vector))))
        if SSH_measurement < (np.nanmedian(SSH_vector) -3*MAD_SSH )
                or SSH_measurement > (np.nanmedian(SSH_vector) +3*MAD_SSH ):
            SSH_measurement=np.nan

        return SSH_measurement

    3nd Outlier Check: If there are at least 6 reamining SWH observations after
     all the checks above, compute the median

    if np.size(np.isnan(swh_hf)==False)>5:
        swh_1hz.append(np.nanmedian(swh_hf))

    Note also that I have no solution for which position to assign to the 1-Hz
    average of 20 20-Hz measurements: we simply take the 1-Hz point from the
    GDR, but you cannot do it since you only have 20-Hz. The best you can do is
    to either take the interpolated mean position of the 20 lat-lon points, or
    to use the SS_CCI v1 1-Hz positions.

    Finally, most of these criteria are the same that I suggested in Passaro
    et al. (2015):

    Passaro, M., Fenoglio-Marc, L. and Cipollini, P., 2014. Validation of
    significant wave height from improved satellite altimetry in the German
    Bight. IEEE Transactions on Geoscience and Remote Sensing, 53(4),
    pp.2146-2156. doi: 10.1109/TGRS.2014.2356331
    """
    def __init__(self):
        self.min_swh_numval = MIN_SWH_VAL
        self.min_sigma0_numval = MIN_SIGMA0_VAL
        self.operator = 'median'

    def mad_outlier(self, values):
        median_swh = np.median(values)
        mad = 1.4286 * np.median(np.absolute(values - median_swh))
        return np.ma.masked_outside(
            values, median_swh - 3 * mad, median_swh + 3 * mad).compressed()

    def _value_1hz_median(self, values):
        # remove outliers
        values = self.mad_outlier(values)
        if values.size > 0:
            return np.median(values)

    def _value_1hz_mean(self, values):
        # remove outliers
        values = self.mad_outlier(values)
        if values.size > 0:
            return np.mean(values)

    def _latitude_1hz_mean(self, values):
        values = np.array(values)
        if values.size == 0:
            return
        return np.mean(values)

    def _longitude_1hz_mean(self, values):
        values = np.array(values)
        if values.size == 0:
            return
        if values.min() * values.max() < 0:
            # manage dateline crossing
            values[values < 0] += 360
            vmean = values.mean()
            if vmean > 180.:
                vmean -= 360.
            return vmean
        else:
            return np.mean(values)

    def _value_1hz_median_rms(self, values):
        # remove outliers
        values = self.mad_outlier(values)
        if values.size > 0:
            return np.sqrt(np.power(values - np.median(values), 2).mean())

    def _value_1hz_mean_rms(self, values):
        # remove outliers
        values = self.mad_outlier(values)
        if values.size > 0:
            return (values - np.mean(values)).std()

    def _numval_1hz(self, swh):
        # remove outliers
        return self.mad_outlier(swh).size

    def get_1hz_value(self, high_to_1hz_match, values,
                      bin_count, operator='median'):
        """Return 1Hz value from full frequency measurements"""
        if operator == 'median':
            swh_1hz = np.ma.fix_invalid(
                binned_statistic(
                    high_to_1hz_match, values,
                    statistic=self._value_1hz_median,
                    bins=bin_count,
                    range=[0, bin_count]
                ).statistic)
            swh_1hz_rms = np.ma.fix_invalid(
                binned_statistic(
                    high_to_1hz_match, values,
                    statistic=self._value_1hz_median_rms,
                    bins=bin_count,
                    range=[0, bin_count]
                ).statistic)
        elif operator == 'mean':
            swh_1hz = np.ma.fix_invalid(
                binned_statistic(
                    high_to_1hz_match, values,
                    statistic=self._value_1hz_mean,
                    bins=bin_count,
                    range=[0, bin_count]
                ).statistic)
            swh_1hz_rms = np.ma.fix_invalid(
                binned_statistic(
                    high_to_1hz_match, values,
                    statistic=self._value_1hz_mean_rms,
                    bins=bin_count,
                    range=[0, bin_count]
                ).statistic)

        else:
            raise ValueError("Bad operator: {}".format(operator))

        return swh_1hz, swh_1hz_rms

    def get_1hz_numval(self, high_to_1hz_match, swh, bin_count):
        """Return the number of valid measurements used to get the 1 Hz value
        """
        numval_1hz = np.ma.fix_invalid(
            binned_statistic(
                high_to_1hz_match, swh,
                statistic=self._numval_1hz,
                bins=bin_count,
                range=[0, bin_count]
            ).statistic)

        return numval_1hz.astype(np.int8)

    def get_1hz_values(
            self, retracked_values, quality, high_to_1hz_match, count_1hz,
            min_numval, rejection_flags, operator='median'):
        """averaging SWH to 1 Hz"""
        retracked_values = retracked_values[quality == cciseastate.QUALITY_GOOD]
        assert (retracked_values.size == retracked_values.count())
        #if len(retracked_values) > 0:
        #    assert (np.isnan(retracked_values).sum() == 0)

        kwargs = {}
        values_1hz, rms_1hz = self.get_1hz_value(
            high_to_1hz_match[quality == cciseastate.QUALITY_GOOD],
            retracked_values,
            count_1hz,
            operator=operator,
            **kwargs)

        numval_1hz = self.get_1hz_numval(
            high_to_1hz_match[quality == cciseastate.QUALITY_GOOD],
            retracked_values,
            count_1hz,
            **kwargs).astype(np.int8)

        # set quality flag
        size = values_1hz.size
        quality_1hz = np.ma.ones(size, dtype=np.int8) \
            * cciseastate.QUALITY_GOOD
        quality_1hz[values_1hz.mask] = cciseastate.QUALITY_BAD
        quality_1hz[numval_1hz < min_numval] = cciseastate.QUALITY_BAD

        # set rejection flag
        rejection_1hz = np.ma.zeros(size, dtype=np.int8)
        rejection_1hz[numval_1hz < min_numval] = rejection_flags[0]
        rejection_1hz[
            (numval_1hz >= self.min_swh_numval)
            & (quality_1hz == cciseastate.QUALITY_BAD)
            ] = rejection_flags[1]

        return values_1hz, rms_1hz, numval_1hz, quality_1hz, rejection_1hz,

    @classmethod
    def basic_check(cls, values, landmask, validity, quality_flag=None):
        """Elementary checks to remove bad retrievals"""
        # quality fields
        size = values.size
        quality = np.ma.ones(size, dtype=np.int8) * cciseastate.QUALITY_GOOD
        rejection = np.ma.ones(size, dtype=np.int8)

        # Eliminate all points over land
        bad = (landmask == 1)
        quality[bad] = cciseastate.QUALITY_BAD
        rejection[bad] += HF_LAND
        logging.info("   land: {}".format(bad.sum()))

        # Eliminate all points with a bad quality Flag
        if quality_flag is not None:
            bad = quality_flag == 1
            quality[bad] = cciseastate.QUALITY_BAD
            rejection[bad] += HF_BAD_QUALITY
            logging.info("   quality flag: {}".format(bad.sum()))

        # Eliminate all points with unrealistic sigma0
        logging.info("   valid range: {}".format(validity))
        bad = np.ma.masked_outside(values, *validity).mask
        logging.info('    out of range:: {} over {}'.format(bad.sum(), size))
        quality[bad] = cciseastate.QUALITY_BAD
        rejection[bad] += HF_OUT_OF_RANGE

        return quality, rejection

    @classmethod
    def basic_check_swh(
            cls, retracked_swh, retracked_sigma0, retracked_quality, landmask,
            sigma0_range=[7., 30.], swh_range=[-0.5, 25]):
        """Elementary checks to remove bad SWH retrievals"""
        quality, rejection = cls.basic_check(
            retracked_swh, landmask, swh_range, quality_flag=retracked_quality)

        # Test removed after PM-7 discussion
        # # Eliminate all points with unrealistic sigma0
        # size = retracked_swh.size
        # logging.info("  Sigma0 valid range: {}".format(sigma0_range))
        # bad_sig0 = np.ma.masked_outside(retracked_sigma0, *sigma0_range).mask
        # logging.info('  Out of range:: {} over {}'.format(bad_sig0.sum(), size))
        #
        # quality[bad_sig0] = cciseastate.QUALITY_BAD
        # rejection[bad_sig0] += HF_SIGMA0_OUT_OF_RANGE
        #
        return quality, rejection

    @classmethod
    def basic_check_sigma0(
            cls, retracked_sigma0, landmask, retracked_quality,
            sigma0_range=[7., 30.]):
        """Elementary checks to remove bad sigma0 retrievals"""
        return cls.basic_check(
            retracked_sigma0, landmask, sigma0_range,
            quality_flag=retracked_quality)

    def run(self,
            l2p_processor,
            retracked_track,
            gdr_track_hf,
            gdr_track_1hz,
            outdir,
            landmask,
            corrections,
            conf):
        """
        Transform high frequence retracked file to 1 Hz measurement file.
        Args:

        Returns:

        """
        # averaging parameters
        if 'min_swh_numval' in conf:
            self.min_swh_numval = conf['min_swh_numval']

        if 'min_sigma0_numval' in conf:
            self.min_sigma0_numval = \
                conf['min_sigma0_numval']

        if 'operator' in conf:
            self.operator = conf['operator']

        # get full frequency to 1 Hz matching from (S)GDR
        count_1hz, high_to_1hz_match = l2p_processor.matching_1hz(
            gdr_track_hf, retracked_track)

        # read full frequency Hz data
        retracked_swh = l2p_processor.swh_20hz(retracked_track)

        # corrrections
        retracked_swh_corr = l2p_processor.corrections_20hz(retracked_track)

        # import matplotlib.pyplot as plt
        # fig = plt.figure(figsize=(10, 10))
        # plt.scatter(retracked_swh, retracked_swh_corr, s=0.1)
        # plt.ylim([-0.5, 0.5])
        # plt.xlim([0, 14])
        # plt.show()

        # quality of 20 Hz data
        rt_swh_quality = l2p_processor.quality_20hz(
            retracked_track, gdr_track_hf)

        # sigma0
        rt_sigma0 = l2p_processor.sigma0_20hz(retracked_track)

        # first SWH outlier check
        logging.info("Processing raw SWH")
        logging.info("First SWH outlier check")
        kwargs = {}
        if 'swh_valid_range' in conf:
            kwargs['swh_range'] = conf['swh_valid_range']
        if 'sigma0_valid_range' in conf:
            kwargs['sigma0_range'] = conf['sigma0_valid_range']
        rt_swh_quality, rt_swh_rejection = self.basic_check_swh(
            retracked_swh, rt_sigma0, rt_swh_quality,
            landmask.get_values('land_mask'), **kwargs)

        # averaging raw SWH
        logging.info("Averaging SWH to 1 Hz")
        logging.info("   min nb of SWH 20 Hz values: {}"
                     .format(self.min_swh_numval))
        logging.info("   averaging method: {}".format(self.operator))

        res = self.get_1hz_values(
            retracked_swh, rt_swh_quality, high_to_1hz_match, count_1hz,
            self.min_swh_numval, [SWH_NUMVAL_1HZ, SWH_VALIDITY_1HZ],
            operator=self.operator
        )
        swh_1hz, rms_1hz, numval_1hz, quality_swh_1hz, rejection_swh_1hz = res

        assert (swh_1hz.size == count_1hz)
        if gdr_track_1hz is not None:
            assert (swh_1hz.size == gdr_track_1hz.dims['time'])

        # averaging corrected SWH
        logging.info("Processing corrected SWH")
        if retracked_swh_corr.count() > 0:

            rt_corr_swh_quality, rt_corr_swh_rejection = self.basic_check_swh(
                retracked_swh + retracked_swh_corr,
                rt_sigma0, rt_swh_quality, landmask.get_values('land_mask'),
                **kwargs)

            res = self.get_1hz_values(
                retracked_swh + retracked_swh_corr, rt_corr_swh_quality,
                high_to_1hz_match, count_1hz, self.min_swh_numval,
                [SWH_NUMVAL_1HZ, SWH_VALIDITY_1HZ],
                operator=self.operator
            )
            corr_swh_1hz, corr_rms_1hz, corr_numval_1hz, \
                corr_quality_swh_1hz, corr_rejection_swh_1hz = res
        else:
            logging.error("No valid SWH correction in file")
            corr_swh_1hz = np.ma.masked_all((swh_1hz.size,))
            corr_rms_1hz = np.ma.masked_all((swh_1hz.size,))
            corr_numval_1hz = np.ma.zeros((swh_1hz.size,), dtype=np.int8)
            corr_quality_swh_1hz = np.ma.ones((swh_1hz.size,), dtype=np.int8) \
                * cciseastate.QUALITY_UNDEFINED
            corr_rejection_swh_1hz = np.ma.ones((swh_1hz.size,), dtype=np.int8)\
                * SWH_VALIDITY_1HZ

        # fig = plt.figure(figsize=(10, 10))
        # plt.scatter(swh_1hz, corr_swh_1hz - swh_1hz, s=0.1)
        # plt.ylim([-0.3, 0.])
        # plt.xlim([0, 5])
        # plt.show()
        # outlier = np.where(corr_swh_1hz - swh_1hz < -0.25)
        # print(outlier, corr_swh_1hz[outlier], swh_1hz[outlier])
        # indout = np.where(high_to_1hz_match==320)
        # print("indices 320: ", indout)
        # print("CORR QUAL ", rt_corr_swh_quality[indout])
        # print("RAW QUAL ", rt_swh_quality[indout])
        # print("CORR HS ", retracked_swh[indout] + retracked_swh_corr[indout])
        # print("RAW HS ", retracked_swh[indout])

        # averaging sigma0
        logging.info("Averaging sigma0 to 1 Hz")
        logging.info("   min nb of sigma0 20 Hz values: {}"
                     .format(self.min_sigma0_numval))
        logging.info("   averaging method: {}".format(self.operator))
        kwargs.pop('swh_range')
        rt_sigma0_quality, rt_sigma0_rejection = self.basic_check_sigma0(
            rt_sigma0, landmask.get_values('land_mask'), rt_swh_quality,
            **kwargs)

        res = self.get_1hz_values(
            rt_sigma0, rt_sigma0_quality, high_to_1hz_match, count_1hz,
            self.min_sigma0_numval, [SIGMA0_NUMVAL_1HZ, SIGMA0_VALIDITY_1HZ],
            operator=self.operator
        )
        sigma0_1hz, sigma0_rms_1hz, numval_sigma0_1hz, \
            quality_sigma0_1hz, rejection_sigma0_1hz = res

        # build output dataset
        fname_1hz = (
            outdir
            / Path(retracked_track.url.parts[-3])
            / Path(retracked_track.url.parts[-2])
            / retracked_track.url.name
        )
        if not fname_1hz.parent.exists():
            fname_1hz.parent.mkdir(parents=True)
        if fname_1hz.exists():
            logging.info("Removing previous 1 Hz file: {}".format(fname_1hz))
            fname_1hz.unlink()

        # calculate lat, lon and times of 1 Hz measurements
        if gdr_track_1hz is not None:
            times = gdr_track_1hz.get_times()
            lat = gdr_track_1hz.get_lat()
            lon = gdr_track_1hz.get_lon()
        else:
            times = np.ma.fix_invalid(
                binned_statistic(
                    high_to_1hz_match,
                    retracked_track.get_times().astype('int64'),
                    statistic=self._value_1hz_mean,
                    bins=count_1hz,
                    range=[0, count_1hz]
                ).statistic)
            times = times.astype('datetime64[ns]')

            lat = np.ma.fix_invalid(
                binned_statistic(
                    high_to_1hz_match, retracked_track.get_lat(),
                    statistic=self._latitude_1hz_mean,
                    bins=count_1hz,
                    range=[0, count_1hz]
                ).statistic)

            lon = np.ma.fix_invalid(
                binned_statistic(
                    high_to_1hz_match,
                    retracked_track.get_lon(),
                    statistic=self._longitude_1hz_mean,
                    bins=count_1hz,
                    range=[0, count_1hz]
                ).statistic)

        dst = {
            'time': {'dims': ('time'), 'data': times},
            'lat': {'dims': ('time'), 'data': lat},
            'lon': {'dims': ('time',), 'data': lon},

            'swh': {'dims': ('time',), 'data': swh_1hz},
            'swh_rms': {'dims': ('time',), 'data': rms_1hz},
            'swh_numval': {'dims': ('time',), 'data': numval_1hz},
            'swh_quality': {
                'dims': ('time',), 'data': quality_swh_1hz,
                'attrs': {'flag_values': FLAG_VALUES['quality'],
                          'flag_meanings': FLAG_MEANINGS['quality']}},
            'swh_rejection': {
                'dims': ('time',), 'data': rejection_swh_1hz,
                'attrs': {
                    'flag_values': FLAG_MASKS['swh_rejection'],
                    'flag_meanings': FLAG_MEANINGS['swh_rejection']}
                },
            'swh_corrected': {'dims': ('time',), 'data': corr_swh_1hz},
            'swh_corrected_rms': {'dims': ('time',), 'data': corr_rms_1hz},
            'swh_corrected_numval': {
                'dims': ('time',), 'data': corr_numval_1hz},
            'swh_corrected_quality': {
                'dims': ('time',), 'data': corr_quality_swh_1hz,
                'attrs': {'flag_values': FLAG_VALUES['quality'],
                          'flag_meanings': FLAG_MEANINGS['quality']}
            },
            'swh_corrected_rejection': {
                'dims': ('time',), 'data': corr_rejection_swh_1hz,
                'attrs': {
                    'flag_values': FLAG_MASKS['swh_rejection'],
                    'flag_meanings': FLAG_MEANINGS['swh_rejection']}
            },
            'sigma0': {'dims': ('time',), 'data': sigma0_1hz},
            'sigma0_rms': {'dims': ('time',), 'data': sigma0_rms_1hz},
            'sigma0_numval': {'dims': ('time',), 'data': numval_sigma0_1hz},
            'sigma0_quality': {
                'dims': ('time',), 'data': quality_sigma0_1hz,
                'attrs': {
                    'flag_values': FLAG_VALUES['quality'],
                    'flag_meanings': FLAG_MEANINGS['quality']},
            },
            'sigma0_rejection': {
                'dims': ('time',), 'data': rejection_sigma0_1hz,
                'attrs': {
                    'flag_values': FLAG_MASKS['sigma0_rejection'],
                    'flag_meanings': FLAG_MEANINGS['sigma0_rejection']}
            },
        }
        track_1hz = cerbere.open_as_feature(
            'Trajectory', dst, 'NCDataset'
        )
        logging.info('Saving {}'.format(fname_1hz))
        track_1hz.save(str(fname_1hz))
