"""
Functions to read configuration file
"""
from pathlib import Path
import yaml


def read_config(config_file, platform):
    with open(config_file, 'r') as fcfg:
        config = yaml.load(fcfg, Loader=yaml.FullLoader)

    if platform not in config['missions']:
        raise ValueError("Unknown platform: {}".format(platform))

    cfg = config['missions'][platform]
    cfg['aux_path'] = Path(config['globals']['auxiliary'])

    return cfg
