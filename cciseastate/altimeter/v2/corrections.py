"""
Manages addition of SWH errors provided by PML/Graham Quartly


"""
from pkg_resources import resource_filename

import numpy as np
import xarray as xr

CORRECTION_VAR = {
    'JASON-3': 'dswh_j3',
    'ENVISAT': 'dswh_en',
    'SARAL': 'dswh_al',
    'CRYOSAT-2': 'dswh_cs',
}


class Corrections(object):
    """Produces corrections for the WHALES retracker on full frequency
    measurements.

    PML instructions:

    PML were tasked with finding the corrections for the WHALES retracker
    applied to CS2, EN & AL.  I provide here one file with LUT corrections for
    all the altimeter missions of interest (plus Jason-3).  As you will see, the
    LUT is in steps of 5cm from 0m to 10m (below 0m, use correction for 0m;
    above 10m use correction for 10m).
    Take output from retracker; look up nearest correction (no real need to
    interpolate unless you wish to), and then ADD correction to the WHALES value

    All values were tabulated from the corrections in the appropriate datafiles.
    For AL, CS2 & J3 the correction in the files is ONLY one of Hs; for EN, the
    correction in the files is the result of calculations that are not open to
    the user, so I have calculated a mean variations with Hs.  For EN this does
    vary with time (again it is unclear how), but the mean corrections should be
    good to within about 2-3 cm.
    [Paolo, the Envisat_Altimetry_Products_ReadMe_v2.pdf says
     on p.3 that there are "New SWH modeled instrumental corrections" -- I have
     tried ESA HelpDesk but they are unable to provide me with further info on
     this.]

    """
    def __init__(self):
        self.lut = xr.open_dataset(resource_filename(
            'cciseastate',
            ('resources/v2/WHALES_Hs_correction.nc')
        ), decode_cf=False)

    def corrections(self, track, mission):
        """Return full frequency SWH corrections"""
        lut_indices = np.digitize(
            track.get_values('swh_WHALES_20hz').filled(-1),
            self.lut.hs0

        )
        lut_indices -= 1
        # for SWH above LUT limits, apply the last correction in LUT range
        lut_indices[lut_indices >= self.lut.dims['x']] = self.lut.dims['x'] - 1

        return np.ma.masked_where(
            lut_indices < 0,
            self.lut[CORRECTION_VAR[mission]][lut_indices])

