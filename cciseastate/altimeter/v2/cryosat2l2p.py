"""
Specific Cryosat-2 processing for the processing of a V2 retracked altimeter
product to CCI format and content.

:copyright: Copyright 2020 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import datetime
import logging
from pathlib import Path

import cerbere

import cciseastate.altimeter.v2.whalesl2p as whalesl2p


class CryoSat2L2P(whalesl2p.WHALESL2P):

    def satellite(self):
        return 'CryoSat-2'

    def instrument(self):
        return 'SIRAL'

    def band(self):
        return 'Ku'

    def resolution(self):
        return "6 km"

    def cycle(self, track):
        self.init_gdr_01_hz(track)
        return self.gdr_01hz.attrs['cycle_number']

    def relative_pass(self, track):
        return

    def equator_crossing_time(self, track):
        self.init_gdr_01_hz(track)
        return datetime.datetime.strptime(
            self.gdr_01hz.attrs['equator_cross_time'].replace('UTC=', ''),
            '%Y-%m-%dT%H:%M:%S.%f'
        )

    def equator_crossing_longitude(self, track):
        self.init_gdr_01_hz(track)
        lon = float(self.gdr_01hz.attrs['equator_cross_long']) / 100000
        if lon > 180:
            lon -= 360.
        return lon

    @classmethod
    def retracked_20hz_feature(cls, fname):
        """return retracked 20 Hz feature as a cerbere trajectory"""
        return cerbere.open_as_feature('Trajectory', fname, 'WHALESNCDataset')

    @classmethod
    def matching_1hz(cls, track, retracked_track):
        return track.dims['time_avg_01_ku'],\
               track.get_values('ind_meas_1hz_20_ku'),

    @classmethod
    def get_hf_time_units(cls, gdr_track):
        """return the time difference between first measurement and reference
        time
        """
        return gdr_track.get_coord('time').to_dataarray().encoding['units']

    @classmethod
    def gdr_track_01hz(cls, retracked_fname, gdr_path):
        gdr_fname = (
            gdr_path
            / Path(retracked_fname.parts[-3])
            / Path(retracked_fname.parts[-2])
            / retracked_fname.name
        )
        return cerbere.open_as_feature(
            'Trajectory', gdr_fname, 'NCDataset',
            field_matching={
                'time_avg_01_ku': 'time',
                'lat_avg_01_ku': 'lat',
                'lon_avg_01_ku': 'lon'},
            dim_matching={'time_avg_01_ku': 'time'}
        )

    @classmethod
    def gdr_track_hf(cls, retracked_fname, gdr_path):
        gdr_fname = (
            gdr_path
            / Path(retracked_fname.parts[-3])
            / Path(retracked_fname.parts[-2])
            / retracked_fname.name
        )
        return cerbere.open_as_feature(
            'Trajectory', gdr_fname, 'NCDataset',
            field_matching={'time_20_ku': 'time',
                            'lat_20_ku': 'lat',
                            'lon_20_ku': 'lon'},
            dim_matching={'time_20_ku': 'time'}
        )

    def _get_rms_table_name(self):
        return 'cci_swh_rms_LUT_cryosat-2.dat'

    def _coeff_std(self):
        return 5.

    def swh_uncertainty_factors(cls):
        return 0.058, 0.040

    @property
    def _calibration_table_name(self):
        return 'tab_calibration_lut_cryosat-2.dat'

    def merge_auxiliaries(self, track):
        super(CryoSat2L2P, self).merge_auxiliaries(track)

        # add GDR info
        try:
            dst = self.gdr_track_01hz(
                track.url, self.configuration['gdr_root'])
            self.l2p.get_field('wind_speed_alt').set_values(
                dst.get_values('wind_speed_alt')
            )

        except IOError:
            logging.error("GDR file could not be found.")
