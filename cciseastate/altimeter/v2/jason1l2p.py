"""
Specific Jason processing for the processing of a V2 retracked altimeter
product to CCI format and content.

:copyright: Copyright 2020 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
from .jasonl2p import JasonL2P


class Jason1L2P(JasonL2P):

    def satellite(self):
        return 'Jason-1'

    def instrument(self):
        return 'Poseidon-2'

    @property
    def _calibration_table_name(self):
        return 'tab_calibration_lut_jason-1.dat'

    def _get_rms_table_name(self):
        return 'cci_swh_rms_LUT_jason-1.dat'

    def _coeff_std(self):
        return 5.

    def swh_uncertainty_factors(cls):
        return 0.054, 0.095





