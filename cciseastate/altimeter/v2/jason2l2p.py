"""
Specific Jason processing for the processing of a V2 retracked altimeter
product to CCI format and content.

:copyright: Copyright 2020 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import cerbere

from .jasonl2p import JasonL2P


class Jason2L2P(JasonL2P):

    def satellite(self):
        return 'Jason-2'

    def instrument(self):
        return 'Poseidon-3'

    def _get_rms_table_name(self):
        return 'cci_swh_rms_LUT_jason-2.dat'

    def _coeff_std(self):
        return 5.

    def swh_uncertainty_factors(cls):
        return 0.048, 0.101

    @property
    def _calibration_table_name(self):
        return 'tab_calibration_lut_jason-2.dat'

    @classmethod
    def retracked_20hz_feature(cls, fname):
        """return retracked 20 Hz feature as a cerbere trajectory"""
        return cerbere.open_as_feature(
            'Trajectory', fname, 'WHALESNCDataset')

    # @classmethod
    def matching_1hz(cls, track, retracked_track):
        return track.dims['time'] / 20, np.arange(track.dims['time']) // 20,
