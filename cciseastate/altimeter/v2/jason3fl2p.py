"""
Specific Jason-3 version F processing for the processing of a V2 retracked
altimeter product to CCI format and content.

:copyright: Copyright 2020 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import logging
from pathlib import Path

import cerbere

from .jasonl2p import JasonL2P


class Jason3FL2P(JasonL2P):

    def satellite(self):
        return 'Jason-3'

    def instrument(self):
        return 'Poseidon-3b'

    def _get_rms_table_name(self):
        return 'cci_swh_rms_LUT_jason-3.dat'

    def _coeff_std(self):
        return 5.

    def swh_uncertainty_factors(cls):
        return 0.048, 0.087

    @property
    def _calibration_table_name(self):
        return 'tab_calibration_lut_jason-3.dat'

    @classmethod
    def retracked_20hz_feature(cls, fname):
        """return retracked 20 Hz feature as a cerbere trajectory"""
        return cerbere.open_as_feature(
            'Trajectory', fname, 'WHALESNCDataset')

    @classmethod
    def gdr_track_01hz(cls, retracked_fname, gdr_path):
        gdr_fname = (
            gdr_path
            / Path(retracked_fname.parts[-3])
            / Path(retracked_fname.parts[-2])
            / retracked_fname.name
        )
        return cerbere.open_as_feature(
            'Trajectory', gdr_fname, 'AVISOJason3F1HzNCDataset')

    @classmethod
    def gdr_track_20hz(cls, retracked_fname, gdr_path):
        gdr_fname = (
            gdr_path
            / Path(retracked_fname.parts[-3])
            / Path(retracked_fname.parts[-2])
            / retracked_fname.name
        )
        return cerbere.open_as_feature(
            'Trajectory', gdr_fname, 'AVISOJason3F20HzNCDataset')

    @classmethod
    def gdr_track_hf(cls, retracked_fname, gdr_path):
        return cls.gdr_track_20hz(retracked_fname, gdr_path)

    @classmethod
    def matching_1hz(cls, track, retracked_track):
        matching = track.get_values('index_1hz_measurement')
        return matching.max()+1, matching,

    def merge_auxiliaries(self, track):
        super(JasonL2P, self).merge_auxiliaries(track)

        # add GDR info
        try:
            dst = self.gdr_track_01hz(
                track.url, self.configuration['gdr_root'])
            self.l2p.get_field('wind_speed_alt').set_values(
                dst.get_values('wind_speed_alt')
            )
            self.l2p.get_field(
                'total_column_liquid_water_content_rad').set_values(
                    dst.get_values('rad_cloud_liquid_water')
            )

        except IOError:
            logging.error("GDR file could not be found.")