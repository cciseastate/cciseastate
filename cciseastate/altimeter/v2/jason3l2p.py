"""
Specific Jason processing for the processing of a V2 retracked altimeter
product to CCI format and content.

:copyright: Copyright 2020 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import numpy as np

from .jasonl2p import JasonL2P


class Jason3L2P(JasonL2P):

    def satellite(self):
        return 'Jason-3'

    def instrument(self):
        return 'Poseidon-3b'

    def _get_rms_table_name(self):
        return 'cci_swh_rms_LUT_jason-3.dat'

    def _coeff_std(self):
        return 5.

    def swh_uncertainty_factors(cls):
        return 0.048, 0.087

    @property
    def _calibration_table_name(self):
        return 'tab_calibration_lut_jason-3.dat'
