"""
Specific Jason processing for the processing of a V2 retracked altimeter
product to CCI format and content.

:copyright: Copyright 2020 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import datetime
import logging
from pathlib import Path

import numpy as np

import cerbere

from .matching import track_matching
import cciseastate.altimeter.v2.whalesl2p as whalesl2p


# reference time
REFERENCE_TIME = datetime.datetime(1980, 1, 1)


class JasonL2P(whalesl2p.WHALESL2P):

    def band(self):
        return 'Ku'

    def cycle(self, track):
        self.init_gdr_01_hz(track)
        return self.gdr_01hz.attrs['cycle_number']

    def relative_pass(self, track):
        self.init_gdr_01_hz(track)
        return self.gdr_01hz.attrs['pass_number']

    def equator_crossing_time(self, track):
        self.init_gdr_01_hz(track)
        return self.gdr_01hz.attrs['equator_time']

    def equator_crossing_longitude(self, track):
        self.init_gdr_01_hz(track)
        lon = self.gdr_01hz.attrs['equator_longitude']
        if lon > 180:
            lon -= 360.
        return lon

    def resolution(self):
        return "5.8 km"

    @classmethod
    def retracked_20hz_feature(cls, fname):
        """return retracked 20 Hz feature as a cerbere trajectory"""
        return cerbere.open_as_feature(
            'Trajectory', fname, 'JASONWHALESNCDataset')

    @classmethod
    def matching_1hz(cls, gdr_track, retracked_track):
        """
        Args:
            gdr_track:
            retracked_track:

        Returns:
            A tuple with:
                * the list of indices of 1 Hz cells in original SGDR
                * the indices of SGDR 1 Hz cell that have a match in the
                  retracked file. As some trimming/editing is done in some
                  WHALES retracked files, that may result in a different size of
                  1 Hz final product compared to (S)GDR. We want to keep the
                  same number of 1 hz data in both products (filling missing
                  data with fill values)
                * the indices of SGDR 1 Hz cells for each 20 hz measurement in
                  retracked file.
        """
        indices = track_matching(retracked_track, gdr_track)
        assert(len(indices[0]) == retracked_track.dims['time'])
        assert(np.unique(indices[0]).size == len(indices[0]))

        indices_1hz = np.arange(gdr_track.dims['time'] // 20)
        gdr_1hz_matching = np.repeat(indices_1hz, 20)[indices[1]]
        tmp = np.unique(gdr_1hz_matching, return_counts=True)
        return len(indices_1hz), gdr_1hz_matching,

    @classmethod
    def get_hf_time_units(cls, gdr_track):
        """return the time difference between first measurement and reference
        time
        """
        return gdr_track.get_coord('time').to_dataarray().encoding['units']

    @classmethod
    def gdr_track_01hz(cls, retracked_fname, gdr_path):
        gdr_fname = (
            gdr_path
            / Path(retracked_fname.parts[-3])
            / Path(retracked_fname.parts[-2])
            / retracked_fname.name
        )
        return cerbere.open_as_feature(
            'Trajectory', gdr_fname, 'NCDataset')

    @classmethod
    def gdr_track_hf(cls, retracked_fname, gdr_path):
        gdr_fname = (
            gdr_path
            / Path(retracked_fname.parts[-3])
            / Path(retracked_fname.parts[-2])
            / retracked_fname.name
        )
        dst = cerbere.open_dataset(
            gdr_fname, 'NCDataset',
            field_matching={
                'lat_20hz': 'lat', 'lon_20hz': 'lon',
                'lat': 'lat_01', 'lon': 'lon_01',
            },
        ).to_xarray()

        # squeeze records dimension
        dst = dst.rename({'time': 'time_01'})
        dst = dst.rename({'time_20hz': 'time'})
        dst = dst.rename_dims({'time_01': 'time_old'})
        dst = dst.stack(time_dim=('time_old', 'meas_ind'))
        dst = dst.swap_dims(
            {'time_dim': 'time'})

        return cerbere.open_as_feature('Trajectory', dst)

    def merge_auxiliaries(self, track):
        super(JasonL2P, self).merge_auxiliaries(track)

        # add GDR info
        try:
            dst = self.gdr_track_01hz(
                track.url, self.configuration['gdr_root'])
            self.l2p.get_field('wind_speed_alt').set_values(
                dst.get_values('wind_speed_alt')
            )
            self.l2p.get_field(
                'total_column_liquid_water_content_rad').set_values(
                    dst.get_values('rad_liquid_water')
            )

        except IOError:
            logging.error("GDR file could not be found.")