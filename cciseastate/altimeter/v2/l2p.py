"""
Base processor for the conversion of a GDR altimeter product to CCI format and
content, v2.

:copyright: Copyright 2020 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
from collections import OrderedDict
import datetime
import logging
from pathlib import Path
from pkg_resources import resource_filename

import numpy as np
import xarray as xr

import cerbere

import cciseastate.altimeter.l2p as l2p

VERSION = "2.0"

# variables to be copied from GDR to L2P
SOURCE_FIELDS = ['sigma0', 'sigma0_rms', 'sigma0_num_valid',
                 'sigma0_quality', 'sigma0_rejection',
                 'swh_quality', 'swh_rejection',
                 'swh', 'swh_rms', 'swh_num_valid']


# variables in L2P
VARIABLES = OrderedDict([
    ('sigma0', 'Ku band backscatter coefficient'),
#    ('sigma0_adjusted', 'Ku band adjusted backscatter coefficient'),
    ('sigma0_rms', 'RMS of the high frequency Ku band backscatter coefficient'),
    ('sigma0_num_valid',
     'number of high frequency valid points used to compute Ku band backscatter coefficient'),
#    ('mss', 'mean square slope from Ku band backscatter'),
    ('swh', 'Ku band significant wave height'),
    ('swh_adjusted', 'Ku band adjusted significant wave height'),
    ('swh_quality', 'quality of Ku band significant wave height measurement'),
    ('swh_uncertainty', 'best estimate of significant wave height standard error'),
    ('swh_rms', 'RMS of the Ku band significant wave height (from high frequency measurements)'),
    ('swh_num_valid', 'number of high frequency valid points used to compute Ku band significant wave height'),
    ('swh_rejection_flags', 'consolidated instrument and ice flags'),
#    ('off_nadir_angle_wf', 'square of the off nadir angle computed from waveforms'),
#    ('range', 'Ku band range'),
#    ('range_rms', 'RMS of the Ku band range'),
    ('wind_speed_alt', 'wind speed from Ku band altimeter, as in GDR'),
    # ('wind_speed_alt_adjusted_abdalla_2012',
    #  'adjusted wind speed from Abdalla 2012'),
    # ('wind_speed_alt_adjusted_gourrion_2002',
    #  'adjusted wind speed from Gourrion 2002'),
    # ('wind_speed_rad', 'wind speed from radiometer, as in GDR'),
    # ('wind_speed_alt_quality', 'quality of Ku band altimeter wind speed'),
    # ('wind_speed_rad_quality', 'quality of radiometer wind speed'),
    ('total_column_liquid_water_content_rad', 'radiometer liquid water content'),
    ('sea_ice_fraction', 'sea ice fraction')
])


class L2P(l2p.L2P):

    def __init__(self, configuration):
        super(L2P, self).__init__(configuration)
        self.gdr_01hz = None

    def processor(self):
        return "CCI Sea State L2P processor v2.0"

    def swh_20hz(self, retracked_track):
        raise NotImplementedError

    def corrections_20hz(self, track):
        lut = xr.open_dataset(resource_filename(
                'cciseastate',
                ('resources/v2/WHALES_Hs_correction_v20210429.nc')
            ), decode_cf=False)
        CORRECTION_VAR = {
            'Jason-1': 'corr_J1',
            'Jason-2': 'corr_J2',
            'Jason-3': 'corr_J3',
            'Envisat': 'corr_En',
            'SARAL': 'corr_Al',
            'CryoSat-2': 'corr_CS',
        }

        mission = self.satellite

        hs0 = lut.hs0.squeeze() - 0.05
        corr = lut[CORRECTION_VAR[mission]].squeeze()

        lut_indices = np.digitize(self.swh_20hz(track).filled(-1), hs0) - 1

        # for SWH above LUT limits, apply the last correction in LUT range
        # first correction for SWH below 0.
        lut_indices[lut_indices >= lut.dims['col']] = lut.dims['col'] - 1
        lut_indices[lut_indices < 0] = 0

        return corr[lut_indices]

    def sigma0_20hz(self, track):
        raise NotImplementedError

    @classmethod
    def quality_20hz(cls, track, *args, **kwargs):
        """quality of 20 Hz data (0 = good, 1 = bad)"""
        raise NotImplementedError

    def init_gdr_01_hz(self, track):
        if self.gdr_01hz is None:
            self.gdr_01hz = self.gdr_track_01hz(
                Path(track.url),
                Path(self.configuration['gdr_root']))

    @classmethod
    def l2p_vars(cls):
        return VARIABLES

    @classmethod
    def retracked_1hz_feature(cls, fname):
        """return retracked 1 Hz feature as a cerbere trajectory"""
        return cerbere.open_as_feature('Trajectory', fname, 'NCDataset')

    def gdr_track_aux(self, retracked_fname, aux_path, suffix, check=True):
        aux_fname = (
            aux_path
            / Path(self.satellite.lower().replace(' ', '_'))
            / Path(retracked_fname.parts[-3])
            / Path(retracked_fname.parts[-2])
            / retracked_fname.name.replace('.nc', '_{}.nc'.format(suffix))
        )
        try:
            return cerbere.open_as_feature(
                'Trajectory', aux_fname, 'NCDataset',
            )
        except:
            if not check:
                raise

            # may be an archiving issue : try previous folder
            date = datetime.datetime.strptime(
                '{}{}'.format(retracked_fname.parts[-3],
                              retracked_fname.parts[-2]),
                '%Y%j'
            ) + datetime.timedelta(days=1)
            return self.gdr_track_aux(
                retracked_fname.parents[2].joinpath(
                    date.strftime('%Y/%j'), retracked_fname.name),
                aux_path, suffix, check=False)

    def get_native_name(self, fieldname):
        """
        Return the source 1 hz equivalent field name
        """
        translation = {
            'swh': 'swh_corrected',
            'swh_rms': 'swh_corrected_rms',
            'swh_num_valid': 'swh_corrected_numval',
            'sigma0': 'sigma0',
            'sigma0_rms': 'sigma0_rms',
            'sigma0_num_valid': 'sigma0_numval',
        }
        if fieldname in translation:
            return translation[fieldname]

    def read_rms_threshold_table(self, rms_table_name):
        """read RMS threshold table"""
        table = resource_filename(
            'cciseastate',
            ('resources/v2/%s' % rms_table_name)
        )
        logging.info("RMS LUT table: {}".format(table))

        with open(table) as rms_file:
            # skip first three header lines
            rms_file.readline()
            rms_file.readline()
            rms_file.readline()

            rms_table = np.array(
                list(map(float, rms_file.read().split()))
            ).reshape((596, 2))
            rms_threshold = rms_table[:, 1].flatten()

        assert(rms_threshold.size == 596)

        rmst_step = 0.05
        rmst_min = rms_table[0, 0] - rmst_step / 2.
        rmst_max = rms_table[-1, 0] + rmst_step / 2.

        return np.ma.fix_invalid(rms_threshold), rmst_step, rmst_min, rmst_max

    def test_swh_rms_threshold(
            self,
            track,
            **kwargs,
    ):
        """
        Test maximum swh_rms threshold.

        Args:
            track (Trajectory): GDR altimeter track
            rms_table_def (tuple): definition of the used SWH RMS table as
                (min SWH limit, max usable SWH limit, bin step)
            rms_table_range (tuple): range of SWH on which the table threshold
                are applied
        """
        swh_ku = track.get_values(self.get_native_name('swh'))
        swh_rms_ku = track.get_values(self.get_native_name('swh_rms'))

        quality = self.l2p.get_values('swh_quality')
        rejection = self.l2p.get_field('swh_rejection_flags')

        # remaining valid values
        valid = (~np.ma.getmaskarray(swh_rms_ku) & ~np.ma.getmaskarray(swh_ku))

        # read threshold table
        rms_threshold, rmst_step, rmst_min, rmst_max = \
            self.read_rms_threshold_table(self._get_rms_table_name())

        # check there are no masked values in the used range
        # (rms_table_range)
        if rms_threshold.size != rms_threshold.count():
            raise Exception(
                "There are undefined values in the RMS threshold table."
            )

        # filter Hs wrt LUT table's thresholds every <rmst_step> meter
        # range from <rmst_min> meters to <rmst_max> meters
        rmst_indices = np.floor(
            swh_ku.filled(0) / rmst_step - rmst_min / rmst_step
        ).astype(int)

        rmst_indices[rmst_indices < 0] = 0
        rmst_indices[rmst_indices >= len(rms_threshold)] = -1
        thresholds = rms_threshold[rmst_indices]

        bad = (valid & (swh_rms_ku > thresholds))
        logging.info(
            'Nb of values rejected LUT SWH RMS test: {}'.format(bad.sum()))
        quality[bad] = l2p.QUALITY_BAD

        rejection.attrs['rms_threshold_lut'] = self._get_rms_table_name()
        self.set_mask_bit(rejection, bad, 'swh_rms_outlier')

    @classmethod
    def swh_numval_limit(cls):
        return 6

    def set_swh_quality(self, track):
        """
        set the quality information on SWH
        """
        swh_numval = track.get_values('swh_corrected_numval')
        swh = track.get_values('swh_corrected')
        swh_rms = track.get_values('swh_corrected_rms')
        #sigma0 = track.get_values('sigma0')

        # init quality fields
        quality = self.l2p.get_values('swh_quality')
        quality[:] = track.get_values('swh_corrected_quality')
        waveform_invalid = (quality == l2p.QUALITY_BAD)

        rejection = self.l2p.get_field('swh_rejection_flags')
        rejection.set_values(track.get_values(
            'swh_corrected_rejection').astype(np.uint16))

        aux_path = self.configuration['aux_path']
        land = self.gdr_track_landmask(track.url, aux_path).get_values(
            'land_mask'
        )

        editing = (
                (swh.filled(0) <= 0) |
                (swh.filled(0) > 30.) |
                (swh_rms.filled(0) <= 0) |
                (swh_numval < self.swh_numval_limit()) |
                (land != 0) |
                waveform_invalid
        ).filled(True)
        quality[editing] = l2p.QUALITY_BAD

        # undefined SWH values
        quality[
            (np.ma.getmaskarray(swh) | np.ma.getmaskarray(swh_rms))
        ] = l2p.QUALITY_UNDEFINED

        # set rejection flag
        self.set_mask_bit(rejection,
                          land != 0,
                          'not_water')
        self.set_mask_bit(rejection,
                          (swh.filled(0) <= 0) | (swh.filled(0) > 30.),
                          'swh_validity')
        self.set_mask_bit(
            rejection,
            ((swh_numval < self.swh_numval_limit())
             | waveform_invalid),
            'waveform_validity'
        )
        # self.set_mask_bit(rejection,
        #                   (sigma0.filled(0) <= 0),
        #                   'sigma0_validity')

        self.set_mask_bit(rejection,
                          swh_rms.filled(0) <= 0,
                          'swh_rms_outlier')

    def set_adjusted_swh(self):
        aswh = self.l2p.get_field('swh_adjusted')
        aswh.set_values(self.adjusted_swh(self.l2p))
        aswh.attrs['adjustment_lut'] = self._calibration_table_name
        aswh.attrs['adjustment_reference'] = \
            "CCI Sea State v2 Product Specification Document, 2021"

    def read_calibration_table(self, table_name):
        """read calibration LUT table"""
        table = resource_filename(
            'cciseastate',
            ('resources/v2/%s' % table_name)
        )
        logging.info("Calibration LUT table: {}".format(table))

        with open(table) as cal_file:
            # skip first three header lines
            cal_file.readline()
            cal_file.readline()
            cal_file.readline()

            cal_table = np.array(
                list(map(float, cal_file.read().split()))
            ).reshape((499, 2))
            calibration = cal_table[:, 1].flatten()

        assert(calibration.size == 499)

        cal_step = 0.05
        cal_min = cal_table[0, 0] - cal_step / 2.
        cal_max = cal_table[-1, 0] + cal_step / 2.

        return np.ma.fix_invalid(calibration), cal_step, cal_min, cal_max

    def adjusted_swh(self, track):

        # read threshold table
        table_name = self._calibration_table_name
        if table_name is None:
            logging.warning("No calibration table applied")
            return track.get_values('swh')

        calibration, cal_step, cal_min, cal_max = \
            self.read_calibration_table(table_name)

        # check there are no masked values in the used range
        # (rms_table_range)
        if calibration.size != calibration.count():
            raise Exception(
                "There are undefined values in the RMS threshold table."
            )

        # Correct Hs wrt LUT table's calibrations every <cal_step> meter
        # range from <cal_min> meters to <cal_max> meters
        cal_indices = np.floor(
            track.get_values('swh').filled(0) / cal_step - cal_min / cal_step
        ).astype(int)

        cal_indices[cal_indices < 0] = 0
        cal_indices[cal_indices >= len(calibration)] = -1

        return track.get_values('swh') + calibration[cal_indices]

    def set_wind_speed_alt_quality(self, track):
        return

    def set_wind_speed_rad_quality(self, track):
        return

    def correct_sigma0_for_wspd(self, track):
        return

    def wind_speed_abdalla(self):
        return

    def wind_speed_gourrion(self, high_winds='young1993'):
        return

    def _get_rms_table_def(self):
        return (0., 30., 0.1)

    def _get_rms_classes(self):
        return (0., 30., 30.)

    def _get_midrange_rms_poly(self):
        return np.poly1d([1])

    def set_adjusted_sigma0(self, track):
        return

    def swh_uncertainty_factors(cls):
        raise NotImplementedError

    def swh_uncertainty_attributes(cls):
        formula = '1.96 * {} * SWH + {}'.format(*cls.swh_uncertainty_factors())
        reference = "CCI Sea State v2 Product Specification Document, 2021"
        return formula, reference

    def swh_uncertainty(self,):
        a1, a0 = self.swh_uncertainty_factors()
        return 1.96 * self.l2p.get_values('swh_adjusted') * a1 + a0

    def copy_source_values(self, intrack):
        super(L2P, self).copy_source_values(intrack)

        # fix time
        self.l2p._std_dataset['time'].values[:] = self.gdr_01hz.get_times()
        self.l2p._std_dataset['time'].encoding = self.gdr_01hz.get_coord(
            'time').encoding
