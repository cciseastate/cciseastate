import collections
import logging

import numpy


def track_matching(srcfeature, destfeature, precision=4):
    """Return the matching between the measurements of two comparable
    tracks"""

    # build dictionary of locations mapping
    dest_lats = numpy.round(destfeature.get_lat(), decimals=precision)
    dest_lons = numpy.round(destfeature.get_lon(), decimals=precision)
    mapping = collections.OrderedDict(
        [((dest_lons[i], dest_lats[i]), i) for i in range(len(dest_lats))
         if dest_lats[i] is not numpy.ma.masked])

    src_lats = numpy.round(srcfeature.get_lat(), decimals=precision)
    src_lons = numpy.round(srcfeature.get_lon(), decimals=precision)

    src_indices = []
    dest_indices = []
    for i, _ in enumerate(src_lats):
        if src_lats[i] is numpy.ma.masked or src_lons[i] is numpy.ma.masked:
            continue

        if (src_lons[i], src_lats[i]) in mapping:
            src_indices.append(i)
            dest_indices.append(mapping[(src_lons[i], src_lats[i])])
        else:
            # check if close measurement
            logging.warning('no exact match found, looking at neighbour')
            for k, _ in enumerate(dest_lats):
                if ((abs(src_lons[i] - dest_lons[k]) <= 0.001)
                        and (abs(src_lats[i] - dest_lats[k]) <= 0.001)):
                    src_indices.append(i)
                    dest_indices.append(k)
                    break

    if (len(dest_indices) > 1 and
            (numpy.diff(dest_indices).min() <= 0 or numpy.diff(
                src_indices).min() <= 0)):
        logging.error("Indices are not monotonic")
        # raise Exception('Indices are not monotonic')

    return src_indices, dest_indices,
