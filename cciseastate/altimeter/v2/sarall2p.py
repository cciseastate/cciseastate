"""
Specific Saral/AltiKa processing for the processing of a V2 retracked altimeter
product to CCI format and content.

:copyright: Copyright 2020 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import logging
from pathlib import Path

import numpy as np

import cerbere
from ceremd import denoise

import cciseastate.altimeter.v2.whalesl2p as whalesl2p


class SaralL2P(whalesl2p.WHALESL2P):

    def satellite(self):
        return 'SARAL'

    def instrument(self):
        return 'AltiKa'

    def band(self):
        return 'Ka'

    def resolution(self):
        return "7 km"

    def cycle(self, track):
        self.init_gdr_01_hz(track)
        return self.gdr_01hz.attrs['cycle_number']

    def relative_pass(self, track):
        self.init_gdr_01_hz(track)
        return self.gdr_01hz.attrs['pass_number']

    def equator_crossing_time(self, track):
        self.init_gdr_01_hz(track)
        return self.gdr_01hz.attrs['equator_time']

    def equator_crossing_longitude(self, track):
        self.init_gdr_01_hz(track)
        lon = self.gdr_01hz.attrs['equator_longitude']
        if lon > 180:
            lon -= 360.
        return lon

    def retracked_20hz_feature(cls, fname):
        """return retracked 20 Hz feature as a cerbere trajectory"""
        return cerbere.open_as_feature(
            'Trajectory', Path(fname), 'WHALES2DNCDataset')

    def quality_20hz(self, retracked_track, gdr_track_hf, **kwargs):
        """quality of 20 Hz data (0 = good, 1 = bad)"""
        rt_swh_quality = super(SaralL2P, self).quality_20hz(retracked_track)

        # filter out bad off nadir angle (off_nadir_angle_wf <= 0.09 deg) as
        # it is going wrong from 3-Feb-2019
        offnadir = gdr_track_hf.get_values('off_nadir_angle_wf_40hz').flatten()
        rt_swh_quality[offnadir > 0.019] = 1

        return rt_swh_quality

    @classmethod
    def matching_1hz(cls, track, retracked_track):
        return track.dims['time'], np.repeat(
            np.arange(track.dims['time']), 40),

    @classmethod
    def gdr_track_01hz(cls, retracked_fname, gdr_path):
        gdr_fname = (
            gdr_path
            / Path(retracked_fname.parts[-3])
            / Path(retracked_fname.parts[-2])
            / retracked_fname.name
        )
        return cerbere.open_as_feature(
            'Trajectory', gdr_fname, 'NCDataset',
            field_matching={
                'time_avg_01_ku': 'time',
                'lat_avg_01_ku': 'lat',
                'lon_avg_01_ku': 'lon'},
            dim_matching={'time_avg_01_ku': 'time'}
        )

    @classmethod
    def gdr_track_hf(cls, retracked_fname, gdr_path):
        gdr_fname = (
            gdr_path
            / Path(retracked_fname.parts[-3])
            / Path(retracked_fname.parts[-2])
            / retracked_fname.name
        )
        return cerbere.open_as_feature(
            'Trajectory', gdr_fname, 'NCDataset',
            field_matching={'time_20_ku': 'time',
                            'lat_20_ku': 'lat',
                            'lon_20_ku': 'lon'},
            dim_matching={'time_20_ku': 'time'}
        )

    def _get_rms_table_name(self):
        return 'cci_swh_rms_LUT_saral.dat'

    def _coeff_std(self):
        return 5.

    def swh_uncertainty_factors(cls):
        return 0.049, 0.078

    @property
    def _calibration_table_name(self):
        return 'tab_calibration_lut_saral.dat'

    def merge_auxiliaries(self, track):
        super(SaralL2P, self).merge_auxiliaries(track)

        # add GDR info
        try:
            dst = self.gdr_track_01hz(
                track.url, self.configuration['gdr_root'])
            self.l2p.get_field('wind_speed_alt').set_values(
                dst.get_values('wind_speed_alt')
            )
            self.l2p.get_field(
                'total_column_liquid_water_content_rad').set_values(
                    dst.get_values('rad_liquid_water')
            )

        except IOError:
            logging.error("GDR file could not be found.")

    def _denoise(self, swh):
        return denoise(
            self.l2p,
            fieldname='swh_adjusted',
            values=swh,
            check_times=True,
            time_threshold=2,
            N=3,
            T_mult=.65)
