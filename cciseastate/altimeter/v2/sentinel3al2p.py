"""
Specific Sentinel-3 processing for the processing of a V2 retracked altimeter
product to CCI format and content.

:copyright: Copyright 2020 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
from .sentinel3l2p import Sentinel3PLRML2P, Sentinel3SARL2P


class Sentinel3APLRML2P(Sentinel3PLRML2P):

    def satellite(self):
        return 'Sentinel-3 A'

    def _get_rms_table_name(self):
        return 'cci_swh_rms_LUT_sentinel-3_a_plrm.dat'

    def _coeff_std(self):
        return 5.

    def swh_uncertainty_factors(cls):
        return 0.045, 0.142

    @property
    def _calibration_table_name(self):
        return 'tab_calibration_lut_sentinel-3_a_plrm.dat'


class Sentinel3ASARL2P(Sentinel3SARL2P):
    def satellite(self):
        return 'Sentinel-3 A'

    def _get_rms_table_name(self):
        return 'cci_swh_rms_LUT_sentinel-3_a_sar.dat'

    def _coeff_std(self):
        return 5.

    def swh_uncertainty_factors(cls):
        return 0.049, 0.107

    @property
    def _calibration_table_name(self):
        return 'tab_calibration_lut_sentinel-3_a_sar.dat'