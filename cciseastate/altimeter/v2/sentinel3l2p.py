"""
Specific Sentinel-3 SRAL processing for the processing of a V2 retracked
altimeter product to CCI format and content.

:copyright: Copyright 2020 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import logging
from pathlib import Path

import numpy as np

import cerbere

import cciseastate.altimeter.v2.l2p as l2p


class Sentinel3L2P(l2p.L2P):

    def satellite(self):
        raise NotImplementedError

    def instrument(self):
        return 'SRAL'

    def band(self):
        return 'Ku'

    def resolution(self):
        return "7 km"

    def cycle(self, track):
        # S3A_SGDR_C0038_P0691_20181204_021246_20181204_030316__PEACHI_V2-1.nc
        return int(track.url.name.split('_')[2].strip("C"))

    def relative_pass(self, track):
        return int(track.url.name.split('_')[3].strip("P"))

    def equator_crossing_time(self, track):
        return

    def equator_crossing_longitude(self, track):
        return

    def retracked_20hz_feature(cls, fname):
        """return retracked 20 Hz feature as a cerbere trajectory"""
        return cerbere.open_as_feature(
            'Trajectory', Path(fname), 'NCDataset',
            field_matching={
                'time_echo_sar_ku': 'time',
                'lat_echo_sar_ku': 'lat',
                'lon_echo_sar_ku': 'lon'},
        )

    @classmethod
    def quality_20hz(cls, retracked_track, *args, **kwargs):
        """quality of 20 Hz data (0 = good, 1 = bad)"""
        # no quality editing even on MQE (email P.Thibault, 13-04-2021)
        return np.ma.zeros((retracked_track.dims['time'],), dtype=np.int8)

    def corrections_20hz(self, track):
        """no corrections for Sentinel 3 / SRAL"""
        return np.ma.zeros((track.dims['time'],))

    @classmethod
    def matching_1hz(cls, track, retracked_track):
        times = retracked_track.get_times()

        # get time delta from first measurements and group by second
        dt = ((times - times[0]) / np.timedelta64(
            1000000000, 'ns') / 1.0).astype(np.int32)

        ind_1hz, ind_20hz = (np.unique(dt, return_inverse=True))

        return len(ind_1hz), ind_20hz

    def copy_source_values(self, intrack):
        super(l2p.L2P, self).copy_source_values(intrack)

    @classmethod
    def gdr_track_01hz(cls, retracked_fname, gdr_path):
        return

    @classmethod
    def gdr_track_hf(cls, retracked_fname, gdr_path):
        return

    def _get_rms_table_name(self):
        return 'cci_v2_swh_rms_LUT_saral.dat'

    def _coeff_std(self):
        return 5.

    def swh_uncertainty_factors(cls):
        return 0.056, 0.057

    def swh_adjustment_factors(cls):
        return 0.963, -0.105


class Sentinel3PLRML2P(Sentinel3L2P):

    def swh_20hz(self, retracked_track):
        values = np.ma.fix_invalid(retracked_track.get_values('swh_plrm_20_ku'))
        # mask values around 0.181 (constant)
        return np.ma.masked_inside(values, 0.181, 0.181001)

    def sigma0_20hz(self, retracked_track):
        return np.ma.fix_invalid(
            retracked_track.get_values('sigma0_plrm_20_ku') +
            retracked_track.get_values('atmosph_sigma0_corr'))


class Sentinel3SARL2P(Sentinel3L2P):

    def swh_20hz(self, retracked_track):
        values = np.ma.fix_invalid(
            retracked_track.get_values('swh_lrrmc_corr_hfa_20_ku')
            - retracked_track.get_values('corr_hfa_lrrmc_ku_20_ku')
        )
        # mask values around 0.1 (constant)
        return np.ma.masked_equal(values, 0.1)

    def sigma0_20hz(self, retracked_track):
        return np.ma.fix_invalid(
            retracked_track.get_values('sigma0_lrrmc_20_ku') +
            retracked_track.get_values('atmosph_sigma0_corr'))
