"""
Base processor for the conversion of a WHALES altimeter product to CCI format
and content, v2.

:copyright: Copyright 2020 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import numpy as np

from . import l2p


class WHALESL2P(l2p.L2P):

    def swh_20hz(self, retracked_track):
        return np.ma.fix_invalid(
            retracked_track.get_values('swh_WHALES_20hz'))

    def quality_20hz(self, retracked_track, *args, **kwargs):
        """quality of 20 Hz data (0 = good, 1 = bad)"""
        if 'swh_WHALES_qual_20hz' in retracked_track.fieldnames:
            # CCI WHALES flavour
            return np.ma.fix_invalid(
                retracked_track.get_values('swh_WHALES_qual_20hz'))
        elif 'swh_WHALES_fitting_error_20hz' in retracked_track.fieldnames:
            # files processed at TUM don't provide the quality flag
            # it can be computed with fitting error, applying recommended
            # threshold 0.3
            rt_swh_quality = np.ma.ones((retracked_track.dims['time'],))

            # good if fitting error < threshold
            rt_swh_quality[np.ma.fix_invalid(
                retracked_track.get_values(
                    'swh_WHALES_fitting_error_20hz')) < 0.3] = 0
            return rt_swh_quality
        else:
            raise ValueError('No valid quality variable')

    def sigma0_20hz(self, retracked_track):
        if 'sigma0_WHALES_20hz' in retracked_track.fieldnames:
            return np.ma.fix_invalid(
                retracked_track.get_values('sigma0_WHALES_20hz'))
        else:
            return np.ma.masked_all((retracked_track.dims['time'],))
