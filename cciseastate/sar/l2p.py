"""
Base processor for the conversion of a SAR product to CCI format and
content.

:copyright: Copyright 2020 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
from abc import abstractmethod
import os
import logging
from collections import OrderedDict
import uuid
import datetime
from pathlib import Path

import numpy
from netCDF4 import getlibversion

import cerbere


VERSION = "1.0"

# Quality levels
QUALITY_GOOD = 3
QUALITY_ACCEPTABLE = 2
QUALITY_BAD = 1
QUALITY_UNDEFINED = 0


# variables in L2P
VARIABLES = OrderedDict([
    ('sigma0', 'Ku band backscatter coefficient'),
    ('sigma0_adjusted', 'Ku band adjusted backscatter coefficient'),
    ('sigma0_rms', 'RMS of the Ku band backscatter coefficient'),
    ('sigma0_num_valid',
     'number of valid points used to compute Ku band backscatter coefficient'),
#    ('mss', 'mean square slope from Ku band backscatter'),
    ('swh', 'Ku band significant wave height'),
    ('swh_adjusted', 'Ku band adjusted significant wave height'),
    ('swh_quality', 'quality of Ku band significant wave height measurement'),
    ('swh_uncertainty', 'best estimate of significant wave height standard error'),
    ('swh_rms', 'RMS of the Ku band significant wave height (from 20 Hz measurements)'),
    ('swh_num_valid', 'number of 20 Hz valid points used to compute Ku band significant wave height'),
    ('swh_rejection_flags', 'consolidated instrument and ice flags'),
#    ('off_nadir_angle_wf', 'square of the off nadir angle computed from waveforms'),
    ('range', 'Ku band range'),
    ('range_rms', 'RMS of the Ku band range'),
    ('wind_speed_alt', 'wind speed from Ku band altimeter, as in GDR'),
    # ('wind_speed_alt_adjusted_abdalla_2012',
    #  'adjusted wind speed from Abdalla 2012'),
    # ('wind_speed_alt_adjusted_gourrion_2002',
    #  'adjusted wind speed from Gourrion 2002'),
    # ('wind_speed_rad', 'wind speed from radiometer, as in GDR'),
    # ('wind_speed_alt_quality', 'quality of Ku band altimeter wind speed'),
    # ('wind_speed_rad_quality', 'quality of radiometer wind speed'),
    ('total_column_liquid_water_content_rad', 'radiometer liquid water content'),
    ('sea_ice_fraction', 'sea ice fraction')
])

# variables to be copied from GDR to L2P
SOURCE_FIELDS = ['sigma0', 'sigma0_rms', 'sigma0_num_valid',
                 'swh', 'swh_rms', 'swh_num_valid', 'off_nadir_angle_wf',
                 'range', 'range_rms', 'wind_speed_alt', 'wind_speed_rad',
                 'total_column_liquid_water_content_rad']

AUTHORITY = 'CF 1.7'
STANDARDNAME = {
    'mss': 'sea_surface_wave_mean_square_slope',
    'swh': 'sea_surface_wave_significant_height',
    'swh_adjusted': 'sea_surface_wave_significant_height',
    'swh_filtered': 'sea_surface_wave_significant_height',
    'sigma0': 'surface_backwards_scattering_coefficient_of_radar_wave',
    'sigma0_adjusted': 'surface_backwards_scattering_coefficient_of_radar_wave',
    'wind_speed_alt': 'wind_speed',
    'wind_speed_alt_adjusted_abdalla_2012': 'wind_speed',
    'wind_speed_alt_adjusted_gourrion_2002': 'wind_speed',
    'wind_speed_rad': 'wind_speed',
    'total_column_liquid_water_content_rad': 'atmosphere_cloud_liquid_water_content',
    'sea_ice_fraction': 'sea_ice_fraction'
}

# fields related to the selected radar band (Ku or Ka) measurements
BAND = ['swh', 'swh_adjusted', 'swh_rms', 'swh_quality',
        'swh_num_valid', 'swh_rejection_flags',
        'sigma0', 'sigma0_adjusted', 'sigma0_rms', 'sigma0_num_valid',
        'mss', 'wind_speed_alt', 'wind_speed_alt_quality',
        'wind_speed_alt_adjusted_abdalla_2012',
        'wind_speed_alt_adjusted_gourrion_2002'
        ]

CONTENTTYPE = {
    'swh': 'physicalMeasurement',
    'swh_adjusted': 'physicalMeasurement',
    'swh_quality': 'qualityInformation',
    'swh_uncertainty': 'qualityInformation',
    'swh_rms': 'auxiliaryInformation',
    'swh_num_valid': 'auxiliaryInformation',
    'swh_rejection_flags': 'qualityInformation',
    'sigma0': 'physicalMeasurement',
    'sigma0_adjusted': 'physicalMeasurement',
    'sigma0_rms': 'auxiliaryInformation',
    'sigma0_num_valid': 'auxiliaryInformation',
    'mss': 'physicalMeasurement',
    'off_nadir_angle_wf': 'auxiliaryInformation',
    'range': 'auxiliaryInformation',
    'range_rms': 'auxiliaryInformation',
    'wind_speed_alt': 'physicalMeasurement',
    'wind_speed_alt_adjusted_abdalla_2012': 'physicalMeasurement',
    'wind_speed_alt_adjusted_gourrion_2002': 'physicalMeasurement',
    'wind_speed_rad': 'physicalMeasurement',
    'wind_speed_alt_quality': 'qualityInformation',
    'wind_speed_rad_quality': 'qualityInformation',
    'total_column_liquid_water_content_rad': 'physicalMeasurement',
    'sea_ice_fraction': 'auxiliaryInformation',
}

FLAG_VALUES = {
    'swh_quality': [0, 1, 2, 3]
}
FLAG_MASKS = {
    'swh_rejection_flags': [1, 2, 4, 8, 16, 32, 64, 128]
}

NO_MISSING_VALUE = [
    'swh_quality',
    'swh_rejection_flags',
    'swh_num_valid',
    'sigma0_num_valid',
    'wind_speed_alt_quality',
]

FLAG_MEANINGS = {
    'swh_quality': ' '.join(['undefined', 'bad', 'acceptable', 'good']),
    'swh_rejection_flags': ' '.join([
        'not_water',
        'sea_ice',
        'swh_validity',
        'sigma0_validity',
        'waveform_validity',
        'ssh_validity',
        'swh_rms_outlier',
        'swh_outlier',
    ])
}

ANCILLARY = {
    'swh': 'swh_quality swh_rejection_flags',
    'swh_adjusted': 'swh_quality swh_rejection_flags',
    'sigma0': 'sigma0_quality',
    'sigma0_adjusted': 'sigma0_quality',
    'wind_speed_alt': 'wind_speed_alt_quality',
    'wind_speed_rad': 'wind_speed_rad_quality',
    'wind_speed_alt_adjusted_abdalla_2012': 'wind_speed_alt_quality',
    'wind_speed_alt_adjusted_gourrion_2002': 'wind_speed_alt_quality',
}

COMMENT = {
    'swh': 'All instrumental corrections included. As available from retracker and unedited.',
    'swh_adjusted': 'All instrumental corrections included. Adjusted and unedited',
    'swh_uncertainty': 'Standard error calculated from buoy colocations',
    'sigma0': 'All instrumental corrections included. As available from retracker and unedited..',
    'sigma0_adjusted': 'All instrumental corrections included. Adjusted and unedited.',
}

UNITS = {
    'sigma0': 'dB',
    'sigma0_adjusted': 'dB',
    'sigma0_rms': 'dB',
    'sigma0_num_valid': '1',
    'mss': 'm2',
    'swh': 'm',
    'swh_adjusted': 'm',
    'swh_filtered': 'm',
    'swh_uncertainty': 'm',
    'swh_rms': 'm',
    'swh_num_valid': '1',
    'off_nadir_angle_wf': 'degree2',
    'wind_speed_alt': 'm s-1',
    'wind_speed_alt_adjusted_abdalla_2012': 'm s-1',
    'wind_speed_alt_adjusted_gourrion_2002': 'm s-1',
    'wind_speed_rad': 'm s-1',
    'total_column_liquid_water_content_rad': 'kg m-2',
    'sea_ice_fraction': '1',
}

DTYPE = {
    'sigma0': numpy.float64,
    'sigma0_adjusted': numpy.float64,
    'sigma0_rms': numpy.float64,
    'sigma0_num_valid': numpy.uint8,
    'mss': numpy.float64,
    'swh': numpy.float64,
    'swh_adjusted': numpy.float64,
    'swh_filtered': numpy.float64,
    'swh_quality': numpy.uint8,
    'swh_uncertainty': numpy.float64,
    'swh_rms': numpy.float64,
    'swh_num_valid': numpy.uint8,
    'swh_rejection_flags': numpy.uint16,
    'off_nadir_angle_wf': numpy.float64,
    'range': numpy.float64,
    'range_rms': numpy.float64,
    'wind_speed_alt': numpy.float64,
    'wind_speed_alt_adjusted_abdalla_2012': numpy.float64,
    'wind_speed_alt_adjusted_gourrion_2002': numpy.float64,
    'wind_speed_rad': numpy.float64,
    'wind_speed_alt_quality': numpy.uint8,
    'wind_speed_rad_quality': numpy.uint8,
    'total_column_liquid_water_content_rad': numpy.float64,
    'sea_ice_fraction': numpy.float64,
}

PRECISION = {
    'sigma0': 3,
    'sigma0_adjusted': 3,
    'sigma0_rms': 3,
    'sigma0_num_valid': 0,
    'mss': 3,
    'swh': 3,
    'swh_adjusted': 3,
    'swh_filtered': 3,
    'swh_quality': 0,
    'swh_uncertainty': 3,
    'swh_rms': 3,
    'swh_num_valid': 0,
    'range': 3,
    'range_rms': 3,
    'swh_rejection_flags': 0,
    'off_nadir_angle_wf': 3,
    'wind_speed_alt': 3,
    'wind_speed_alt_adjusted_abdalla_2012': 3,
    'wind_speed_alt_adjusted_gourrion_2002': 3,
    'wind_speed_rad': 3,
    'wind_speed_alt_quality': 0,
    'wind_speed_rad_quality': 0,
    'total_column_liquid_water_content_rad': 3,
    'sea_ice_fraction': 2
}

RELATED_ATTR = {
    'BAND': 'band',
    'COMMENT': 'comment',
    'PRECISION': 'least_significant_digit',
    'ANCILLARY': 'ancillary_variables',
    'FLAG_VALUES': 'flag_values',
    'FLAG_MEANINGS': 'flag_meanings',
    'FLAG_MASKS': 'flag_masks',
    'CONTENTTYPE': 'coverage_content_type'
}

VARTTRIBUTES = OrderedDict([
    ('COMMENT', COMMENT),
    ('PRECISION', PRECISION),
    ('ANCILLARY', ANCILLARY),
    ('FLAG_VALUES', FLAG_VALUES),
    ('FLAG_MEANINGS', FLAG_MEANINGS),
    ('FLAG_MASKS', FLAG_MASKS),
    ('CONTENTTYPE', CONTENTTYPE)
])

GLOBAL_ATTRIBUTES = {
    'Conventions': 'CF-1.7, ACDD-1.3, ISO 8601',
    'title': 'ESA CCI Sea State L2P derived from %s GDR',
    'id': 'ESACCI-SEASTATE-L2P-SWH-%s',
    'institution': "Institut Francais de Recherche pour l'Exploitation de la mer / CERSAT, European Space Agency",
    'institution_abbreviation': 'Ifremer/Cersat, ESA',
    'source': 'CCI Sea State %s GDR to L2P Processor',
    'history': '%s - Creation',
    'references': 'CCI Sea State Product Specification Document (PSD), v1.1',
    'product_version': '1.0',
    'summary': 'This dataset contains along-track significant wave height measurements from %s altimeter, cross-calibrated with other altimetry missions and reference in situ measurements.',
    'keywords': ['Oceans > Ocean Waves > Significant Wave Height',
                 'Oceans > Ocean Waves > Sea State'
                 ],
    'keywords_vocabulary': 'NASA Global Change Master Directory (GCMD) Science Keywords',
    'naming_authority': 'fr.ifremer.cersat',
    'cdm_data_type': 'trajectory',
    'featureType': 'trajectory',
    'comment': 'These data were produced at ESACCI as part of the ESA SST CCI project.',
    'creator_name': 'Cersat',
    'creator_url': 'http://cersat.ifremer.fr',
    'creator_email': 'cersat@ifremer.fr',
    'creator_institution': 'Ifremer / Cersat',
    'project': 'Climate Change Initiative - European Space Agency',
    'geospatial_lat_min': -80.,
    'geospatial_lat_max': 80.,
    'geospatial_lat_units': 'degree_north',
    'geospatial_lon_min': -180.,
    'geospatial_lon_max': 180.,
    'geospatial_lon_units': 'degree_east',
    'standard_name_vocabulary': 'NetCDF Climate and Forecast (CF) Metadata Convention version 1.7',
    'license': 'ESA CCI Data Policy: free and open access',
    'platform': '',
    'platform_type': 'low earth orbit satellite',
    'platform_vocabulary': 'CCI',
    'instrument': '',
    'instrument_type': 'altimeter',
    'instrument_vocabulary': 'CCI',
    'spatial_resolution': '',
    'cycle_number': '',
    'relative_pass_number': '',
    'equator_crossing_time': '',
    'equator_crossing_longitude': '',
    'netcdf_version_id': getlibversion(),
    'acknowledgement': 'Please acknowledge the use of these data with the following statement: these data were obtained from the ESA CCI Sea State project',
    'format_version': 'Data Standards v2.1',
    #'processing_software': '%s GDR to L2P Processor %s',
    'processing_level': 'L2P',
    'track_id': '',
    'publisher_name': 'Cersat',
    'publisher_url': 'cersat.ifremer.fr',
    'publisher_email': 'cersat@ifremer.fr',
    'publisher_institution': 'Ifremer / Cersat',
    'scientific_support_contact': 'Guillaume.Dodet@ifremer.fr',
    'technical_support_contact': 'cersat@ifremer.fr',
    'key_variables': 'swh_denoised'
}


# size in km of the sliding window SWH outlier filter
WINDOW_IN_KM = 100000.

# required minimum of valid measurements in above window
MIN_NB_OF_VALID_POINTS = 4


class L2P(object):

    def __init__(self, configuration):
        self.satellite = self.satellite()
        self.band = self.band()
        self.instrument = self.instrument()
        self.configuration = configuration

    def init_l2p(self, intrack):
        self.l2p = intrack

        # update dynamic attributes
        if 'cycle_number' not in self.l2p.attrs:
            self.l2p.attrs['cycle_number'] = self.cycle(intrack)
        if 'relative_pass_number' not in self.l2p.attrs:
            self.l2p.attrs['relative_pass_number'] = self.relative_pass(intrack)

        eqtime = self.equator_crossing_time(intrack)
        if eqtime is not None and 'equator_crossing_time' not in self.l2p.attrs:
            self.l2p.attrs['equator_crossing_time'] = eqtime
        eqlon = self.l2p.attrs['equator_crossing_longitude']
        if eqlon is not None and 'equator_crossing_longitude' not in \
                self.l2p.attrs:
            self.l2p.attrs['equator_crossing_longitude'] = eqlon

        # global attributes
        creation_time = datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S')
        if 'date_created' not in self.l2p.attrs:
            self.l2p.attrs['date_created'] = creation_time
        if 'date_modified' not in self.l2p.attrs:
            self.l2p.attrs['date_modified'] = creation_time
        if 'instrument' not in self.l2p.attrs:
            self.l2p.attrs['instrument'] = self.instrument
        if 'platform' not in self.l2p.attrs:
            self.l2p.attrs['platform'] = self.satellite
        self.l2p.attrs['track_id'] = str(uuid.uuid4())
        if 'band' not in self.l2p.attrs:
            self.l2p.attrs['band'] = self.band
        if 'source' not in self.l2p.attrs:
            self.l2p.attrs['source'] = (
                self.l2p.attrs['source'] % self.satellite)
        if 'source_version' not in self.l2p.attrs:
            self.l2p.attrs['source_version'] = VERSION
        if 'history' not in self.l2p.attrs:
            self.l2p.attrs['history'] = (
                [self.l2p.attrs['history'] % datetime.datetime.now()])
        if 'summary' not in self.l2p.attrs:
            self.l2p.attrs['summary'] = (
                self.l2p.attrs['summary'] % self.satellite)
        if 'id' not in self.l2p.attrs:
            self.l2p.attrs['id'] = (
                self.l2p.attrs['id'] % self.satellite)
        if 'title' not in self.l2p.attrs:
            self.l2p.attrs['title'] = (
                self.l2p.attrs['title'] % self.satellite)
        if 'spatial_resolution' not in self.l2p.attrs:
            self.l2p.attrs['spatial_resolution'] = self.resolution()

    def gdr_track_sea_ice_concentration(self, fname, aux_path):
        return self.gdr_track_aux(fname, aux_path, 'seaice')

    def gdr_track_dist2coast(self, fname, aux_path):
        return self.gdr_track_aux(fname, aux_path, 'dist2coast')

    def gdr_track_landmask(self, fname, aux_path):
        return self.gdr_track_aux(fname, aux_path, 'landmask')

    def gdr_track_bathymetry(self, fname, aux_path):
        return self.gdr_track_aux(fname, aux_path, 'bathy')

    def gdr_track_era5(self, fname, aux_path):
        return self.gdr_track_aux(fname, aux_path, 'era5')

    def gdr_track_era5_swh(self, fname, aux_path):
        return self.gdr_track_aux(fname, aux_path, 'era5_swh')

    def gdr_track_sealevel(self, fname, aux_path):
        return self.gdr_track_aux(fname, aux_path, 'sealevel')

    @classmethod
    def input_feature(cls, fname):
        """return input GDR feature as a cerbere trajectory"""
        raise NotImplementedError

    @abstractmethod
    def satellite(self):
        raise NotImplementedError

    @abstractmethod
    def instrument(self):
        raise NotImplementedError

    @abstractmethod
    def band(self):
        raise NotImplementedError

    @abstractmethod
    def resolution(self):
        raise NotImplementedError

    @abstractmethod
    def cycle(self, track):
        raise NotImplementedError

    @abstractmethod
    def relative_pass(self, track):
        raise NotImplementedError

    @abstractmethod
    def equator_crossing_time(self, track):
        raise NotImplementedError

    @abstractmethod
    def equator_crossing_longitude(self, track):
        raise NotImplementedError

    @classmethod
    def get_cci_name(cls, satellite, algo, start, version):
        """
        return CCI a compliant file name

        example: ESACCI-SEASTATE-L2P-ISSP-Sentinel-1_A-20170130T145103-fv01.nc
        """
        return 'ESACCI-SEASTATE-L2P-ISSP-{}-{}-{}-{}.nc'.format(
            satellite.replace(' ', ''),
            algo,
            start.strftime('%Y%m%dT%H%M%S'),
            version
        )

    def gdr_track_aux(self, fname, aux_path, suffix):
        aux_fname = (
            aux_path
            / Path(self.satellite.lower().replace(' ', '_'))
            / Path(fname.parts[-3])
            / Path(fname.parts[-2])
            / fname.name.replace('.nc', '_{}.nc'.format(suffix))
        )
        return cerbere.open_as_feature(
            'Trajectory', aux_fname, 'NCDataset',
        )

    def merge_auxiliaries(self, fname):
        """add ancillary fields"""
        # sea ice
        try:
            dst = self.gdr_track_sea_ice_concentration(
                fname, self.configuration['aux_path'])
            self.l2p.add_field(dst.get_field('sea_ice_fraction').clone())
        except IOError:
           raise

        # bathymetry
        try:
            dst = self.gdr_track_bathymetry(
                fname, self.configuration['aux_path'])
            self.l2p.add_field(dst.get_field('bathymetry').clone())
        except IOError:
            raise IOError("bathymetry file could not be found.")

        # distance to coast
        try:
            dst = self.gdr_track_dist2coast(
                fname, self.configuration['aux_path'])
            self.l2p.add_field(dst.get_field('distance_to_coast').clone())

        except IOError:
            raise IOError("dist2coast file could not be found.")

        # era5
        try:
            dst = self.gdr_track_era5(
                fname, self.configuration['aux_path'])
            for name in ['wind_speed_model_u', 'wind_speed_model_v',
                         'surface_air_pressure', 'surface_air_temperature']:
                self.l2p.add_field(dst.get_field(name).clone())
                self.l2p.get_field(name).precision = 3
        except IOError:
            logging.error("ERA5 file could not be found.")

        # era5 SWH
        try:
            dst = self.gdr_track_era5_swh(
                fname, self.configuration['aux_path'])
            self.l2p.add_field(dst.get_field('swh_model').clone())
            self.l2p.get_field('swh_model').precision = 3
        except IOError:
            logging.error("ERA5 SWH file could not be found.")

    def process(self, fname, outrootdir, auxiliary=False):

        intrack = self.input_feature(fname)

        if intrack.get_lat().count() == 0:
            logging.warning(
                "The file was not processed because it has no valid data."
            )
            exit(0)

        # create output feature
        self.init_l2p(intrack)

        # output file name
        outdir = Path(outrootdir) / \
                 intrack.time_coverage_start.strftime('%Y/%j')
        outfname = outdir / \
            self.get_cci_name(self.satellite,
                              self.algorithm(),
                              intrack.time_coverage_start,
                              self.version(intrack.url))

        # merge with auxiliary fields
        if auxiliary:
            self.merge_auxiliaries(fname)

        # global attributes
        if 'processing_software' not in self.l2p.attrs:
            self.l2p.attrs['processing_software'] = self.processor()

        # float clipping
        #         for f in self.l2p.get_fieldnames():
        #             if f in PRECISION:
        #                 field = self.l2p.get_field(f)
        #                 scale = 10**PRECISION[f]
        #                 field.set_values(
        #                     numpy.round(field.get_values() * scale) / scale
        #                     )

        # open output file
        if not os.path.exists(outdir):
            os.makedirs(outdir)

        logging.info('Output L2P file : %s' % outfname)
        if os.path.exists(outfname):
            os.remove(outfname)

        print("output ", outfname)
        self.l2p.save(outfname)
