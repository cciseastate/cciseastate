"""
Specific ENVISAT SAR DLR processing for the processing of a V2 retracked
altimeter product to CCI format and content.

:copyright: Copyright 2020 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import cerbere

import cciseastate.sar.l2p as l2p


class EnvisatSARWMDLRL2P(l2p.L2P):

    def satellite(self):
        return "Envisat"

    def band(self):
        return 'C'

    def instrument(self):
        return 'ASAR'

    def instrument_mode(self):
        return 'wave mode'

    def algorithm(self):
        return "LI2020"

    def version(self, fname):
        return fname.stem.split('-')[-1]

    def equator_crossing_time(self, intrack):
        return

    def equator_crossing_longitude(self, intrack):
        return

    def init_l2p(self, intrack):
        super(EnvisatSARWMDLRL2P, self).init_l2p(intrack)

        # fix attributes
        self.l2p.attrs['id'] = "ESACCI-SEASTATE-L2P-ISSP-ENVISAT_SARWM-LI2020"
        self.l2p.attrs['platform'] = self.satellite
        self.l2p.attrs['instrument'] = "ASAR"
        self.l2p.attrs['platform_vocabulary'] = "CEOS"
        self.l2p.attrs['instrument_vocabulary'] = "CEOS"

        self.l2p.attrs['acquisition_mode'] = self.instrument_mode()

        self.l2p.attrs['key_variables'] = "swh, Tm2"
        self.l2p.attrs['keywords'] = [
            "Oceans > Ocean Waves > Sea State",
            "Oceans > Ocean Waves > Significant Wave Height",
            "Oceans > Ocean Waves > Wave Period",
        ]

        # rename fields
        self.l2p.rename_field(
            'sigma0_normalized_variance', 'normalized_variance')
        self.l2p.get_field('normalized_variance').attrs.pop('standard_name')

    def input_feature(cls, fname):
        """return input GDR feature as a cerbere trajectory"""
        return cerbere.open_as_feature('Trajectory', fname, 'NCDataset')
