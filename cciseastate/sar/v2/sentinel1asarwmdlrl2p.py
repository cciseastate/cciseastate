"""
Specific Sentinel-1A SAR DLR processing for the processing of a product to CCI
V2 format and content.

:copyright: Copyright 2020 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import cciseastate.sar.v2.sentinel1sarwmdlrl2p as l2p


class Sentinel1ASARWMDLRL2P(l2p.Sentinel1SARWMDLRL2P):

    def satellite(self):
        return "Sentinel-1 A"

    def init_l2p(self, intrack):
        super(Sentinel1ASARWMDLRL2P, self).init_l2p(intrack)

        # fix attributes
        self.l2p.attrs['id'] = self.l2p.attrs['id'].replace(
            'S1', "Sentinel-1_A")
