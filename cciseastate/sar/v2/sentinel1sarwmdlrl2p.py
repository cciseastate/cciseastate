"""
Specific Sentinel-1 SAR DLR processing for the processing of a V2 retracked
altimeter product to CCI format and content.

:copyright: Copyright 2020 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import cerbere

import cciseastate.sar.l2p as l2p


class Sentinel1SARWMDLRL2P(l2p.L2P):

    def satellite(self):
        raise NotImplementedError

    def band(self):
        return 'C'

    def instrument(self):
        return 'C-Band SAR'

    def instrument_mode(self):
        return 'wave mode'

    def algorithm(self):
        return "DLR2021.1"

    def version(self, fname):
        return fname.stem.split('-')[-1]

    def equator_crossing_time(self, intrack):
        return

    def equator_crossing_longitude(self, intrack):
        return

    def init_l2p(self, intrack):
        super(Sentinel1SARWMDLRL2P, self).init_l2p(intrack)

        # fix attributes
        self.l2p.attrs['id'] = self.l2p.attrs['id'].replace(' ', "_")
        self.l2p.attrs['platform'] = self.satellite
        self.l2p.attrs['platform_vocabulary'] = "CEOS"
        self.l2p.attrs['instrument_vocabulary'] = "CEOS"

        self.l2p.attrs['acquisition_mode'] = self.instrument_mode()

        self.l2p.attrs['key_variables'] = self.l2p.attrs[
            'key_variables']\
            .replace('SWH', 'swh')\
            .replace('SW1', 'swell_swh_primary')\
            .replace('SW2', 'swell_swh_secondary')\
            .replace('SWW', 'windwave_swh')\
            .replace('Tmw', 'windwave_period')
        self.l2p.attrs['title'] = self.l2p.attrs['title'].replace(
            'Sentinel-1', self.satellite)
        self.l2p.attrs['summary'] = self.l2p.attrs['summary'].replace(
            'Sentinel-1', self.satellite)
        self.l2p.attrs['keywords'] = [
            "Oceans > Ocean Waves > Sea State",
            "Oceans > Ocean Waves > Significant Wave Height",
            "Oceans > Ocean Waves > Wave Period",
            "Oceans > Ocean Waves > Wave Direction",
            "Oceans > Ocean Waves > Swells",
            "Oceans > Ocean Waves > Wind Waves"
        ]

    def input_feature(cls, fname):
        """return input GDR feature as a cerbere trajectory"""
        return cerbere.open_as_feature('Trajectory', fname, 'NCDataset')
