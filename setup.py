#-*- coding: utf-8 -*-
"""

A collection of processors for CCI Sea State project

:copyright: Copyright 2019 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
from setuptools import setup, find_packages


setup(
    zip_safe=False,
    name='cciseastate',
    version='0.1.0',
    author='Jeff Piolle <jfpiolle@ifremer.fr>',
    author_email='jfpiolle@ifremer.fr',
    url="http://cersat.ifremer.fr",
    packages=find_packages(),
    scripts=[
        'bin/l2p_v1',
        'bin/l2p_v2',
        'bin/l2p_sar',
        'bin/add_cy46r1',
        'bin/add_era5wave',
        'bin/add_era5',
        'bin/add_era5_050',
        'bin/add_ccisealevel',
        'bin/add_cmemssealevel',
        'bin/l2p2l4',
        'bin/l2pmerge_v1',
        'bin/l2pmerge_v2',
        'bin/l2p2l3',
        'bin/add_sentinel3_l2',
        'bin/average_to_1hz',
        'bin/add_corrections',
        'bin/add_denoising',
        'bin/add_seaice_concentration',
    ],
    license='LICENSE.txt',
    entry_points={
        'cciseastate.missions': [
            'ENVISAT = cciseastate.altimeter.v2.envisatl2p:EnvisatL2P',
            'CRYOSAT-2 = cciseastate.altimeter.v2.cryosat2l2p:CryoSat2L2P',
            'SARAL = cciseastate.altimeter.v2.sarall2p:SaralL2P',
            'JASON-1 = cciseastate.altimeter.v2.jason1l2p:Jason1L2P',
            'JASON-2 = cciseastate.altimeter.v2.jason2l2p:Jason2L2P',
            'JASON-3 = cciseastate.altimeter.v2.jason3l2p:Jason3L2P',
            'JASON-3F = cciseastate.altimeter.v2.jason3fl2p:Jason3FL2P',
            'SENTINEL-3A_PLRM = '
            'cciseastate.altimeter.v2.sentinel3al2p:Sentinel3APLRML2P',
            'SENTINEL-3A_SAR = '
            'cciseastate.altimeter.v2.sentinel3al2p:Sentinel3ASARL2P',
            'SENTINEL1A_DLR = '
            'cciseastate.sar.v2.sentinel1asarwmdlrl2p:Sentinel1ASARWMDLRL2P',
            'SENTINEL1B_DLR = '
            'cciseastate.sar.v2.sentinel1bsarwmdlrl2p:Sentinel1BSARWMDLRL2P',
            'ENVISAT_DLR = '
            'cciseastate.sar.v2.envisatsarwmdlrl2p:EnvisatSARWMDLRL2P',
        ]},
    package_data={
        'cciseastate': ['resources/v1/cryosat-2_swh_rms_threshold.txt',
                        'resources/v1/jason-1_swh_rms_threshold.txt',
                        'resources/v1/jason-2_swh_rms_threshold.txt',
                        'resources/v1/jason-3_swh_rms_threshold.txt',
                        'resources/v1/saral_swh_rms_threshold.txt',
                        'resources/v1/sentinel-3a_swh_rms_threshold.txt',
                        'resources/v1/topex_ku_sigma0_cal.txt',
                        'resources/v2/WHALES_Hs_correction.nc',
                        'resources/v2/WHALES_Hs_correction_v20210429.nc',
                        'resources/v2/cci_swh_rms_LUT_cryosat-2.dat',
                        'resources/v2/cci_swh_rms_LUT_envisat.dat',
                        'resources/v2/cci_swh_rms_LUT_jason-1.dat',
                        'resources/v2/cci_swh_rms_LUT_jason-2.dat',
                        'resources/v2/cci_swh_rms_LUT_jason-3.dat',
                        'resources/v2/cci_swh_rms_LUT_sentinel-3_a_plrm.dat',
                        'resources/v2/cci_swh_rms_LUT_sentinel-3_a_sar.dat',
                        'resources/v2/cci_swh_rms_LUT_saral.dat',
                        'resources/v2/tab_calibration_lut_cryosat-2.dat',
                        'resources/v2/tab_calibration_lut_envisat.dat',
                        'resources/v2/tab_calibration_lut_jason-1.dat',
                        'resources/v2/tab_calibration_lut_jason-2.dat',
                        'resources/v2/tab_calibration_lut_jason-3.dat',
                        'resources/v2/tab_calibration_lut_saral.dat',
                        'resources/v2/tab_calibration_lut_sentinel-3_a_sar.dat',
                        'resources/v2/tab_calibration_lut_sentinel-3_a_plrm.dat',
                        ]},
    description='A collection of processors for CCI Sea State project'
)
