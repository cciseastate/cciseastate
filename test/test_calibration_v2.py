from pkg_resources import resource_filename
import numpy as np

table_name = "tab_calibration_lut_jason-1.dat"

table = resource_filename(
    'cciseastate',
    ('resources/v2/%s' % table_name)
)
print("Calibration LUT table: {}".format(table))

with open(table) as cal_file:
    # skip first three header lines
    cal_file.readline()
    cal_file.readline()
    cal_file.readline()

    cal_table = np.array(
        list(map(float, cal_file.read().split()))
    ).reshape((499, 2))
    calibration = np.ma.fix_invalid(cal_table[:, 1]).flatten()

assert(calibration.size == 499)

cal_step = 0.05
cal_min = cal_table[0, 0] - cal_step / 2.
cal_max = cal_table[-1, 0] + cal_step / 2.

# check there are no masked values in the used range
# (rms_table_range)
if calibration.size != calibration.count():
    raise Exception(
        "There are undefined values in the RMS threshold table."
    )

SWH = np.ma.array([0, 0.01, 0.05, 0.30, 0.324, 0.325, 0.326, 24.95, 24.975,
                   25., 30.])
print(SWH, cal_min, cal_max)
# Correct Hs wrt LUT table's calibrations every <cal_step> meter
# range from <cal_min> meters to <cal_max> meter
cal_indices = np.floor(SWH / cal_step - cal_min / cal_step).astype(int)

cal_indices[cal_indices < 0] = 0
cal_indices[cal_indices >= len(calibration)] = -1
print(cal_indices)
print(calibration[cal_indices])
print(SWH + calibration[cal_indices])